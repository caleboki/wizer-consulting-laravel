<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    public function trainings()
    {
    	return $this->belongsToMany('App\Training', 'organization_training');
    }

    public function users()
    {
    	return $this->hasMany('App\User');
    }
}
