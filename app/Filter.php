<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $fillable = [
	'title'
	];

	public function courses()
	{
		return $this->belongsToMany('App\Course', 'course_filter', 'filter_id', 'course_id');
	}

	public function category()
	{
		return $this->belongsTo('App\Category');
	}
}
