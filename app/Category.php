<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'title', 'status'
    ];

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'course_category');
    }

    public function filters ()
    {

    	return $this->hasOne('App\Filter');
    }
}
