<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Category;
use App\Material;
use App\Filter;
use Session;
use Purifier;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::orderby('id', 'desc')->paginate(10);
        return view('courses.index')->with('courses', $courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $filters = Filter::all();
        $materials = Material::all();
        return view('courses.create', compact('categories', 'materials', 'filters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:100',
            'description' =>'required',
            'image' => 'image',
            
            ]);

        $course = new Course();
        $course->title = $request->title;
        $course->description = Purifier::clean($request->description);

        if($request->hasFile('image'))
        {
            
            $image = $request->image;
            
            $image_new_name = time().".".$image->getClientOriginalExtension();
            $image->move('assets/uploads/courses', $image_new_name);
            
            $course->image = 'assets/uploads/courses/' . $image_new_name;
        }
        else
        {
            $course->image = 'assets/extra-images/course-grid-img'.rand(1, 13).'.jpg';//replace $course->id with a random function
        }

       
        $course->save();

        if (isset($course->categories)) {
            
            $course->categories()->sync($request->categories);
        }

        if (isset($course->materials)) {
            
            $course->materials()->sync($request->materials);
        }

        
        

        $course->filters()->sync($request->filters);
        

        

        return redirect()->route('courses.index')->with('success', 'A new course has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('courses.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $categories = Category::all();
        $materials = Material::all();
        $filters = Filter::all();

        return view('courses.edit', compact('course', 'categories', 'materials', 'filters'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::find($id);
        
        $this->validate($request, [
            'title'=>'required|max:100',
            'description' =>'required',
            'image' => 'image'
            
            ]);

       
        $course->title = $request->title;
        $course->description = Purifier::clean($request->description);

        if($request->hasFile('image'))
        {
            
            $image = $request->image;
            $image_new_name = time() . $image->getClientOriginalName();
        
            $image->move('assets/uploads/courses', $image_new_name);
            
            $course->image = 'assets/uploads/courses/' . $image_new_name;
        }

       
        
        $course->save();

        $c = $request->categories;

        if (isset($c)) {        
            $course->categories()->sync($c);  
        }        
        else {
            $course->categories()->detach(); //If no category is selected remove exisiting category
        }

        if (isset($request->materials)) {
            
            $course->materials()->sync($request->materials);
        }

        else {
            $course->materials()->detach(); //If no material is selected remove exisiting material
        }

        $f = $request->filters;

        if (isset($f)) {        
            $course->filters()->sync($f);  
        }        
        else {
            $course->filters()->detach(); //If no filter is selected remove exisiting filter
        }

    
        Session::flash('success', 'Successfully updated the course');
        return redirect()->route('courses.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        $course->categories()->detach();
        $course->materials()->detach();
        $course->filters()->detach();
        $course->delete();
        return redirect()->route('courses.index')->with('success','Course has been  deleted');
    }
}
