<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Organization;
use App\Training;
use App\Instructor;
use Session;

use App\Mail\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->forget('training_cost');
        session()->forget('training_id');
        session()->forget('training_title');
        session()->forget('training_venue');
        Session::save();

        $trainings = Training::orderby('start_date', 'asc')->paginate(20);

        if (Auth::check())
        {

            $id = Auth::id();
            $user = User::findOrFail($id);


            $isMaterialAvailable = false;

            //if logged in user does not belong to an organization, get all trainings/events

            if (is_null($user->organization_id))
            {
                return view('events.events', compact('trainings', 'user', 'isMaterialAvailable'));
            }
            else
            {
                //Get only training sponsored by the user's organization
                $organization = Organization::findOrFail($user->organization_id);
                $trainings = $organization->trainings;
                return view('events.events', compact('trainings', 'user', 'isMaterialAvailable'));

            }
        }

        return view('events.events', compact('trainings', 'isMaterialAvailable'));
    }

    public function store(Request $request)
    {

        $training_id = (int)$request->training_id;
        $id = Auth::id();
        $user = User::findOrFail($id);
        $user->trainings()->attach($training_id);

        // Session::flash('success', 'You have successfully registered for this event. An email has been sent to you for more information');
        //Use modal instead here

        \Mail::to($user)->send(new Event($user));
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = Training::all();
        $event = Training::findOrFail($id);
        $instructors = Instructor::all();

        //Saving training info in session so as to be accessible on the purchase page
        session(['training_id' => $event->id]);
        session(['training_cost' => $event->cost]);
        session(['training_venue' => $event->venue]);
        session(['training_title' => $event->title]);
        Session::save();

        if (Auth::check()) {

            $id = Auth::id();
            $user = User::findOrFail($id);

            $isMaterialAvailable = false;
            return view('events.show', compact('events', 'event', 'user', 'instructors', 'isMaterialAvailable'));
        }
        else
        {
            return view('events.show', compact('events', 'event', 'instructors'));
        }


    }


    public function contact()
    {
       return view('front.contact');
    }

    public function materials(Request $request)
    {
       $event_id = (int)$request->training_id;
       $event = Training::findOrFail($event_id);
       $materialIds = [];
       return view('events.materials', compact('event', 'materialIds'));
    }
}
