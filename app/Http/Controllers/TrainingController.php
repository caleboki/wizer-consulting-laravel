<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Training;
use App\Course;
use App\Instructor;
use Session;
use Purifier;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trainings = Training::all();
        return view('trainings.index')->with('trainings', $trainings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $courses = Course::all();
        $instructors = Instructor::all();
        return view('trainings.create', compact('courses', 'instructors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'title'=>'required|max:80',
            
            'image'=>'required|image',
            'venue'=>'required',
            
            'outcome'=>'required|max:820',
            'course_delivery'=>'required|max:800',
            'who_should_attend'=>'required|max:800',
            'cost'=>'required',
            'start_date'=>'required|date',
            'end_date' =>'required|date|after:start_date'
            ]);

        $training = new Training();
        
        $training->title = $request->title;
        $training->venue = Purifier::clean($request->venue);
        $training->cost = $request->cost;
        $training->description = Purifier::clean($request->description);
        $training->outcome = Purifier::clean($request->outcome);
        $training->course_delivery = Purifier::clean($request->course_delivery);
        $training->who_should_attend = Purifier::clean($request->who_should_attend);
        $training->start_date = date("Y-m-d H:i:s", strtotime($request->start_date));
        $training->end_date = date("Y-m-d H:i:s", strtotime($request->end_date));

        if ($request->image) {
            
            $image = $request->image;
            $image_new_name = time() . $image->getClientOriginalName();
        
            $image->move('assets/uploads/trainings', $image_new_name);
            
            $training->image = 'assets/uploads/trainings/' . $image_new_name;
        }
        
       
        
        $training->save();

        $training->courses()->sync($request->courses);

        $training->instructors()->sync($request->instructors);


        return redirect()->route('trainings.index')->with('success', 'A new training has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('trainings.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $training = Training::findOrFail($id);
        $courses = Course::all();
        $instructors = Instructor::all();
        

        return view('trainings.edit', compact('training', 'courses', 'instructors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $training = Training::find($id);
        
         $this->validate($request, [
            'title'=>'required|max:80',
            'image'=>'image',
            //'image'=>'image',
            'venue'=>'required',
            //'description'=>'required',
            'outcome'=>'required|max:820',
            'course_delivery'=>'required|max:800',
            'who_should_attend'=>'required|max:800',
            'cost'=>'required',
            'start_date'=>'required|date',
            'end_date' =>'required|date|after:start_date'
            ]);

        $training->title = $request->title;
        $training->venue = Purifier::clean($request->venue);
        $training->cost = $request->cost;
        $training->description = Purifier::clean($request->description);
        $training->outcome = Purifier::clean($request->outcome);
        $training->course_delivery = Purifier::clean($request->course_delivery);
        $training->who_should_attend = Purifier::clean($request->who_should_attend);        
        $training->start_date = date("Y-m-d H:i:s", strtotime($request->start_date));
        $training->end_date = date("Y-m-d H:i:s", strtotime($request->end_date));

        if ($request->image)
        {
            $image = $request->image;
            $image_new_name = time() . $image->getClientOriginalName();
        
            $image->move('assets/uploads/trainings', $image_new_name);
            
            $training->image = 'assets/uploads/trainings/' . $image_new_name;
        }
        
        $training->save();

        $c = $request->courses;

        if (isset($c)) {        
            $training->courses()->sync($c);  
        }        
        else {
            $training->courses()->detach(); 
        }

        $i = $request->instructors;

        if (isset($i)) {        
            $training->instructors()->sync($i);  
        }        
        else {
            $training->instructors()->detach(); 
        }

       
    
        Session::flash('success', 'Successfully updated the training');
        return redirect()->route('trainings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $training = Training::find($id);
        $training->courses()->detach();
        $training->instructors()->detach();
        $training->users()->detach();
        $training->organizations()->detach();
        
        $training->delete();
        return redirect()->route('trainings.index')->with('success','Training has been deleted');
    }
}
