<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Session;
use Purifier;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comments = Comment::orderby('id', 'desc')->paginate(5); 

        return view('comments.index')->with('comments', $comments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('comments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
            'organization'=>'required',
            'comment'=>'required'
            ]);

        $comment = new Comment();
        
        $comment->title = $request->title;
        $comment->organization = $request->organization;
        $comment->comment = Purifier::clean($request->comment);
        
        $comment->save();

        return redirect()->route('comments.index')->with('success', 'A new comment has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::findOrFail($id);
        return view('comments.edit', compact('comment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);
        
        $this->validate($request, [
            'title'=>'required',
            'organization'=>'required',
            'comment'=>'required'
            
            ]);

        $comment->title = $request->title;
        $comment->organization = $request->organization;
        $comment->comment = Purifier::clean($request->comment);
               
        $comment->save();

       
    
        Session::flash('success', 'Successfully updated the comment');
        return redirect()->route('comments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
               
        $comment->delete();
        return redirect()->route('comments.index')->with('success','Comment has been  deleted');
    }
}
