<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Training;
use Auth;
use Session;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::findOrFail($id);
        return view('home', compact('user'));
    }

    // public function store(Request $request)
    // {
    //     $this->validate($request, [     
    //         'avatar' => 'image'
    //         ]);

    //     $id = Auth::id();

    //     $user = User::find($id);

    //     $user->avatar = $request->avatar;

    //     if($request->hasFile('avatar'))
    //     {
            
    //         $avatar = $request->avatar;
    //         $avatar_new_name = $user->first_name.$user->last_name.".".$avatar->getClientOriginalExtension();
    //         $avatar->move('assets/uploads/avatars', $avatar_new_name);
            
    //         $user->avatar = 'assets/uploads/avatars/' . $avatar_new_name;
    //     }
    //     else
    //     {
    //         $user->avatar = 'assets/uploads/avatars/1.png';
    //     }
        
    //     $user->save();

    //     return redirect()->back();

    //     //return redirect()->route('home')->with('success','Your avater has been updated');

    // }

    public function upload(Request $request)
    {
        //return "hello world";

        if (!$request->hasFile('profile_picture') ) {
            
            return abort(404, 'The upload field is required');
        }

        $allowed = array('jpg', 'jpeg', 'png', 'bmp');

        foreach ($allowed as $a) {

            if ($a == $request->profile_picture->extension()) {

                $uploadedFile = $request->profile_picture;
        
                $uploadPath = $uploadedFile->store('profile_pictures');
                session(['uploadPath' => $uploadPath]);
                
                Session::save();

                $id = Auth::id();
                $user = User::find($id);
                
                $user->avatar = session('uploadPath');
                $user->save();
                
                return response(['status' => 'success'], 200);

            }
            // else {
            //     return abort(404, 'Only these formats are allowed '. implode(" , ", $allowed));
            // }

        }
        return abort(404, 'Only these formats are allowed '. implode(" , ", $allowed));

       

        
    }

    public function setting()
    {
        
            
        $id = Auth::id();
        $user = User::findOrFail($id);
        return view('home-settings', compact('user'));
                
    }

    public function updateSetting(Request $request)
    {
       
        $id = Auth::id();
       $user = User::find($id);

        $this->validate($request, [
            'first_name' => 'required|string|max:25',
            'last_name' => 'required|string|max:25',
            'email' => 'required|string|email|max:25|unique:users,email,'.$id,
            'password' => 'required|string|min:6|confirmed',
            ]);
            

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);

        $user->save();

        Session::flash('success', 'You have successfully updated your profile');
        return redirect()->route('setting');
    }

    public function upcoming()
    {
         $id = Auth::id();
        $user = User::find($id);
        $trainings = Training::all();
        return view('home-upcoming', compact('user', 'trainings'));
    }

    

}
