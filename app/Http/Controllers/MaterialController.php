<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Material;
use App\Course;
use Session;
use Purifier;


class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materials = Material::orderby('id', 'desc')->paginate(10);
        return view('materials.index')->with('materials', $materials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();


        return view('materials.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:100',
            'description' =>'required',
            'link' => 'required'
            ]);

        $material = new Material();
        $material->title = $request->title;
        $material->description = Purifier::clean($request->description);

        $link = $request->link;
        $link_new_name = $material->title.".".$link->getClientOriginalExtension();
        $link->move('assets/uploads/materials', $link_new_name);

        $material->link = 'assets/uploads/materials/' . $link_new_name;
        $material->save();

        $material->courses()->sync($request->courses);


        return redirect()->route('materials.index')->with('success', 'A new material has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = Material::findOrFail($id);
        $courses = Course::all();

        return view('materials.edit', compact('material', 'courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $material = Material::find($id);

        $this->validate($request, [
            'title'=>'required|max:100',
            'description' =>'required',
            'link' => 'required'
            ]);

        $material->title = $request->title;
        $material->description = Purifier::clean($request->description);

        $material->link = $request->link;
        $material->save();

        $c = $request->courses;

        if (isset($c)) {
            $material->courses()->sync($c);
        }
        else {
            $material->courses()->detach(); //If no course is selected remove exisiting course
        }



        Session::flash('success', 'Successfully updated the material');
        return redirect()->route('materials.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $material = Material::find($id);
        $material->courses()->detach();
        $material->delete();
        return redirect()->route('materials.index')->with('success','Material has been  deleted');
    }
}
