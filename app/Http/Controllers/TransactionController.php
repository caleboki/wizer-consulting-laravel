<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Transaction;
use App\Training;

use App\Mail\PaymentVerified;
use App\Mail\AdminPaymentVerified;

use Session;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::all();
        return view('transactions.index', compact('transactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        dd('this is the create section');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $transaction = Transaction::findOrFail($id);
        return view('transactions.edit', compact('transaction'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        $this->validate($request, [
            'status' => 'required'
        ]);

        if ($request->status == 'canceled') {
            
            $transaction->status = -2;
        }
        
        if ($request->status == 'expired') {
            
            $transaction->status = -1;
        }

        if ($request->status == 'unverified') {
            
            $transaction->status = 0;
        }

        if ($request->status == 'pending') {
            
            $transaction->status = 1;
        }

        if ($request->status == 'verified') {
            
            $transaction->status = 2;

            $users = $transaction->users()->get();
            $training = Training::find($transaction->training_id);

            foreach($users as $user) {
                \Mail::to($user)->send(new PaymentVerified($user, $transaction, $training));
            }

            $user = Auth::user();
            \Mail::to($user)->send(new AdminPaymentVerified($user, $transaction, $training));
            
        }

        

        

        $transaction->save();
        Session::flash('success', 'Transaction successfully updated');
        return redirect()->route('transactions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
