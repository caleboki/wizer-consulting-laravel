<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Organization;
use App\Course;
use App\Training;
use App\Category;
use App\Material;
use App\Comment;
use App\Resource;


class AdminController extends Controller
{

	public function index()
	{
		return redirect()->route('admin.dashboard');
	}

    public function dashboard()
    {
    	$users = User::orderBy('created_at', 'desc')->get();
    	$organizations = Organization::all();
    	$courses = Course::all();
    	$trainings = Training::all();
    	$categories = Category::all();
        $materials = Material::all();
        $resources = Resource::all();
    	$comments = Comment::all();
    	return view('admin.dashboard', compact('users', 'organizations', 'courses', 'trainings', 'categories', 'materials', 'comments', 'resources'));
    }
}
