<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organization;
use App\Training;
use Session;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizations = Organization::orderby('id', 'desc')->paginate(10);
        return view('organizations.index')->with('organizations', $organizations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trainings = Training::all();
        
        return view('organizations.create', compact('trainings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required',
            'code'=>'required|unique:organizations,code'
            
            ]);

        $organization = new Organization();
        $organization->name = $request->name;
        $organization->industry = $request->industry;
        $organization->code = $request->code;
        
        $organization->save();

        $organization->trainings()->sync($request->trainings);


        return redirect()->route('organizations.index')->with('success', 'A new Organization has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organization = Organization::findOrFail($id);
        $trainings = Training::all();
        

        return view('organizations.edit', compact('organization', 'trainings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organization = Organization::find($id);
        
        $this->validate($request, [
            'name'=>'required'
            
            ]);

        $organization->name = $request->name;
        $organization->industry = $request->industry;
        $organization->code = $request->code;
        
        $organization->save();

        $c = $request->trainings;

        if (isset($c)) {        
            $organization->trainings()->sync($c);  
        }        
        else {
            $organization->trainings()->detach(); 
        }

       
    
        Session::flash('success', 'Successfully updated the Organization');
        return redirect()->route('organizations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organization = Organization::find($id);
        $organization->trainings()->detach();
        
        $organization->delete();
        return redirect()->route('organizations.index')->with('success','Organization has been  deleted');
    }
}
