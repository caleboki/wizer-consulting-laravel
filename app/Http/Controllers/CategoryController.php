<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use Illuminate\Http\Request;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderby('id', 'desc')->paginate(5); 

        return view('categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        return view('categories.create')->withCourses($courses);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:100',
            'status' => 'required'
            ]);

        $category = new Category();
        $category->title = $request->get('title');
        $category->status = $request->get('status');

        $category->save();

        $category->courses()->sync($request->courses);

        return redirect()->route('categories')->with('success', 'A new category has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $category = Category::findOrFail($id); //Find post of id = $id

        // return view ('categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $category = Category::findOrFail($id);
         $courses = Course::all();
         return view('categories.edit', compact('category', 'courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        
        
        $this->validate($request, [
            'title'=>'required|max:100',
            'status' => 'required'
            ]);

        $category->title = $request->title;
        $category->status = $request->status;

        $category->save();

        $c = $request->courses;

        if (isset($c)) {        
            $category->courses()->sync($c);  
        }        
        else {
            $category->courses()->detach(); //If no course is selected remove exisiting course
        }

        Session::flash('success', 'Successfully updated the category');
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->courses()->detach();
        $category->delete();
        return redirect()->route('categories')->with('success','Category has been  deleted');
    }
}
