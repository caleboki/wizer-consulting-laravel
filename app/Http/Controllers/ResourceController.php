<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;
use Session;
use Purifier;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::orderby('id', 'desc')->paginate(10);
        return view('resources.index')->with('resources', $resources);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('resources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required|max:100',
            'description' =>'required',
            'link' => 'required'
            ]);

        $resource = new Resource();
        $resource->title = $request->title;
        $resource->description = Purifier::clean($request->description);

        $link = $request->link;
        $link_new_name = $resource->title.".".$link->getClientOriginalExtension();
        $link->move('assets/uploads/resources', $link_new_name);

        $resource->link = 'assets/uploads/resources/' . $link_new_name;
        $resource->save();


        return redirect()->route('resources.index')->with('success', 'A new resource has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resource = Resource::findOrFail($id);
        return view('resources.edit', compact('resource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $resource = Resource::find($id);

        $this->validate($request, [
            'title'=>'required|max:100',
            'description' =>'required',
            'link' => 'required'
            ]);

        $resource->title = $request->title;
        $resource->description = Purifier::clean($request->description);

        $resource->link = $request->link;
        $resource->save();


        Session::flash('success', 'Successfully updated the resource');
        return redirect()->route('resources.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource = Resource::find($id);
        $resource->delete();
        return redirect()->route('resources.index')->with('success','Resource has been  deleted');
    }
}
