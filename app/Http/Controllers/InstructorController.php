<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Instructor;
use App\Training;
use Session;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructors = Instructor::orderby('id', 'desc')->paginate(5); 

        return view('instructors.index')->with('instructors', $instructors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trainings = Training::all();
        
        return view('instructors.create', compact('trainings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'about' => 'max:500',
            'avatar' => 'image',
            'email' => 'required|string|email|max:255|unique:instructors',
            
            ]);

        $instructor = new Instructor();

        
        $instructor->first_name = $request->get('first_name');
        $instructor->last_name = $request->get('last_name');
        $instructor->avatar = $request->get('avatar');
        $instructor->email = $request->get('email');
        $instructor->about = $request->get('about');
        $instructor->title = $request->get('title');

        if($request->hasFile('avatar'))
        {
            
            $avatar = $request->avatar;
            
            $avatar_new_name = $instructor->first_name.$instructor->last_name.".".$avatar->getClientOriginalExtension();
            $avatar->move('assets/uploads/instructors', $avatar_new_name);
            
            $instructor->avatar = 'assets/uploads/instructors/' . $avatar_new_name;
        }
        else
        {
            $instructor->avatar = 'assets/uploads/instructors/1.png';
        }

        $instructor->save();

        $instructor->trainings()->sync($request->trainings);

        Session::flash('success', 'Successfully created a new instructor');
        return redirect()->route('instructors.index');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instructor = Instructor::findOrFail($id);
        return view('instructors.show', compact('instructor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $instructor = Instructor::findOrFail($id);
        $trainings = Training::all();
        return view('instructors.edit', compact('instructor', 'trainings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $instructor = Instructor::find($id);

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'about' => 'max:500',
            'avatar' => 'image',
            'email' => 'required|string|email|max:255|unique:instructors,email,'.$id,
            
            ]);

        $instructor->first_name = $request->get('first_name');
        $instructor->last_name = $request->get('last_name');
        $instructor->title = $request->get('title');
        $instructor->about = $request->get('about');
        $instructor->email = $request->get('email');

        if($request->hasFile('avatar'))
        {
            
            $avatar = $request->avatar;
            
            $avatar_new_name = $instructor->first_name.$instructor->last_name.".".$avatar->getClientOriginalExtension();
            $avatar->move('assets/uploads/instructors', $avatar_new_name);
            
            $instructor->avatar = 'assets/uploads/instructors/' . $avatar_new_name;
        }

        $instructor->save();

        $c = $request->trainings;
        

        if (isset($c)) {        
            $instructor->trainings()->sync($c);  
        }        
        else {
            $instructor->trainings()->detach(); //If no training is selected remove exisiting training
        }

        Session::flash('success', 'Successfully updated the instructor');
        return redirect()->route('instructors.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $instructor = Instructor::find($id);
        $instructor->trainings()->detach();

        $instructor->delete();
        return redirect()->route('instructors.index')->with('success','The Instructor has been  deleted');
    }
}
