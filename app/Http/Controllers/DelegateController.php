<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;


use App\Transaction;
use App\Training;
use App\Mail\Welcome;
use App\Mail\DelegateEvent;
use App\Mail\DelegateWelcome;
use App\Mail\DelegatesWelcome;
use App\Mail\CustomerEvent;
use Session;


use Illuminate\Support\Facades\Hash;

class DelegateController extends Controller
{

    public function __construct(Request $request)
    {
        if ($request->training_id) {


            session(['training_id' => $request->training_id]);
            session(['training_cost' => $request->training_cost]);
            session(['training_title' => $request->training_title]);

            Session::save();

        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

       if (is_null($request->training_id))
       {
           return redirect()->route('events.index');
       }

       if (!session('training_id') && !$request)
       {
           return redirect()->route('events.index');
       }

        $training_id = $request->training_id;
        $training_title = $request->training_title;
        $training_venue = $request->training_venue;
        $training_cost = $request->training_cost;



        //Put training path to session so it can be accessed from login controller

        $request->session()->put('path', '/events/'.$training_id);


        return view('delegates.create', compact('training_id', 'training_title', 'training_venue', 'training_cost'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * This method is handles the registration of an event when a user is not signed in
     */
    public function store(Request $request)
    {

        $training_id = (int)$request->training_id;
        $training_title = $request->training_title;
        $training_venue = $request->training_venue;
        $training_cost = (int)$request->training_cost;

        $this->validate($request, [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',

            'email' => 'required|string|email|max:100',

            ]);

        $email = $request->email;

        //store email in session, to be used on purchase page

        session(['email' => $email]);

        $user = User::where('email', $request->email)->first();

        //If user is not authenticated

        if (!Auth::check())
        {

            //If entered user does not already exist add it
            if (!$user) {

                $this->validate($request, [
                'first_name' => 'required|string|max:25',
                'last_name' => 'required|string|max:25',

                'email' => 'required|string|email|max:25',

                ]);

                $length = 10;
                $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
                $str = '';
                $max = mb_strlen($keyspace, '8bit') - 1;

                for ($i = 0; $i < $length; ++$i) {
                    $str .= $keyspace[random_int(0, $max)];
                }
                $password = $str;

                $user = new User();

                $user->first_name = $request->get('first_name');
                $user->last_name = $request->get('last_name');


                $user->email = $email;
                $user->password = Hash::make($password);

                $user->save();

                $u = User::where('email', $user->email)->first();
                $u->trainings()->sync($training_id);

                $transaction = new Transaction();

                $transaction->training_id = $training_id;


                $narration = "WIZ".date("Ymd").time();

                $transaction->user_id = $user->id;
                $transaction->narration = $narration;
                $transaction->cost = $training_cost;

                $transaction->save();

                $user->transactions()->attach($transaction->id);

                //Send Delegate/Welcome email
                $training = $user->trainings[0];
                \Mail::to($user)->send(new DelegateWelcome($user, $password, $transaction, $training));


            }

            else {

                return redirect()->route('login');

            }


            return view('purchase', compact('email', 'training_title', 'training_venue', 'training_cost', 'narration'));

        }


        return view('purchase', compact('email', 'training_title', 'training_venue', 'training_cost', 'narration'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:100',
            'last_name' => 'required|string|max:100',

            'email' => 'required|string|email|max:100',

            ]);

        return response()->json(['success'=>'Done!']);
    }

    // This method handles registration of multiple users for a course. E.g HR registering multiple delegates
    public function persist(Request $request)
    {

        //Save delegates to db here

        $delegates = $request->all();


        $steps = count($delegates);
        $training_id = session('training_id');
        $training_title = session('training_title');
        $training_venue = session('training_venue');
        $training_cost = (int)session('training_cost') * $steps;


        $id = Auth::id();

        $transaction = new Transaction();

        $transaction->training_id = (int)$training_id;
        $transaction->user_id = $id;
        $narration = "WIZ".date("Ymd").time();
        $transaction->narration = $narration;
        $transaction->cost = $training_cost;
        $transaction->save();

        $training = Training::find($transaction->training_id);


        $delegates_ids = array();
        foreach ($delegates as $delegate) {

            $email = $delegate['email'];

            $user = User::where('email', $email)->first();


            //Check if delegate does not already exist in db

            if (!$user) {


                $length = 10;
                $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
                $str = '';
                $max = mb_strlen($keyspace, '8bit') - 1;

                for ($i = 0; $i < $length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
                }
                $password = $str;

                $user = new User();

                $user->first_name = $delegate['first_name'];
                $user->last_name = $delegate['last_name'];
                $user->email = $delegate['email'];
                $user->password = Hash::make($password);

                //\Mail::to($user)->send(new Welcome($user, $password));



                \Mail::to($user)->send(new DelegatesWelcome($user, $password, $transaction, $training));

                $user->save();

            }

            else {

                $user = User::where('email', $delegate['email'])->first();

                \Mail::to($user)->send(new CustomerEvent($user, $transaction, $training));

            }


            array_push($delegates_ids, $user->id);

        }




        //Attach saved delegates to transactions
        foreach ($delegates_ids as $id) {

            $user = User::find($id);
            $user->transactions()->attach($transaction->id);
            $user->trainings()->attach($training_id);


        }

        //Send email to registrant/hr
        $user = Auth::user();
        $training = Training::find($transaction->training_id);
        \Mail::to($user)->send(new DelegateEvent($user, $transaction, $training));


        session(['email' => $email]);
        session(['training_cost' => $training_cost]);
        session(['training_venue' => $training_venue]);
        session(['narration' => $narration]);
        session(['training_title' => $training_title]);


        Session::save();

       return view('purchase', compact('email', 'training_title', 'training_venue', 'training_cost', 'narration'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //This method handles self registration of an event
    public function purchase(Request $request)
    {
        //Clear narration session here because of purchase page
        session()->forget('narration');
        Session::save();

        if ($request)
        {

            $training_id = (int)$request->training_id;
            $training_title = $request->training_title;
            $training_venue = $request->training_venue;
            $training_cost = (int)$request->training_cost;


            $id = (int)$request->user_id;

            if ($request->email)
            {

                $email = $request->email;
            }

            else
            {

                $email = Auth::user()->email;
            }


             $this->validate($request, [
                'first_name' => 'required|string|max:100',
                'last_name' => 'required|string|max:100',

                'email' => 'required|string|email|max:100',

                ]);

            session(['email' => $email]);
            Session::save();

            //Not creating a new user, just add transaction
            $user = User::find($id);

            //Prevent duplicate entry on training_user table upon page refresh
            $user_trainings = $user->trainings()->get()->all();
            $ids = [];
            foreach ($user_trainings as $user_training) {

                array_push($ids, $user_training->id);

            }

            if (!in_array($training_id, $ids)) {

                $user->trainings()->attach($training_id);

            }


            foreach ($user->transactions as $transaction)
            {

                if($transaction->training_id == $training_id && $transaction->cost == $training_cost)
                {
                    $narration = $transaction->narration;
                    break;
                }
            }

            if (!isset($narration))
            {

                $transaction = new Transaction();

                $transaction->training_id = $training_id;

                $transaction->user_id = Auth::id();

                $narration = "WIZ".date("Ymd").time();
                $transaction->narration = $narration;

                $transaction->cost = $training_cost;

                $transaction->save();

                $user->transactions()->attach($transaction->id);

            }


            //Get training corresponing to this transaction: to be sent to registrant email
            $training = Training::find($transaction->training_id);

            \Mail::to($user)->send(new DelegateEvent($user, $transaction, $training));


            return view('purchase', compact('training_id', 'training_title', 'training_venue', 'email', 'training_cost', 'narration'));

        }

    }

    public function narration(Request $request)
    {

        $narration = $request->narration;

        $transaction = Transaction::where('narration', $narration)->first();
        $transaction->payment_proof = session('uploadPath');
        $transaction->status = 1;
        $transaction->save();


        // Session::flash('success', 'You have successfully registered for this event');
        flash('Thank you for your registration. Payment will be verified and a confirmation email will be sent to you shortly')->success();

    }

    public function payment_proof(Request $request)
    {

        if (!$request->hasFile('payment_proof') ) {

            return abort(404, 'The upload field is required');
        }

        $allowed = array('pdf', 'doc', 'docx', 'txt', 'jpg', 'jpeg', 'png', 'bmp');

        foreach ($allowed as $a) {

            if ($a == $request->payment_proof->extension()) {

                $uploadedFile = $request->payment_proof;

                $uploadPath = $uploadedFile->store('transactions');
                session(['uploadPath' => $uploadPath]);

                Session::save();

                return response(['status' => 'success'], 200);

            }

        }

        return abort(404, 'Only these formats are allowed '. implode(" , ", $allowed));

    }

    public function edit_transactions(Request $request)
    {
        return view('proof.edit');
    }

    public function update_transactions(Request $request)
    {
        $this->validate($request, [
            'narration' => 'required|string|max:100',

            ]);

        $narration = $request->narration;

        $transaction = Transaction::where('narration', $narration)->firstOrFail();

        if ($transaction) {
            //Get training/event details corresponding to this transaction
            $training = Training::find($transaction->training_id);
            $training_id = $training->id;
            $training_cost = $training->cost;
            $training_venue = $training->venue;
            $training_title = $training->title;

            //Get user/initiator corresponding to this transaction;
            $user = User::find($transaction->user_id);


            return view('proof.update', compact('transaction', 'training_id', 'training_cost', 'training_venue', 'training_title', 'user'));
        }

    }

}
