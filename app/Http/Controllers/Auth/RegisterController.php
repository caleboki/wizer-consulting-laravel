<?php

namespace App\Http\Controllers\Auth;


use Auth;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Organization;
use Session;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Validation\ValidationException;

use App\Mail\Welcome;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            ]);
        
    }

    // public function register(Request $request)
    // {
    //     $this->validator($request->all())->validate();
    //     //dd($request->code);

    //     if (is_null($request->code)) //If user does not belong to an organization
    //     {
            
    //         event(new Registered($user = $this->create($request->all())));

    //         $this->guard()->login($user);

    //         return $this->registered($request, $user)
    //                     ?: redirect($this->redirectPath());    

    //     }

    //     if (!is_null($request->code)) { //If user belongs to an organization
            
    //         $c = Organization::where('code', $request->code)->first();

    //         if (is_null($c)) //If user enters the wrong organization code
    //         { 
                
    //             //return redirect('register')->with('error', 'No organization found');
    //             //Session::flash('error', 'Wrong code');
    //             // $request->session()->flash('error', 'Wrong code');
    //             // return redirect('register');

    //             throw ValidationException::withMessages([
    //         $this->username() => [trans('auth.failed')],
    //     ]);


    //         }

    //         if (!is_null($c)) {
            
                

    //             $user = new User;

    //             //dd($c->id);
    //             $user->organization_id = $c->id;
    //             $user->first_name = $request->get('first_name');
    //             $user->last_name = $request->get('last_name');
    //             $user->avatar = 'assets/uploads/avatars/1.png';
    //             $user->email = $request->get('email');
    //             $user->password = Hash::make($request->get('password'));

    //             $user->save();
    //             $this->guard()->login($user);

    //             return $this->registered($request, $user)
    //                     ?: redirect($this->redirectPath());     

    //         }
    //     }

    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {       
       
        
        // return User::create([
        // 'first_name' => $data['first_name'],
        // 'last_name' => $data['last_name'],
        // 'email' => $data['email'],
        // 'password' => Hash::make($data['password']),
        // ]);

        $user = User::create([
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
        ]);

        \Mail::to($user)->send(new Welcome($user, $data['password']));

        return $user;


        
    }

   





    

}
