<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(Request $request){

        
        if (session('path')) {
            
            $r = session('path');
            session()->forget('path');
            $this->redirectTo = $r;
            
        }

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {

            // $p = $request->session()->pull('path', 'default');

            // if ($p) {
                
            //     //return response()->json(['code'=> 0, 'message'=> 'Login Successful']);
            //     return response()->json(['success'=>'Done!']);
            // }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        

        return $this->sendFailedLoginResponse($request);
        //return response()->json(['code'=> 96, 'message'=> 'Incorrect email and/or password']);
        


        //dd($this->redirectTo);

        // $this->validateLogin($request);

        // if ($this->attemptLogin($request)) {
        //     //return response()->json(['code'=> 0, 'message'=> 'Login Successful']);
        //     return redirect()->back();

        // }else{
        //     //return response()->json(['code'=> 96, 'message'=> 'Incorrect email and/or password']);
        //     return redirect()->back()->with('errors','Operation Successful !');

        // }
    }

    
}
