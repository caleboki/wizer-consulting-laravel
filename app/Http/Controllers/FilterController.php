<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filter;
use App\Course;
use App\Category;
use Session;

class FilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = Filter::orderby('id', 'desc')->paginate(10);
        return view('filters.index')->with('filters', $filters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        $categories = Category::all();
        $filters = Filter::all();
        
        return view('filters.create', compact('courses', 'categories', 'filters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $courses = Course::all();
        $this->validate($request, [
            'title'=>'required|max:50'
            
            ]);

        $filter = new Filter();
        $filter->title = $request->title; 
        $filter->category_id = $request->category_id;   
        $filter->save();

        

        if (isset($courses)) {
            $filter->courses()->sync($request->courses);
        }

        

        return redirect('filters')->with('success', 'A new filter has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $filter = Filter::findOrFail($id);
        $courses = Course::all();
        $categories = Category::all();

        return view('filters.edit', compact('filter', 'courses', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $filter = Filter::find($id);
        $category = Category::find($id);
        
        $this->validate($request, [
            'title'=>'required|max:50'
            
            ]);

        $filter->title = $request->title;
        
        $filter->save();

        $f = $request->courses;

        if (isset($f)) {        
            $filter->courses()->sync($f);  
        }        
        else {
            $filter->courses()->detach(); //If no filter is selected remove exisiting filter
        }

    
        Session::flash('success', 'Successfully updated the filter');
        return redirect()->route('filters.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $filter = Filter::find($id);
        $filter->courses()->detach();
        $filter->delete();
        return redirect('filters')->with('success','Filter has been  deleted');
    }
}
