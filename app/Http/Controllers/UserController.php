<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Organization;
use App\Training;
use App\Role;
use App\Transaction;
use Session;
use Illuminate\Support\Facades\Hash;

use App\Mail\Welcome;
use App\Mail\Event;
use App\Mail\myEvent;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderby('id', 'desc')->paginate(20);

        return view('users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $organizations = Organization::all();
        $trainings = Training::all();

        return view('users.create', compact('organizations', 'trainings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'avatar' => 'image',
            'email' => 'required|string|email|max:255|unique:users',
            //'password' => 'required|string|min:6',
            ]);

        if ($request->has('password') && !empty($request->password))
            {
                $password = trim($request->password);
            }
        else
            {
                $length = 10;
                $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
                $str = '';
                $max = mb_strlen($keyspace, '8bit') - 1;
                for ($i = 0; $i < $length; ++$i) {
                $str .= $keyspace[random_int(0, $max)];
                }
                $password = $str;

            }



        $user = new User();


        $user->organization_id = $request->organizations[0];
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->email = $request->get('email');
        $user->password = Hash::make($password);

        if($request->hasFile('avatar'))
        {

            $uploadedFile = $request->avatar;

            $uploadPath = $uploadedFile->store('profile_pictures');
            session(['uploadPath' => $uploadPath]);

            Session::save();

            $user->avatar = session('uploadPath');
            $user->save();
        }
        else
        {
            $user->avatar = 'profile_pictures/1.png';
        }

        \Mail::to($user)->send(new Welcome($user, $password));

        $user->save();


        if (!is_null($request->trainings)) {

            $user->trainings()->sync($request->trainings);
            \Mail::to($user)->send(new myEvent($user));
        }

        return redirect()->route('users.index')->with('success', 'A new user has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = User::findOrFail($id);
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organizations = Organization::all();
        $user = User::findOrFail($id);
        $trainings = Training::all();
        $roles = Role::all();
        return view('users.edit', compact('organizations', 'user', 'trainings', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::find($id);

        $this->validate($request, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,
            //'password' => 'required|string|min:6|confirmed',
            ]);

        if ($request->password_options == 'auto')
        {

            $length = 10;
            $keyspace = '123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
            $str = '';
            $max = mb_strlen($keyspace, '8bit') - 1;
            for ($i = 0; $i < $length; ++$i)
            {
                $str .= $keyspace[random_int(0, $max)];
            }

            $user->password = Hash::make($str);

            $user->organization_id = $request->organizations[0];
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;


            $user->save();

        }

        elseif ($request->password_options == 'manual')
        {

            $this->validate($request, [

            'password' => 'required|string|min:6',
            ]);


            $user->password = Hash::make($request->password);

            $user->organization_id = $request->organizations[0];
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;

            $user->save();
        }

        else
        {
            $user->organization_id = $request->organizations[0];
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;

            $user->save();

        }



        if($request->hasFile('avatar'))
        {

            $uploadedFile = $request->avatar;

            $uploadPath = $uploadedFile->store('profile_pictures');
            session(['uploadPath' => $uploadPath]);

            Session::save();

            $user->avatar = session('uploadPath');
            $user->save();

        }


        $c = $request->trainings;


        if (isset($c)) {
            $user->trainings()->sync($c);
        }
        else {
            //If no training is selected remove exisiting training

            //Get transaction id/ids
            $ids = array();
            foreach($user->transactions as $t) {

                array_push($ids, $t->id);

            }

           //If no training or user is assigned to these transactions cancel it
            foreach($ids as $id) {
                $tra = Transaction::find($id);

                $count = count($tra->users);
                //dd($count);
                if($count == 1) {
                    $tra->status = -2;
                    $tra->save();
                }
            }
            $user->transactions()->detach();

            $user->trainings()->detach();


        }

        if ($request->roles)
        {

            $user->roles()->sync($request->roles);
        }
        else
        {
            $user->roles()->detach();
        }

        if (!is_null($request->trainings)) {

            \Mail::to($user)->send(new myEvent($user));
        }


        Session::flash('success', 'Successfully updated the user');
        return redirect()->route('users.index');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->trainings()->detach();
        $user->transactions()->detach();

        $user->delete();
        return redirect()->route('users.index')->with('success','The User has been  deleted');
    }
}
