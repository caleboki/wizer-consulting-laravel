<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resource;

class ListResourcesController extends Controller
{
    public function index() {

        $resources = Resource::orderby('id', 'desc')->paginate(10);
        return view('listresources')->with('resources', $resources);
    }
}
