<?php
//fixing indentation in the view
function formatList($str) 
    {
        return str_replace('</li>', '</span></li>', str_replace('<li>','
        <li><div style="margin-left: 14px; margin-top: -30px;">', $str));
    }

function randImage() 
	{

		 $bg = array('business-work.jpg', 'iStock_000023490895Small.jpg', 'iStock_000052131506Small.jpg', 'training_lead.jpg', 'training1.jpg', 'training2.jpg'); // array of filenames
    // $bg = array('iStock_000052131506Small.jpg', 'training_lead.jpg');

		$i = rand(0, count($bg)-1); // generate random number size of the array
  		$selectedBg = "$bg[$i]"; // set variable equal to which random filename was chosen
  		return $selectedBg;
	}

function formatSizeUnits($bytes)
  {
      if ($bytes >= 1073741824)
      {
          $bytes = number_format($bytes / 1073741824, 2) . ' GB';
      }
      elseif ($bytes >= 1048576)
      {
          $bytes = number_format($bytes / 1048576, 2) . ' MB';
      }
      elseif ($bytes >= 1024)
      {
          $bytes = number_format($bytes / 1024, 2) . ' KB';
      }
      elseif ($bytes > 1)
      {
          $bytes = $bytes . ' bytes';
      }
      elseif ($bytes == 1)
      {
          $bytes = $bytes . ' byte';
      }
      else
      {
          $bytes = '0 bytes';
      }

      return $bytes;
}

