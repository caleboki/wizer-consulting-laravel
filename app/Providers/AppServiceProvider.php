<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\User;
use App\Organization;
use App\Course;
use App\Training;
use App\Category;
use App\Material;
use App\Comment;
use App\Instructor;
use App\Logo;
use App\Transaction;
use App\Resource;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //View component to have these variable available to the admin template

        view()->composer('_admin', function ($view) {
            $view->with('users', User::all())
                ->with('organizations', Organization::all())
                ->with('courses', Course::all())
                ->with('trainings', Training::all())
                ->with('categories', Category::all())
                ->with('materials', Material::all())
                ->with('comments', Comment::all())
                ->with('instructors', Instructor::all())
                ->with('logos', Logo::all())
                ->with('transactions', Transaction::all())
                ->with('resources', Resource::all());

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
