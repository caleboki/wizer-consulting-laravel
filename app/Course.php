<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
	protected $fillable = [
	'title', 'description', 'photo', 'status'
	];

	public function categories()
    {
        return $this->belongsToMany('App\Category', 'course_category', 'course_id', 'category_id');
    }

    public function trainings()
    {
    	return $this->belongsToMany('App\Training', 'course_training', 'course_id', 'training_id');
    }
    public function filters()
	{
		return $this->belongsToMany('App\Filter', 'course_filter', 'course_id', 'filter_id');
	}

    public function materials()
    {
        return $this->belongsToMany('App\Material', 'course_material');
    }
}
