<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    
    protected $fillable = [
		'title', 'venue', 'description', 'cost', 'start_date', 'end_date'
	];

    public function courses()
    {
    	return $this->belongsToMany('App\Course', 'course_training');
    }

    public function organizations()
    {
    	return $this->belongsToMany('App\Organization', 'organization_training', 'training_id', 'organization_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'training_user', 'training_id', 'user_id');
    }

    public function instructors()
    {
        return $this->belongsToMany('App\Instructor', 'instructor_training', 'training_id', 'instructor_id');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
}
