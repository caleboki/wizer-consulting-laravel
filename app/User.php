<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use LaratrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'organization_id', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function organization() 
    {
        return $this->belongsTo('App\Organization');
    }

    public function trainings()
    {
        return $this->belongsToMany('App\Training', 'training_user', 'user_id', 'training_id');
    }


    public function transactions()
    {
        return $this->belongsToMany('App\Transaction', 'transaction_user', 'user_id', 'transaction_id');
    }

    public function transaction()
    {
        return $this->hasMany('App\Transaction');
    }
}
