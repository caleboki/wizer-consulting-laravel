<?php

namespace App\Mail;

use App\User;
use App\Transaction;
use App\Training;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerEvent extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $transaction;
    public $training;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Transaction $transaction, Training $training)
    {
        $this->user = $user;
        $this->transaction = $transaction;
        $this->training = $training;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.customer');
    }
}
