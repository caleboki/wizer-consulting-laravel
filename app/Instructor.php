<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model
{
    protected $fillable = [
	'first_name', 'last_name', 'email', 'about'
	];

	public function trainings()
    {
        return $this->belongsToMany('App\Training', 'instructor_training');
    }
}
