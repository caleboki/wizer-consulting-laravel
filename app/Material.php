<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model
{
    protected $fillable = [

        'title', 'description', 'link'
    ];

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'course_material', 'material_id', 'course_id');
    }
}
