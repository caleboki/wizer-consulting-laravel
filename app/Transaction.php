<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    public function users()
    {
    	return $this->belongsToMany('App\User', 'transaction_user', 'transaction_id', 'user_id');
    }

    public function training()
    {
    	return $this->belongsTo('App\Training');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }
}
