
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('file-component', require('./components/FileuploadComponent.vue'));
Vue.component('image-component', require('./components/ImageuploadComponent.vue'));

Vue.component(
  'example-component',
  require('./components/ExampleComponent.vue')
);

Vue.config.devtools = false
Vue.config.debug = false
Vue.config.silent = true

var app = new Vue({
  el: '#app',
  data: {
    permissionType: 'basic',
    resource: '',
    crudSelected: ['create', 'read', 'update', 'delete'],
    password_options: '',
    auto_password: ''
  },
  methods: {
    crudName: function(item) {
      return item.substr(0,1).toUpperCase() + item.substr(1) + " " + app.resource.substr(0,1).toUpperCase() + app.resource.substr(1);
    },
    crudSlug: function(item) {
      return item.toLowerCase() + "-" + app.resource.toLowerCase();
    },
    crudDescription: function(item) {
      return "Allow a User to " + item.toUpperCase() + " a " + app.resource.substr(0,1).toUpperCase() + app.resource.substr(1);
    }
  }
});


var app2 = new Vue({
      
  el: '#app2',
  data: {
    
    save: "Save All",
    registering_for: 'else',
    count: 0,
    steps: 0,

    training_cost: Number(document.getElementById('training_cost').value),

    
    dbtn: true,
    disabled: false,
    summary: false,

    
    delegates: [],

    delegate: 
    {
    first_name: '',
    last_name: '',
    email: '',
    editing: false,
    
    },

    
    allerrors: [],
    success: false,


  },

  computed: {
    total_cost: function() {
      this.count = this.delegates.length;
      var total = this.count * this.training_cost;
      return new Intl.NumberFormat().format(total);
    },

    
  },

  methods: {

    addDelegate() {

    var dataform = new FormData();
    dataform.append('first_name', this.delegate.first_name);
    dataform.append('last_name', this.delegate.last_name);
    dataform.append('email', this.delegate.email);

    axios.post('/delegates/add', dataform).then( response => {
      
      
      this.delegates.push(this.delegate);
      

      this.steps = this.steps + 1; 
      
      
      this.allerrors = [];
      
      this.success = true;

      this.delegate = {

        first_name: '',
        last_name: '',
        email: '',
        editing: false
     
      }

      

     
      
    } ).catch((error) => {
           this.allerrors = error.response.data.errors;
           
           this.success = false;
    });
    
  },

    Finished() {

      
      if (this.count > 0) {

        if (this.delegate.first_name == '' || this.delegate.last_name == '' || this.delegate.email == '' ) 
        {
                
          var delegates = this.delegates;
         
          console.log(this.delegates);
          this.summary = true;           

        }

        else 
        {
          $('#add-delegate').click();
                                
        }

      }
        else 
      {

        $('#add-delegate').click();

      }
    },

    Save() {

      this.disabled = true;
      this.save = "Loading...";

      axios.post('/delegates/persist', this.delegates).then( response => {

        
        this.steps = 0; 
                
        this.allerrors = [];
        
        this.success = true;

        this.delegate = {

          first_name: '',
          last_name: '',
          email: '',
          editing: false
      
      }

    window.location.href = "/purchase";

   
    } ).catch((error) => {
           this.allerrors = error.response.data.errors;
           return console.log(this.allerrors)
           
           this.success = false;
    }); 


    },

    back() {

      
      if (this.steps > 0) {
        this.steps--;
      }
      
      this.dbtn = false;
      this.delegate = {

        first_name: this.delegates[this.steps].first_name,
        last_name: this.delegates[this.steps].last_name,
        email: this.delegates[this.steps].email,
        editing: false
     
      }
      
    },

    foward() {

      
      this.steps++;
      
      if (typeof this.delegates[this.steps] === 'undefined') {

        this.delegate = {

        first_name: '',
        last_name: '',
        email: '',
        editing: false
     
      }
      return this.dbtn = true;
        
      }
      
      console.log(this.delegates[this.steps]);
      this.delegate = {

        first_name: this.delegates[this.steps].first_name,
        last_name: this.delegates[this.steps].last_name,
        email: this.delegates[this.steps].email,
        editing: false
     
      }
      console.log(this.delegate)


    },

    UpdateCreate(){

      this.allerrors = [];
      var dataform = new FormData();
      dataform.append('first_name', this.delegate.first_name);
      dataform.append('last_name', this.delegate.last_name);
      dataform.append('email', this.delegate.email);


      axios.post('/delegates/add', dataform).then( response => {
        
        this.dbtn = true;

        //return console.log(this.steps)
     
        this.delegates[this.steps] = this.delegate;        

        this.delegate = {

          first_name: '',
          last_name: '',
          email: '',
          editing: false
       
        }
       
       
        
      } ).catch((error) => {
             this.allerrors = error.response.data.errors;
             
             this.success = false;

      });  

      console.log(this.steps) 

      


    },

    DeleteCreate(index){

      this.dbtn = true;
      this.delegates.splice(index, 1);

      this.delegate = {

        first_name: '',
        last_name: '',
        email: '',
        editing: false
     
      }
    },

    

    Edit(d) {

      
      this.steps--;

      this.delegate = {

        first_name: this.delegates[this.steps].first_name,
        last_name: this.delegates[this.steps ].last_name,
        email: this.delegates[this.steps].email,
        editing: false
     
      }

      this.dbtn = false;

      this.summary = false;
     
    },

    Delete(d) {

      var index = this.delegates.indexOf(d);
      this.delegates.splice(index, 1);
      


    }



    
  }


    


     
});

var app3 = new Vue({

  el: '#app3',
  data: {

  },

  methods: {

    upload(e) {
      
      const files = e.target.files
        if (files && files.length > 0) {
          this.$emit('input', files[0])
        }

        console.log(e.target.files)
    }

  }

});


var app4 = new Vue({

  el: '#app4'

});

   


