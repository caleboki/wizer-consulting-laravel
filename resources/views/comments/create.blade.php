@extends('_admin')

@section('title', "|Create Comment")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create Comment</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('comments.store')}}" method="post">
		      	@csrf

		      	<div class="form-group">
		          <label>Business Title</label>
		          <input type="text" name="title" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Organization</label>
		          <input type="text" name="organization" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Comment</label>
		          <textarea rows="2" cols="111" name="comment" ></textarea>		          
		        </div>

				
		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

