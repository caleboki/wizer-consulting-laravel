@extends('_admin')
@section('content')
@section('title', "|Comments" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Comments</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('comments.create')}}">Add Comment</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		        <th>Business Title</th>
		        <th>Organization</th>
		        <th>Comment</th>

                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($comments as $comment)

			      <tr>
			      	<td>{{$comment->title}}</td>
			      	<td>{{$comment->organization}}</td>
			      	<td>{{strip_tags(str_limit($comment->comment, 50))}}</td>

			        <td>
			        	<a href="{{ route('comments.edit', $comment->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>

			          	<form class="delete" action="{{action('CommentController@destroy', $comment['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>


			      </tr>
			    @endforeach

		    </tbody>

		</table>
		<div class="text-center">{!! $comments->links() !!}</div>

      </div>

	</div>


@endsection
