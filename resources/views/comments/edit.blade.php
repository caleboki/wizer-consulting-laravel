@extends('_admin')

@section('title', "|Edit Comment")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Comment</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('CommentController@update', $comment['id'])}}" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">

        		<div class="form-group">
		          <label>Title</label>
		          <input type="text" name="title" value="{{ $comment->title }}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Organization</label>
		          <input type="text" name="organization" value="{{ $comment->organization }}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Comment</label>
		          <textarea rows="2" cols="106" name="comment" required>{{$comment->comment}}</textarea>		          
		        </div>


		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

