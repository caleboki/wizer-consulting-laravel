<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin Area | Dashboard</title>
    <!-- Bootstrap core CSS -->
    <!-- <link href="assets/css/bootstrap.css" rel="stylesheet"> Glypicon not working-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="/assets/css/admin/style.css" rel="stylesheet">
    <link href="/assets/css/admin/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="/assets/css/toastr.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">

    <script src="http://cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>
      tinymce.init({ 
      selector:'textarea',
      plugins: "lists"
      });
    </script>
  </head>
  <body>

    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Admin</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            
            <li><a href="{{route('users.index')}}">Users</a></li>
            <li><a href="{{route('organizations.index')}}">Organizations</a></li>
            <li><a href="{{route('trainings.index')}}">Training</a></li>
            <li><a href="{{route('categories.index')}}">Categories</a></li>
            <li><a href="{{route('courses.index')}}">Courses</a></li>
            <li><a href="{{route('materials.index')}}">Materials</a></li>
            <li><a href="{{route('comments.index')}}">Comments</a></li>
            
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Welcome {{ Auth::user()->first_name }} </a></li>
            <a href="{{ route('logout') }}" class="cs-logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>

          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <header id="header">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Wizer Consulting <small>Dashboard</small></h1>
          </div>
          
        </div>
      </div>
    </header>


    <section id="main">
      <div class="container" id="app">
        @include('_errors')
        @include('flash')
        <div class="row">
          <div class="col-md-3">
            <div class="list-group">
              <a href="index.html" class="list-group-item active main-color-bg">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Dashboard
              </a>
              {{--  <a href="{{route('users.index')}}" class="list-group-item"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Users <span class="badge">{{count($users)}}</span></a>  --}}
              <a href="{{route('organizations.index')}}" class="list-group-item"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Organizations <span class="badge">{{count($organizations)}}</span></a>
              <a href="{{route('trainings.index')}}" class="list-group-item"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Trainings <span class="badge">{{count($trainings)}}</span></a>
              <a href="{{route('instructors.index')}}" class="list-group-item"><span class="glyphicon glyphicon-education" aria-hidden="true"></span> Instructors <span class="badge">{{count($instructors)}}</span></a>
              <a href="{{route('categories.index')}}" class="list-group-item"><span class="glyphicon glyphicon-sort-by-attributes" aria-hidden="true"></span> Categories <span class="badge">{{count($categories)}}</span></a>
              <a href="{{route('courses.index')}}" class="list-group-item"><span class="glyphicon glyphicon-education" aria-hidden="true"></span> Courses <span class="badge">{{count($courses)}}</span></a>
              <a href="{{route('materials.index')}}" class="list-group-item"><span class="glyphicon glyphicon-book" aria-hidden="true"></span> Materials <span class="badge">{{count($materials)}}</span></a>
              <a href="{{route('comments.index')}}" class="list-group-item"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Comments <span class="badge">{{count($comments)}}</span></a>
              <a href="{{route('logos.index')}}" class="list-group-item"><span class="glyphicon glyphicon-picture" aria-hidden="true"></span> Logos <span class="badge">{{count($logos)}}</span></a>

              <a href="{{route('transactions.index')}}" class="list-group-item"><span class="glyphicon glyphicon-euro" aria-hidden="true"></span> Transactions <span class="badge">{{count($transactions)}}</span></a>
              <a href="{{route('permissions.index')}}" class="list-group-item"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Permissions</a>
              <a href="{{route('roles.index')}}" class="list-group-item"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> Roles</a>
              
              
            </div>

      
          </div>
          @yield('content')
        </div>
      </div>
    </section>

    <footer id="footer" style=" bottom: 0; width: 100%;">
      <p>© {{ Carbon\Carbon::now()->year }} Wizer Consulting All Rights Reserved.</p>
    </footer>

    <!-- Modals -->

    <!-- Add Page -->
    <div class="modal fade" id="addPage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <form>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Add Page</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Page Title</label>
                <input type="text" class="form-control" placeholder="Page Title">
              </div>
              <div class="form-group">
                <label>Page Body</label>
                <textarea name="editor1" class="form-control" placeholder="Page Body"></textarea>
              </div>
              <div class="checkbox">
                <label>
                  <input type="checkbox"> Published
                </label>
              </div>
              <div class="form-group">
                <label>Meta Tags</label>
                <input type="text" class="form-control" placeholder="Add Some Tags...">
              </div>
              <div class="form-group">
                <label>Meta Description</label>
                <input type="text" class="form-control" placeholder="Add Meta Description...">
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          </form>
        </div>
      </div>
    </div>



 

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="/assets/scripts/admin/moment-with-locales.min.js"></script>
    <script src="/assets/scripts/admin/bootstrap.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="/assets/scripts/admin/bootstrap-datetimepicker.min.js"></script>
    
    

    <script>
      CKEDITOR.replace( 'editor1' );

      $('#datetimepicker1').datetimepicker({format:'YYYY-MM-DD HH:mm:ss'});
      $('#datetimepicker2').datetimepicker({format:'YYYY-MM-DD HH:mm:ss'});
    </script>

    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>

    <script>
      $(document).ready( function () {
      $('#table').DataTable();
      } );
    </script>

    

  </body>
</html>
