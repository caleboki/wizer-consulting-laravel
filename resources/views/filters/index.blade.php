@extends('_admin')
@section('content')
@section('title', "|Filters" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Filters</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('filters.create')}}">New Filter</a></button>
      	<table class="table table-striped">
		    <thead>
		      <tr>		        
		        <th>Filter</th>
						<th>Category</th>		       
		        <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($filters as $filter)
		      
			      <tr>
			        <td>{{$filter->title}}</td>	
							@if($filter->category)
								<td>{{$filter->category->title}}</td>
							@else
								<td>No Category</td>
							@endif	
							        
			        <td>
			        	<a href="{{ route('filters.edit', $filter->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>

			        	
			          	<form class="delete" action="{{action('FilterController@destroy', $filter['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf
	        				
	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>

			        	
			      </tr>
			    @endforeach
		    </tbody>
		</table>
      </div>

	</div>


@endsection