@extends('_admin')

@section('title', "|Create Filter")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create Filter</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('filters.store')}}" method="post">
		      	@csrf
        		
		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" required>
		        </div>

						@isset($categories)
		        	
			        {{--  <div class="form-group">
			        	<label>Categories</label>

			        	@foreach($categories as $category)			        	
					        <div class="radio">					        	
			        			<label><input type="radio" name="categories[]" value='{{ $category->id }}'>{{ $category->title }}</label><br>	
									</div>
								@endforeach	
							</div>  --}}

								
								<div class="form-group">
									<label for="sel1">Categories</label>
									<select class="form-control" name="category_id">
										@foreach($categories as $category)

											@if($category->filters['id'] == $category->category_id)
												<option disabled selected value> -- select an option -- </option>
												<option value='{{ $category->id }}'>{{ $category->title }}</option>
											@endif
											{{--  <option value='{{ $category->id }}'>{{ $category->filters['id'] }}</option>  --}}
											
											
										
											{{--  <option value='{{ $category->id }}'>{{ $category->title }}</option>  --}}
											{{--  $category->filters->id   --}}
											{{--  $filter->title  --}}
											
										@endforeach
										
									</select>
								</div>
							

						@endisset


		        @isset($courses)
		        	
			        <div class="form-group">
			        	<label>Courses</label>

			        	@foreach($courses as $course)			        	
					        <div class="checkbox">					        	
			        			<label><input type="checkbox" name="courses[]" value='{{ $course->id }}'>{{ $course->title }}</label><br>	
									</div>
								@endforeach	
							</div>

						@endisset


		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

