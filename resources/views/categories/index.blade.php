@extends('_admin')
@section('content')
@section('title', "|Categories" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Categories</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('categories.create')}}">New Category</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Status</th>
                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($categories as $category)

			      <tr>
			        <td>{{$category->title}}</td>
			        <td>{{$category->status}}</td>
			        <td>
			        	<a href="{{ route('categories.edit', $category->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>


			          	<form class="delete" action="{{action('CategoryController@destroy', $category['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf
	        				<!-- <input type="submit" value="Delete User"> -->
	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		<div class="text-center">{!! $categories->links() !!}</div>
      </div>

	</div>


@endsection
