@extends('_admin')

@section('title', "|Create Category")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create Category</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('categories.store')}}" method="post">
		      	@csrf
        		
		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" required>
		        </div>
		        <div class="form-group">
		          <label>Status</label>
		         
		          <input name="status" type="text" class="form-control" required>
		        </div>

		        

		        <div class="form-group">
		        	<label>Attach to a Course</label>
		        	@foreach($courses as $course)
				        <div class="checkbox">
				        	
		        			<label><input type="checkbox" name="courses[]" value='{{ $course->id }}'>{{ $course->title }}</label><br>
		  					
						</div>
					@endforeach
				</div>
		        
		        

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

