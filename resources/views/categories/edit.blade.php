@extends('_admin')

@section('title', "|Edit Category")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Category</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('CategoryController@update', $category['id'])}}" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">
		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" value="{{$category->title}}" required>
		        </div>
		        <div class="form-group">
		          <label>Status</label>
		         
		          <input name="status" type="text" class="form-control" value="{{$category->status}}" required>
		        </div>

		        <div class="form-group">
		        	<label>Courses</label>
		        	
		        	@foreach($courses as $course)
		        		<div class="checkbox">
					    	

					        <label>
					        	<input type="checkbox" name="courses[]" value='{{ $course->id }}'
					        	@if( count($category->courses->where('id', $course->id)) )
					            	checked="1"
					        	@endif
					        	>
					        	{{ $course->title }}
					        </label>
					        <br>
				    	</div>
				 	@endforeach
				</div>
		        

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

