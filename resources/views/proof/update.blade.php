@extends('proof._main')

<div class="container" style="margin-top: 100px;">
    <h2>Payment Options</h2>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Option 1: {{ __(' Bank Transfer') }}</div>

                <div class="panel-body text-center" id="app3">
                    <strong>Account Name:</strong>Wizer Consulting Services<br>
                    <strong>Bank Name:</strong> Guaranty Trust Bank<br>
                    <strong>Account Number:</strong> 0126543406<br>
                    <strong>Sort Code: 058152094</strong><br>                  
                    
                    <strong>Amount: &#8358;{{number_format($training_cost)}}</strong><br><br>                   
                    
                    <br>
  
                    Please quote the following unique code in the remark section of your bank transfer : <strong style="color: red;">{{ $transaction->narration}}</strong><br>
              
                    <br>

                    <file-component narration="{{ $transaction->narration }}"></file-component>

                </div>
            </div>
        </div>
    </div>
    
    <div class="row justify-content-center">
        <div class="col-md-12"><br>
            <div class="text-center">
                <hr>
            </div>
        </div>
    </div>
    <br>
    
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Option 2: {{ __('Debit/Credit Card') }}</div>

                <div class="panel-body">
                    <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8">
                        @csrf

                        <input type="hidden" name="email" value="{{ $user->email }}"> {{-- required --}}
                        <input type="hidden" name="orderID" value="345">
                        <input type="hidden" name="amount" value="{{ $training_cost * 100}}"> {{-- required in kobo --}}
                        <input type="hidden" name="quantity" value="3">
                        <input type="hidden" name="metadata" value="{{ json_encode($array = ['key_name' => 'value',]) }}" > {{-- For other necessary things you want to add to your payload. it is optional though --}}
                        <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                        <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> {{-- required --}}

                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">{{ __('Event Name') }}</label>

                            <div class="col-md-6">

                                <input id="training_title" type="text" class="form-control" name="training_title" value="{{ $training_title }}" readonly>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Amount') }} (&#8358;)</label>

                            <div class="col-md-6">
                            
                                <input id="training_cost" type="text" class="form-control" name="training_cost" value="{{ number_format($training_cost) }}" readonly>
                                 
                            </div>
                        </div> 
                        <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-12 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block center-block">
                                    {{ __('Pay Now') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


{{--  Footer start  --}}

<footer id="footer" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    text-align: center;"> 


    <div class="cs-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="copyright-text">
                        <!-- <p>© 2018 Wizer Consulting All Rights Reserved.</p> -->
                        <p>© {{ Carbon\Carbon::now()->year }} Wizer Consulting All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="cs-social-media">
                        <ul>
                            <li><a href="index.html#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-instagram2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-youtube3"></i></a></li>
                            <li><a href="index.html#"><i class="icon-linkedin22"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>


{{-- Footer end --}}