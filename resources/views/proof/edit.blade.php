@extends('proof._main')

<div class="container" style="margin-top: 100px;">

    <form action="/delegates/transactions/update" method="POST">
        @csrf
        <div class="form-group">
            {{--  <b><small style="color: red;" v-if="error.message">{{error.message}}</small></b>  --}}
            @include('_errors')
            <label for="narration">Enter your Narration code</label>
            <input type="text" class="form-control" id="narration" name="narration">
            <br>
            <button type="submit" class="btn btn-primary btn-block center-block">Submit</button>
        </div>
        <br>
        
    </form>

</div>

{{--  Footer start  --}}
<footer id="footer" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    text-align: center;"> 


    <div class="cs-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="copyright-text">
                        <!-- <p>© 2018 Wizer Consulting All Rights Reserved.</p> -->
                        <p>© {{ Carbon\Carbon::now()->year }} Wizer Consulting All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="cs-social-media">
                        <ul>
                            <li><a href="index.html#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-instagram2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-youtube3"></i></a></li>
                            <li><a href="index.html#"><i class="icon-linkedin22"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>


{{-- Footer end --}}