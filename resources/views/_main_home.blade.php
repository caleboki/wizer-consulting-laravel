@include('_header')
            
    @yield('content')

    
    
    <script src="/assets/scripts/responsive.menu.js"></script> <!-- Slick Nav js --> 
    <script src="/assets/scripts/chosen.select.js"></script> <!-- Chosen js --> 
    <script src="/assets/scripts/slick.js"></script> <!-- Slick Slider js --> 
    <script src="/assets/scripts/jquery.mCustomScrollbar.concat.min.js"></script> 
    <script src="/assets/scripts/jquery.mobile-menu.min.js"></script><!-- Side Menu js --> 
    <script src="/assets/scripts/counter.js"></script><!-- Counter js --> 

    <!-- Put all Functions in functions.js --> 
    <script src="/assets/scripts/functions.js"></script>

    <!--isotope here...-->
    <script src="/assets/scripts/isotope.pkgd.min.js"></script>

    <script src="/assets/scripts/wizer_training-2.js"></script>

    <script src="/assets/scripts/owl.carousel.min.js"></script>

    <script>
        $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        autoplay:true,
        autoplayTimeout:3000,
    autoplayHoverPause:true,
        responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
        }
        })
    </script>

     <script type="text/javascript">
        const app3 = new Vue({
        el: '#login-modal',


        data: {
            form: {
                email : '',
                password : '',
            },
            allerrors: [],
            success : false,    
        },
        methods : {
            onSubmit() {


                dataform = new FormData();
                dataform.append('email', this.form.email);
                dataform.append('password', this.form.password);
                


                axios.post('/login', dataform).then( response => {
                    console.log(response);
                    this.allerrors = [];
                    this.form.email = '';
                    this.form.password = '';
                    this.success = true;
                    window.location.href = "/"
                } ).catch((error) => {
                         this.allerrors = error.response.data.errors;
                         this.success = false;
                    });
            }
        }
    });
    </script>
</body>
</html>
