@extends('_main')



<!-- Sub Header Start -->  
<div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="cs-page-title">
                    <h1>{{Auth::user()->first_name}}</h1>
                    <p style="color:#aaa;">650+ video-based courses and short courses to help you develop creative and technical skills.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Sub Header End --> 

<!-- Main Start -->
<div class="main-section">
    <div class="page-section">
        <div class="container">
            <div class="row">
                <div class="page-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="cs-user-sidebar">
                        <div class="cs-profile-pic">
                            <div class="profile-pic">
                                <div class="cs-media">
                                    <figure> <img src="{{ asset($user->avatar) }}" alt="" style="60px" height="60px" style="border-radius: 50%"> </figure>
                                </div>
                            </div>
                            <form action="{{route('home.store')}}" enctype="multipart/form-data" method="post">
                              @csrf
                              <div class="cs-browse-holder"> <em>My Profile Photo</em>
                                <span class="file-input btn-file"> Update Avatar
                                  <input type="file" name="avatar">

                                </span><br><br>
                                <button type="submit" class="btn btn-file btn-sm">Save</button>

                              </div>

                            </form>
                        </div>
                        <div class="cs-usser-account-list">
                            <ul>
                                <!-- <li><a href="instructor-detail.html"><i class="icon-user3 cs-color"></i>About me</a></li> -->
                                <li class="active"><a href="instructor-courses.html"><i class="icon-graduation-cap cs-color"></i>My Courses</a></li>
                                <li><a href="instructor-statements.html"><i class="icon-file-text2 cs-color"></i>Statement</a></li>
                                <li><a href="instructor-account-setting.html"><i class="icon-gear cs-color"></i>Profile Setting</a></li>
                            </ul>
                            <a href="{{ route('logout') }}" class="cs-logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="icon-log-out cs-color"></i>Logout</a> </div>
                        </div>
                      </div>
                    <div class="page-content col-lg-8 col-md-8 col-sm-12 col-xs-12">
                      @yield('content')
                    </div>
            </div>
        </div>
    </div>
</div>
<!-- Main End --> 


