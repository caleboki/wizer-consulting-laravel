@extends('_admin')

@section('title', "|Create Course")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create Course</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('courses.store')}}" enctype="multipart/form-data" method="post">
		      	@csrf
        		
		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" required>
		        </div>

		        <div class="form-group">
                    <label for="name">Upload course image</label>
                    <input type="file" name="image"  class="form-control">
                </div>

		        <div class="form-group">
		          <label>Description</label>
		          <textarea rows="2" cols="111" class="form-control" name="description"></textarea>		          
		        </div>

		        

		        <div class="row">
		        	<div class="col-md-6">
				        <div class="form-group">
				        	<label>Category</label>
				        	@foreach($categories as $category)
						        <div class="checkbox">
						        	
				        			<label><input type="checkbox" name="categories[]" value='{{ $category->id }}'>{{ $category->title }}</label><br>
				  					
								</div>
							@endforeach
						</div>
					</div>

					<div class="col-md-6">
				        <div class="form-group">
				        	<label>Materials</label>
				        	@foreach($materials as $material)
						        <div class="checkbox">
						        	
				        			<label><input type="checkbox" name="categories[]" value='{{ $material->id }}'>{{ $material->title }}</label><br>
				  					
								</div>
							@endforeach
						</div>
					</div>
				</div>

				<!-- <div class="form-group">
		        	<label>Filters</label>
		        	@foreach($filters as $filter)
				        <div class="checkbox">
				        	
		        			<label><input type="checkbox" name="filters[]" value='{{ $filter->id }}'>{{ $filter->title }}</label><br>
		  					
						</div>
					@endforeach
				</div> -->

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

