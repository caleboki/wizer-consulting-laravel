@extends('_admin')
@section('content')
@section('title', "|Courses" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Courses</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('courses.create')}}">New Course</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Description</th>
		        <th>Materials</th>

                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($courses as $course)

			      <tr>
			        <td>{{str_limit($course->title, 30)}}</td>
			        <td>{{strip_tags(str_limit($course->description, 50))}}</td>
			        <td>{{$course->materials->pluck('title')->implode(", ")}}</td>

			        <td>
			        	<a href="{{ route('courses.edit', $course->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>


			          	<form class="delete" action="{{action('CourseController@destroy', $course['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		<div class="text-center">{!! $courses->links() !!}</div>
      </div>

	</div>


@endsection
