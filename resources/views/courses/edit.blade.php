@extends('_admin')

@section('title', "|Edit Course")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Course</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('CourseController@update', $course['id'])}}" enctype="multipart/form-data" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">
		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" value="{{$course->title}}" required>
		        </div>

		        <div class="form-group">
                    <label for="name">Upload course image</label>
                    <input type="file" name="image"  class="form-control">
                </div>

		        <div class="form-group">
		          <label>Description</label>
		          <textarea rows="2" cols="106" name="description" required>{{$course->description}}</textarea>
		          
		        </div>
		        
		        
		        
		        <div class="row">
		        	<div class="col-md-4">
		        		<div class="form-group">
		        			<label>Category</label>
		        	
				        	@foreach($categories as $category)
				        		<div class="checkbox">
							    	

							        <label>
							        	<input type="checkbox" name="categories[]" value='{{ $category->id }}'
							        	@if( count($course->categories->where('id', $category->id)) )
							            	checked="1"
							        	@endif
							        	>
							        	{{ $category->title }}
							        </label>
							        <br>
						    	</div>
						 	@endforeach
						</div>
		        	</div>
		        	<div class="col-md-4">
		        		<div class="form-group">
		        			<label>Materials</label>
		        	
				        	@foreach($materials as $material)
				        		<div class="checkbox"> 	

							        <label>
							        	<input type="checkbox" name="materials[]" value='{{ $material->id }}'
							        	@if( count($course->materials->where('id', $material->id)) )
							            	checked="1"
							        	@endif
							        	>
							        	{{ $material->title }}
							        </label>
							        <br>
						    	</div>
						 	@endforeach
						</div>
		        	</div>

		        	<div class="col-md-4">
		        		<div class="form-group">
		        		<label>Filters</label>
		        	
			        	@foreach($filters as $filter)
			        		<div class="checkbox">
						    	

						        <label>
						        	<input type="checkbox" name="filters[]" value='{{ $filter->id }}'
						        	@if( count($course->filters->where('id', $filter->id)) )
						            	checked="1"
						        	@endif
						        	>
						        	{{ $filter->title }}
						        </label>
						        <br>
					    	</div>
					 	@endforeach
						</div>
		        	</div>
		        </div>

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

