@extends('_admin')

@section('title', "|Edit Organization")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Organization</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('OrganizationController@update', $organization['id'])}}" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">

        		<div class="form-group">
		          <label>Name</label>
		          <input type="text" name="name" value="{{$organization->name}}" class="form-control" required>
						</div>
						
						<div class="form-group">
		          <label>Industry</label>
		          <input name="industry" type="text" value="{{$organization->industry}}" class="form-control">
		        </div>

		        
		        <div class="form-group">
		          <label>Code</label>
		          <input type="text" name="code" value="{{$organization->code}}" class="form-control" required>
		        </div>

		        

				<div class="form-group">
		        	<label>Trainings</label>
		        	
		        	@foreach($trainings as $training)
		        		<div class="checkbox">
					    	

					        <label>
					        	<input type="checkbox" name="trainings[]" value='{{ $training->id }}'
					        	@if( count($organization->trainings->where('id', $training->id)) )
					            	checked="1"
					        	@endif
					        	>
					        	{{ $training->title }}
					        </label>
					        <br>
				    	</div>
				 	@endforeach
				</div>


		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

