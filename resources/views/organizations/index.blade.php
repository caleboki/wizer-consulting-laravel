@extends('_admin')
@section('content')
@section('title', "|Organizations" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Organizations</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('organizations.create')}}">New Organization</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		        <th>Name</th>
		        <th>Code</th>
		        <th>Training</th>
		        <th></th>
		        <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($organizations as $organization)

			      <tr>
			        <td>{{$organization->name}}</td>
			        <td>{{$organization->code}}</td>
			        <td>{{str_limit($organization->trainings()->pluck('title')->implode(', '), 50)}}</td>

			        <td>
			        	<a href="{{ route('organizations.edit', $organization->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>


			          	<form class="delete" action="{{action('OrganizationController@destroy', $organization['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		<div class="text-center">{!! $organizations->links() !!}</div>
      </div>

	</div>


@endsection
