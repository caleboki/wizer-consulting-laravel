@extends('_admin')

@section('title', "|Add Organization")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Add Organization</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('organizations.store')}}" method="post">
		      	@csrf

		        <div class="form-group">
		          <label>Name</label>
		          <input name="name" type="text" class="form-control" required>
						</div>

						<div class="form-group">
		          <label>Industry</label>
		          <input name="industry" type="text" class="form-control">
		        </div>

		        <div class="form-group">
		          <label>Code</label>
		          <input name="code" type="text" class="form-control" required>
		        </div>



		        <div class="form-group">
		        	<label>Training</label>
                    @foreach($trainings as $training)
                        @if(strtotime($training->end_date) - time() > 0 )
                            <div class="checkbox">

                                <label><input type="checkbox" name="trainings[]" value='{{ $training->id }}'>{{ $training->title }} <div>({{ \Carbon\Carbon::parse($training->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}})</div></label><br>

                            </div>
                        @endif
					@endforeach
				</div>


		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>

</div>


@endsection

