<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="_token" content="{{csrf_token()}}" />

        <title>Wizer Consulting</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="/assets/css/bootstrap.css" rel="stylesheet">
        <link href="/assets/css/bootstrap-theme.css" rel="stylesheet">
        <link href="/assets/css/iconmoon.css" rel="stylesheet">
        <link href="/assets/css/chosen.css" rel="stylesheet">
        <link href="/assets/css/jquery.mobile-menu.css" rel="stylesheet">
        <link href="/assets/css/style.css" rel="stylesheet">
        <link href="/assets/css/cs-smartstudy-plugin.css" rel="stylesheet">

        <link href="/assets/css/cs-smartstudy-event-plugin.css" rel="stylesheet">

        <link href="/assets/css/color.css" rel="stylesheet">
        <link href="/assets/css/widget.css" rel="stylesheet">
        <link href="/assets/css/responsive.css" rel="stylesheet">
        <link href="/assets/css/fontawesome-all.css" rel="stylesheet">
        <link href="/assets/css/owl.carousel.min.css" rel="stylesheet">
        <link href="/assets/css/owl.theme.default.css" rel="stylesheet">
        <link href="/assets/css/custom.css" rel="stylesheet">
        

        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
        <script src="{{asset('js/app.js')}}"></script>



        <script src="/assets/scripts/jquery.js"></script>
        <script src="/assets/scripts/modernizr.js"></script>
        <script src="/assets/scripts/bootstrap.min.js"></script>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

       

         

        
    </head>
    <body class="wp-smartstudy">


        <div class="wrapper" id="app">
             
            <div id="overlay"></div>

            <div id="mobile-menu">
                <ul>
                    <li>
                        <div class="mm-search">
                            <form id="search" name="search">
                                <div class="input-group">
                                    <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
                            <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="icon-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="/#courses">Courses</a>
                        
                    </li>
                    
                    <li><a href="{{route('events.index')}}">Events</a>
                        
                    </li>

                    @if(Auth::check())
                        <li><a href="{{route('home')}}">Profile</a></li>
                    @endif
                    
                    <li><a href="{{route('contact')}}">Contact</a>
                        
                    </li>
                    
                </ul>
            </div>

            <header>
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                        
                            <a href="/" class="navbar-left"><img src="/assets/images/wize_logo.jpg" style="width: 100px; margin-top: 5px;"></a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="/">HOME</a></li>
                                <li><a href="/#courses">COURSES</a></li>
                                <li><a href="{{route('events.index')}}">EVENTS</a></li>
                                @if(Auth::check())
                                    <li><a href="{{route('home')}}">PROFILE</a></li>
                                @endif
                                
                                <li><a href="{{route('contact')}}">CONTACT</a></li>

                                @guest
                                    <li><a data-toggle="modal" data-target="#login-modal"><!-- <i class="icon-login"></i> -->LOGIN</a></li>
                                    <li><a class="nav-link" href="{{ route('register') }}">{{ __('REGISTER') }}</a></li>
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre style="text-transform: uppercase;">WELCOME {{ Auth::user()->first_name }} 
                                        <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            @role('superadministrator|administrator') 
                                                <a class="dropdown-item" href="/admin">Admin</a> 
                                                <br>
                                            @endrole
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                            
                                            

                                        </div>

                                        
                                        
                                            
                                        

                                
                                    </li>
                                    
                                @endguest
                                
                            
                            </ul>
                            <!-- <ul class="nav navbar-nav navbar-right">
                                
                            
                                @guest
                                    <li><a data-toggle="modal" data-target="#login-modal"><i class="icon-login"></i>Login</a></li>
                                    <li><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Welcome {{ Auth::user()->first_name }} 
                                        <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>


                                        </div>

                                
                                    </li>
                                @endguest
                            </ul> -->
                        </div>
                    </div>
                </nav>

                

                <!-- Modal HTML Markup -->
                <div id="login-modal" class="modal fade">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <h1 class="modal-title">Login</h1>
                            </div>
                            <div class="modal-body">
                                <div class="cs-login-form">
                                    <form role="form" method="POST" action="/login" @submit.prevent="onSubmit">
                                        <div class="input-holder">
                                            {{ csrf_field() }}
                                            <div :class="['form-group', allerrors.email ? 'has-error' : '']">
                                                <label for="email" class="control-label">E-Mail Address</label>
                                                <div>
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus v-model="form.email">
                                                    <span v-if="allerrors.email" :class="['label label-danger']">@{{ allerrors.email[0] }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-holder">
                                            <div :class="['form-group', allerrors.password ? 'has-error' : '']">
                                                <label for="password" class="control-label">Password</label>
                                                <div>
                                                    <input id="password" type="password" class="form-control" name="password" v-model="form.password">
                                                    <span v-if="allerrors.password" :class="['label label-danger']">@{{ allerrors.password[0] }}</span>
                                                </div>
                                            </div>

                                        </div>
                                        
                     
                                        <div class="form-group">
                                            <div>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="remember"> Remember Me
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                     
                                        <div class="form-group">
                                            <div>
                                                <button type="submit" class="btn btn-primary" id="ajaxSubmit">
                                                    Login
                                                </button>
                                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                                    Forgot Your Password?
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>    
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <!-- <div id="login-register" class="modal fade">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <h1 class="modal-title">Create Account</h1>
                            </div>
                            <div class="modal-body">
                                <div class="cs-login-form">
                                    <form role="form" method="POST" action="{{ url('/register') }}">
                                        
                                        <h5>Are you sponsored by an Organization?</h5>
                                        <label class="radio-inline"><input type="radio" name="optradio">No</label>
                                        <label class="radio-inline"><input type="radio" name="optradio">Yes</label><br><br>

                                        <div class="form-group">
                                          <div class="">
                                            <div class="form-inline">
                                              <div class="form-group ">
                                                <div class="">

                                                  <label for="org_name">Organization Name</label> 
                                                  <input type="text" id="modal_org_name" name="org_name" class="form-control ">
                                                </div>
                                              </div> 
                                              <div class="form-group ">
                                                <div>
                                                  <label for="county">Code</label> 
                                                  <input type="text" id="county" name="county" class="form-control">
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="form-group">
                                          <div class="">
                                            <div class="form-inline">
                                              <div class="form-group ">
                                                <div class="">

                                                  <label for="name">First Name</label> 
                                                  <input type="text" id="org_name" name="firstName" class="form-control ">
                                                </div>
                                              </div> 
                                              <div class="form-group ">
                                                <div>
                                                  <label for="county">Last Name</label> 
                                                  <input type="text" id="lastName" name="lastName" class="form-control">
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="form-group">
                                          <div class="">
                                            <div class="form-inline">
                                              <div class="form-group ">
                                                <div class="">

                                                  <label for="email">Email</label> 
                                                  <input type="text" id="modal_email" 
                                                  name="email" class="form-control">
                                                </div>
                                              </div> 
                                              
                                            </div>
                                          </div>
                                        </div>

                                        
                                        
                                         
                                       
                     
                                        
                                    </form>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- /.modal -->
                
            </header>
            <br>
            <br>
            