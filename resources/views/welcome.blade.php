
@extends('_main')

@section('title', "|Edit Course")
@section('content')
{{--  Banner Start  --}}


<div class="page-section gradient" style="background: url(assets/extra-images/{{ randImage() }}) no-repeat; background-size: cover; padding: 50px 0; position: relative;">
    <div class="glass"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="cs-column-text">
                    <span style="display:inline-block;padding:10px 20px;background:rgba(0,0,0,0.8);color:#FFF;font-size:18px;margin-bottom:22px;">What would you like to learn?</span>
                    <h1 style="color:#ffffff !important;line-height:64px !important;text-transform:capitalize !important;">Wizer Academy</h1>
                    <p style="font-size:36px !important;line-height:42px !important;color:#FFF !important;font-weight:400 !important;margin-bottom:30px;">Advance Your Career. Stay Ahead.</p>
                    @if(Auth::check())
                        <a style="font-size:13px;font-weight:700;line-height:19px;padding:16px 28px;border-radius:50px;color:#FFF;text-decoration:none;outline:none;" class="cs-bgcolor" href="{{route('events.index')}}">Sign up now!</a>
                    @else
                        <a style="font-size:13px;font-weight:700;line-height:19px;padding:16px 28px;border-radius:50px;color:#FFF;text-decoration:none;outline:none;" class="cs-bgcolor" href="{{route('register')}}">Sign up now!</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

{{--  Banner End  --}}



{{--  Main Start  --}}
<div class="main-section">
    <div class="page-section" style="background-color:#f9fafa; padding:50px 0;">
        <div class="container">
            <div class="row">
                <div class="section-fullwidtht col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="row cs-counter-holder">
                        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="cs-counter simple center">
                                <div class="cs-text">
                                    <strong class="counter cs-color">{{count($courses)}}</strong>
                                    <span>Courses across different domains</span>

                                </div>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="cs-counter simple center">
                                <div class="cs-text">
                                    <strong class="counter cs-color">{{count($organizations)}}</strong>
                                    <span>Organizations</span>

                                </div>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="cs-counter simple center">
                                <div class="cs-text">
                                    {{--  <strong class="counter cs-color">{{count($users)}}</strong>  --}}
                                    <strong class=" cs-color"> > 1500</strong>
                                    <span>Participants</span>

                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="page-section" style="background:#f9fafa;margin-bottom:82px;">
        <div class="container">
            {{--  Start of Calendar section  --}}
            <div class="page-section">
                <div class="text-center">
                    @include('flash::message')
                </div>


                <div class="cs-section-title left side_bar_left_body">
                    <br>


                    {{--  @include('flash')  --}}


                    <br>
                    <h2>Upcoming Courses</h2>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme">
                            @foreach($trainings as $training)

                                @if(strtotime($training->end_date) - time() > 0 )

                                    <div class="item">
                                        <div class="thumbnail">

                                            <div class="caption custom-caption" >

                                                <h5 class="calendar-header"><a href="events/{{$training->id}}">{{ \Carbon\Carbon::parse($training->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}}</h5></a>

                                                <p><a href="events/{{$training->id}}">{!! $training->title !!}</a></p> <!-- <a href="#" class="btn btn-default btn-xs pull-right" role="button"></a> -->  <!-- <a href="events/{{$training->id}}" class="btn btn-default btn-xs" role="button">More Info</a> -->

                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </div>
                    </div>
                </div>

                <div class="widget cs-text-widget right" >
                    <div class="cs-text" style="padding:0;">
                        <a href="assets/docs/Brochure-2019.pdf" download class="cs-bgcolor"><i class="icon-keyboard_arrow_right"></i> Download Brochure</a>
                    </div>
                </div>


            </div>
            {{--  End of Calendar section  --}}

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="cs-section-title left side_bar_left_body"><br>
                    <a href="#" id="programmes"><h2>Courses</h2></a>

                    </div>
                </div>
                <aside class="section-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="widget cs-widget-links side_bar_left_body" id="filters">
                        <ul>
                            <li><a data-filter="*" id="clear">All</a></li>

                            @foreach($categories as $category)
                                <li><a data-filter=".{{$category->id}}">
                                 {{$category->title}}
                                </a></li>

                            @endforeach


                        </ul>
                    </div>

                </aside>
                <div class="page-content course-list col-lg-9 col-md-9 col-sm-12 col-xs-12 grid" id="courses">
                    <div class="row">
                        @foreach($courses as $course)

                            <!-- <div class="element-item col-lg-4 col-md-4 col-sm-6 col-xs-12 transition {{implode(" ", array_pluck($course->filters->toArray(), 'title'))}}"> -->
                            <div class="element-item col-lg-4 col-md-4 col-sm-6 col-xs-12 transition {{ (sizeof($course->categories) > 0) ? $course->categories[0]->id : '' }}">

                                <div class="cs-courses courses-grid">
                                    <div class="cs-media">
                                        <figure>
                                            <a href="/courses/{{$course->id}}">
                                            @if(is_null($course->image))
                                                <img src="assets/extra-images/course-grid-img{{$course->id}}.jpg" alt=""/>
                                            @else
                                                <img src="{{$course->image}}" alt=""/>

                                            @endif
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="cs-text">

                                        <div class="cs-post-title">
                                        <h5><a href="/courses/{{$course->id}}">{{str_limit($course->title, 32)}}</a></h5>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-section" style="margin-bottom:30px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="cs-element-title" style="margin-bottom:30px;">
                        <h2>What Participants are Saying</h2>
                    </div>
                </div>



                <ul class="cs-teamlist-slider">
                    @foreach($comments as $comment)

                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="cs-team listing loop">

                                <div class="cs-text">
                                    <h5 class="cs-color">{{$comment->title}}</h5>
                                    <span>{{$comment->organization}}</span>
                                    <p>{{$comment->comment}}</p>

                                </div>
                            </div>
                        </li>

                    @endforeach

                </ul>
            </div>
        </div>


    </div>

    <div class="page-section" style="margin-bottom:40px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="cs-fancy-heading" style="margin-bottom:40px;">
                        <h6 style="font-size:14px !important; color:#999 !important; text-transform:uppercase !important;">Our Clients</h6>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="cs-graduate-slider">
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo.png" alt="" style="width: 200px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo2.png" alt="" style="width: 120px; height: 50px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo3.png" alt="" style="width: 100px; height: 50px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo4.png" alt="" style="width: 200px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo5.png" alt="" style="width: 200px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo6.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo7.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo8.png" alt="" style="width: 200px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo9.png" alt="" style="width: 200px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo10.png" alt="" style="width: 200px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo11.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo12.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo13.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo14.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo15.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                        <li>
                            <div class="cs-media">
                                <figure> <img src="assets/extra-images/logo16.png" alt="" style="width: 120px;"/> </figure>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>
{{--  Main end  --}}
@endsection
