@extends('_main_home')

<!-- Sub Header Start -->  <br>
<div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="cs-page-title">
                    <h1>{{Auth::user()->first_name}}</h1>
                    <p style="color:#aaa;">650+ courses to help you develop creative and technical skills.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Sub Header End --> 

<!-- Main Start -->
<div class="main-section">
    <div class="page-section">
        <div class="container">
            <div class="row">
                <div class="page-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="cs-user-sidebar" style="padding-bottom:0px;">
                        <div class="cs-profile-pic" id="app">
                            
								<div class="cs-media">
									<figure> <img src="{{ asset('storage/'.$user->avatar) }}" alt="" style="height:200px; width:200px; "> </figure>
								</div>
									
									
								<image-component></image-component>
                        </div>
                        <div class="cs-usser-account-list">
                            <ul>
                                
                                <li><a href="{{route('home')}}"><i class="icon-graduation-cap cs-color"></i>My Events</a></li>
                                <li><a href="{{route('upcoming')}}"><i class="icon-graduation-cap cs-color"></i>Upcoming Events</a></li>
                            
                                <li class="active"><a href="{{route('setting')}}"><i class="icon-gear cs-color"></i>Profile Setting</a></li>
                            </ul>
                            <a href="{{ route('logout') }}" class="cs-logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="icon-log-out cs-color"></i>Logout</a> </div>
                        </div>
                      </div>
                   

                    <div class="page-content col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div class="cs-user-content cs-instructor">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="cs-section-title">
										<h2>General Settings</h2>
										@include('_errors')
    									<!-- @include('flash') -->
									</div>
								</div>
								<form action="{{action('HomeController@updateSetting')}}" method="post">
									@csrf
									<div class="cs-field-holder">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<label>First Name</label>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="cs-field">
												<input name="first_name" type="text" placeholder=""  value="{{$user->first_name}}">
											</div>
										</div>
									</div>

									<div class="cs-field-holder">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<label>Last Name</label>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="cs-field">
												<input name="last_name" type="text" placeholder="" value="{{$user->last_name}}">
											</div>
										</div>
									</div>
									<div class="cs-field-holder">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<label>Email Address</label>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="cs-field">
												<input name="email" type="text" placeholder="" value="{{$user->email}}">
											</div>
										</div>
									</div>
								

								
									<div class="cs-field-holder">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<h6>Update password</h6>
										</div>
									</div>
									<div class="cs-field-holder">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<label>Password</label>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="cs-field">
												<input name="password" type="password" placeholder="" >
											</div>
										</div>
									</div>
									<div class="cs-field-holder">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<label>Confirm password</label>
										</div>
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="cs-field">
												<input name="password_confirmation" type="password" placeholder="" >
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="cs-seprator"></div>
									</div>
									
									<div class="cs-field-holder">
										<div class="col-lg-3 col-md-3 col-sm-12 col-md-12">
											<div class="cs-field"><div class="cs-btn-submit"><input name="name" type="submit" value="Save Changes" ></div></div>
										</div>
									</div>


								</form>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main End --> 

