@extends('delegates._main')


<div class="wrapper" id="app2">
	<div class="container" style="padding-top: 1.5rem;" v-if="summary == false">    
	            
	    <div id="signupbox" style=" margin-top:50px" class="mainbox ">
	        <div class="panel panel-info">
	            <div class="panel-heading">
	                <div class="panel-title">Training Registration</div>
	                @if (!Auth::check())
		                <div class="panel-title" style="float:right; position: relative; top:-20px; color: red;"><a href="{{route('login')}}">
		                Already have an account?</a></div>
	                @endif
	                
	                
	            </div>  
	            <div class="panel-body" >
	            	@include('_errors')

	            	<!-- Start of self/else form     -->
	                
	                @if(Auth::check())
		                <form class="form-horizontal" method="post" >
		                     
		                    <div class="form-group">
		                        <label  class="control-label col-md-3 "><span style="color: red">*</span>Registering For </label>
		                        <div class="controls col-md-8 "  style="margin-bottom: 10px">
		                            <label class="radio-inline"> <input type="radio" name="registering" v-model="registering_for" value="self" style="margin-bottom: 10px" >Self </label>
		                            <label class="radio-inline"> <input type="radio" name="registering" v-model="registering_for" value="else" style="margin-bottom: 10px">Someone Else </label>
		                        </div>
		                    </div>
		                </form>


	                @endif   

	                <!-- End of self/else form    -->

	                @if(!Auth::check())

		                <form  class="form-horizontal" action="{{route('delegates.store')}}"  method="post">
		                    @csrf

		                    <div class="form-group"> 
		                        <label for="first_name" class="control-label col-md-3"> <span style="color: red">*</span>First Name </label> 
		                        <div class="controls col-md-8 ">
		                        	

		                            <input class="form-control" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" style="margin-bottom: 10px" type="text"   required />

		                            
		                        </div>
		                    </div>

		                    <div class="form-group"> 
		                        <label class="control-label col-md-3"> <span style="color: red">*</span>Last Name </label> 
		                        <div class="controls col-md-8 "> 
		                            

		                            <input class=" form-control" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" style="margin-bottom: 10px" type="text" required/>

		                            
		                        </div>
		                    </div>
		                    
		                    <div class="form-group"> 
		                        <label class="control-label col-md-3 "> <span style="color: red">*</span>Email </label> 
		                        <div class="controls col-md-8 "> 
		                           

		                            <input name="email" placeholder="Email" value="{{old('email')}}" style="margin-bottom: 10px" type="email" />

		                            
		                        </div>
		                    </div>

		                    <div class="form-group"> 
		                        <label class="control-label col-md-3 "> Training </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            <input class="form-control" id="training" name="training" value="{{$training_title or old('training_title') }}" style="margin-bottom: 10px" type="text" disabled />
		                            
		                        </div>
		                    </div>
		                    

		                    <input name="training_id" type="hidden" value="{{$training_id}}">

		                    <input name="training_title" type="hidden" value="{{$training_title}}">

		                    <input name="training_venue" type="hidden" value="{{$training_venue}}">

		                    <input name="training_cost" type="hidden" value="{{$training_cost}}">



		                    <div class="form-group pull-right"> 
		                        <div class="aab controls col-md-4 "></div>
		                        <div class="controls col-md-12 ">
		                            <input type="submit" name="Purchase" value="Next...Payment Options" class="btn btn-primary btn btn-info" />
		                            
		                        </div>
		                    </div> 

		                   
		                </form>


	                @endif

	                @if(Auth::check())

		                <form  class="form-horizontal" method="post" action="{{action('DelegateController@purchase')}}" v-if="registering_for == 'self'">
		                    @csrf
		                    <div class="form-group required"> 
		                        <label for="id_name" class="control-label col-md-3  requiredField"> <span style="color: red">*</span>First Name </label> 

		                        <div class="controls col-md-8 ">
		                        	
		                            <input class="form-control" name="first_name" value="{{Auth::user()->first_name}}" style="margin-bottom: 10px" type="text" readonly />
		                            
		                        </div>
		                    </div>

		                    <div class="form-group required"> 
		                        <label for="id_name" class="control-label col-md-3  requiredField"> <span style="color: red">*</span>Last Name </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            <input class="input-md textinput textInput form-control" id="id_name" name="last_name" value="{{Auth::user()->last_name}}" style="margin-bottom: 10px" type="text" readonly />
		                            
		                        </div>
		                    </div>
		                    
		                    <div class="form-group required"> 
		                        <label for="id_name" class="control-label col-md-3  requiredField"> <span style="color: red">*</span>Email </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            <input class="input-md textinput textInput form-control" name="email" value="{{Auth::user()->email}}" style="margin-bottom: 10px" type="email" readonly />
		                            
		                        </div>
		                    </div>

		                    <div class="form-group required"> 
		                        <label for="training_title" class="control-label col-md-3"> Training </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            <input class="form-control" id="training_title" name="training_title" value="{{$training_title}}" style="margin-bottom: 10px" type="text" readonly />
		                            
		                        </div>
		                    </div>

		                    <div class="form-group required"> 
		                        <label for="id_training" class="control-label col-md-3 "> Cost (&#8358;) </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            	<input class="form-control" id="training_cost" name="training_cost" value="{{$training_cost or old('training_cost') }}" style="margin-bottom: 10px" type="text" readonly />
		                            
		                        </div>
		                    </div>

		                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">

		                    <input name="training_id" type="hidden" value="{{$training_id}}">

		                    <input name="training_title" type="hidden" value="{{$training_title}}">

		                    <input name="training_venue" type="hidden" value="{{$training_venue}}">
		                    <input name="training_cost" type="hidden" value="{{$training_cost}}">

		                    <input type="hidden" name="email" value="{{Auth::user()->email }}">
		                    

		                    <div class="form-group pull-right"> 
		                        <div class="aab controls col-md-4 "></div>
		                        <div class="controls col-md-12 ">
		                            <input type="submit" name="Purchase" value="Next...Payment Options" class="btn btn-primary btn btn-info" id="submit-id-signup" />
		                            
		                        </div>
		                    </div>

		                   
		             
		                </form>

		                <form  class="form-horizontal"  v-if="registering_for == 'else'" @submit.prevent>
		                    @csrf


		                    <input name="user_id" type="hidden" value="{{Auth::user()->id}}">
		                    <input name="training_id" type="hidden" value="{{$training_id}}">

		                    <!-- <input name="training_title" type="hidden" value="{{$training_title}}"> -->

		                    <!-- <input name="training_venue" type="hidden" value="{{$training_venue}}"> -->
		                    <!-- <input name="training_cost" type="hidden" value="{{$training_cost}}"> -->

		                    

		                    <div class="form-group "> 
		                        <label for="first_name" class="control-label col-md-3"> <span style="color: red">*</span>First Name </label> 
		                        <div class="controls col-md-8 ">
		                        	

		                            <input class="form-control" name="first_name" placeholder="First Name" v-model="delegate.first_name" style="margin-bottom: 10px" type="text" />
		                            <span v-if="allerrors.first_name" :class="['label label-danger']">@{{ allerrors.first_name[0] }}</span>

		                            
		                        </div>
		                    </div>

		                    <div id="div_id_name" class="form-group"> 
		                        <label for="last_name" class="control-label col-md-3 "> <span style="color: red">*</span>Last Name </label> 
		                        <div class="controls col-md-8 "> 
		                            

		                            <input class=" form-control" id="id_name" name="last_name" placeholder="Last Name" style="margin-bottom: 10px" type="text" v-model="delegate.last_name" />
		                            <span v-if="allerrors.last_name" :class="['label label-danger']">@{{ allerrors.last_name[0] }}</span>

		                            
		                        </div>
		                    </div>
		                    
		                    <div class="form-group "> 
		                        <label for="id_name" class="control-label col-md-3  required"> <span style="color: red">*</span>Email </label> 
		                        <div class="controls col-md-8 "> 
		                           

		                            <input name="email" placeholder="Email" style="margin-bottom: 10px" type="email" v-model="delegate.email" />
		                            <span v-if="allerrors.email" :class="['label label-danger']">@{{ allerrors.email[0] }}</span>

		                            
		                        </div>
		                    </div>

		                    <div class="form-group required"> 
		                        <label for="id_training" class="control-label col-md-3 "> Training </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            <input class="form-control" id="training_title" name="training_title" value="{{$training_title or old('training_title') }}" style="margin-bottom: 10px" type="text" readonly />
		                            
		                        </div>
		                    </div>

		                   

		                    <div class="form-group required"> 
		                        <label for="id_training" class="control-label col-md-3 "> Cost (&#8358;) </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            <input class="form-control" id="training_cost" name="training_cost" value="{{$training_cost or old('training_cost') }}" style="margin-bottom: 10px" type="text" readonly />
		                            
		                        </div>
		                    </div>
		                    
		                    <div class="form-group required"> 
		                        <label for="id_training" class="control-label col-md-3"> Referenced By </label> 
		                        <div class="controls col-md-8 "> 
		                            
		                            <input class="form-control" id="training" name="training" value="{{Auth::user()->first_name}}" style="margin-bottom: 10px" type="text" readonly />
		                            
		                        </div>
		                    </div>
		                    

		                    

		                    <div class="form-group pull-right"> 
		                        <div class="aab controls col-md-4 "></div>
		                        <div class="controls col-md-12 ">

		                        	<button type="submit" class="btn btn-primary btn btn-info" name="Back" v-if="count > 0 ? true : false" @click="back()">Back</button>

		                        	<button type="submit" class="btn btn-primary btn btn-info" name="Foward" v-if="dbtn == false" @click="foward()">Foward</button>

		                        	<button type="submit" class="btn btn-primary btn btn-info" name="Purchase" id="add-delegate" v-if="dbtn == true" @click="addDelegate">Add Delegate</button>

		                        	<button type="submit" class="btn btn-primary btn btn-info" name="Update" v-if="dbtn == false" @click="UpdateCreate()">Update</button>
		                            
		                            <button type="submit" class="btn btn-primary btn btn-danger" name="Delete" v-if="dbtn == false" @click="DeleteCreate()">Delete</button>
		                            
		                            <button type="submit" class="btn btn-primary btn btn-info" name="Finished" v-if="dbtn == true" @click="Finished()">Finished</button>

		                                  
		                        </div>
		                    </div> 
		             
		                </form>
		                <div class="panel-body" v-if= "registering_for == 'else'"> <h4>Number of Delegates Added: @{{count}}<br> Total cost: &#8358;@{{total_cost}}</h4> </div>
		               

	                @endif

	                
	            </div>
	        </div>
	    </div> 
	    
	</div>

	@if(Auth::check()) 

		<!-- Start of summary section -->
		<div class="container"  style="padding-top: 1.5rem;" v-if="summary == true">
			<div id="signupbox" style=" margin-top:50px" class="mainbox ">
		        <div class="panel panel-info">
		            <div class="panel-heading">
		                <div class="panel-title"><!-- {{session('training_title')}} -->Summary</div>

		            </div>
		            
		             
	                    <div class="aab controls col-md-4 "></div>
	                    <div class="controls col-md-12 ">

	                    	<button type="submit" class="btn btn-primary btn btn-info pull-right" @click="Save()" style="margin-left: 10px;" :disabled="disabled == 1 ? true : false">@{{ save }}</button>
	                    	<!-- <button type="submit" class="btn btn-primary btn btn-danger pull-right" @click="DeleteCreate()" style="margin-left: 10px;">Delete Delegate</button> -->
	                    	<button type="submit" class="btn btn-primary btn btn-info pull-right" @click="summary = false">Add Delegate</button>

	                        
	                    
		            	</div>

		            <div class="panel-body"  v-for="(d, index) in delegates">


		            	<form  class="form-horizontal" @submit.prevent>
			                @csrf


		                    <div class="form-group"> 
		                        <label for="first_name" class="control-label col-md-3"> <span style="color: red">*</span>First Name </label> 
		                        <div class="controls col-md-8 ">
		                        	

		                            <input class="form-control" name="first_name" v-model="d.first_name" style="margin-bottom: 10px" type="text" :readonly="d.editing == 0 ? true : false" />

		                            
		                        </div>
		                    </div>

		                    <div class="form-group"> 
		                        <label class="control-label col-md-3"> <span style="color: red">*</span>Last Name </label> 
		                        <div class="controls col-md-8 "> 
		                            

		                            <input class=" form-control" name="last_name" v-model="d.last_name" value="" style="margin-bottom: 10px" type="text" :readonly="d.editing == 0 ? true : false" />

		                            
		                        </div>
		                    </div>
			                    
		                    <div class="form-group"> 
		                        <label class="control-label col-md-3 "> <span style="color: red">*</span>Email </label> 
		                        <div class="controls col-md-8 "> 
		                           

		                            <input name="email" v-model="d.email" value="" style="margin-bottom: 10px" type="email" :readonly="d.editing == 0 ? true : false" />

		                            
		                        </div>
		                    </div>

		                    <div class="form-group"> 
		                        <label class="control-label col-md-3 "> Training Cost </label> 
		                        <div class="controls col-md-8 "> 
		                           

		                            <input name="training_cost" value="{{$training_cost}}" style="margin-bottom: 10px" type="text" disabled />

		                            
		                        </div>
		                    </div>

		                    <div class="form-group pull-right"> 
		                        <div class="aab controls col-md-4 "></div>
		                        <div class="controls col-md-12 ">


		                            
		                            <button type="submit" class="btn btn-primary btn btn-info" v-if="d.editing == false" @click="Edit(d)">Edit Delegate</button>

		                            <button type="submit" class="btn btn-primary btn btn-info" v-if="d.editing == true" @click="UpdateSummary(d)">Update</button>

		                            <button type="submit" class="btn btn-primary btn btn-danger" @click="Delete(d)">Delete Delegate</button>
		                            
		                        </div>
		                    </div>
		   
			            </form>


		            </div>


		            
		        </div>
		    </div>

		</div>
		<!-- End of summary section -->
	@endif

</div>
    
         
{{--  Footer start  --}}
<footer id="footer" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    text-align: center;"> 


    <div class="cs-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="copyright-text">
                        <!-- <p>© 2018 Wizer Consulting All Rights Reserved.</p> -->
                        <p>© {{ Carbon\Carbon::now()->year }} Wizer Consulting All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="cs-social-media">
                        <ul>
                            <li><a href="index.html#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-instagram2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-youtube3"></i></a></li>
                            <li><a href="index.html#"><i class="icon-linkedin22"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>


{{-- Footer end --}}