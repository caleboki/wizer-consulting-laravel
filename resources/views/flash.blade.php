@if (session('success'))
    <div class="alert alert-success" style="text-align: center;">{{ session('success') }}</div>
@endif