@extends('_main')

@section('title', "|Course")
@section('content')

<div class="wrapper">
	<!-- Side Menu Start -->
	<div id="overlay"></div>
    <div id="mobile-menu">
        <ul>
            <li>
                <div class="mm-search">
                    <form id="search" name="search">
                        <div class="input-group">
                            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
					   <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="icon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <li><a href="index.html">Home</a></li>
			<li class="active"><a href="/#courses">Courses</a>

			</li>

			<li><a href="{{route('events.index')}}">Events</a>

			</li>

			<li><a href="{{route('contact')}}">Contact</a>

			</li>

        </ul>
    </div>
	<!-- Side Menu End -->

	<!-- Sub Header Start -->
	<div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="cs-page-title">
						<h1>Course Detail</h1>
						<p style="color:#aaa;">Stay ahead...Enhance your technical skills through our world class training programs</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Sub Header End -->
	<br><br>
	<!-- Main Start -->
	<div class="main-section">
	  <div class="page-section">
	    <div class="container">
	      <div class="row">
	        <aside class="page-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div >
                	@if(!is_null($course->image))
						<img src="/{{ $course->image }} " style="width: 100%; margin-top: 20px; height: 200px; object-fit: cover;">
					@else
						<img src="/assets/images/BasicExcel.jpg " style="width: 100%; margin-top: 20px; height: 200px; object-fit: cover;">

					@endif

					@if ($course->trainings->count() > 0)
						@if (!is_null($course->trainings[0]->expectations_1))
							<div class="well well-sm" style="margin-top: 20px;">
								{!! formatList($course->trainings[0]->expectations_1) !!}
							</div>
						@endif

						@if (!is_null($course->trainings[0]->expectations_1))
							<div class="well well-sm" style="margin-top: 20px">
								{!! formatList($course->trainings[0]->expectations_2) !!}
							</div>
						@endif

					@endif


					<div class="well well-sm" style="margin-top: 20px">
						All modules are presented with
						extensive hands-on exercises/case
						studies that are designed to allow
						delegates apply concepts learned.
					</div>



                </div>





            </aside>
	        <div class="page-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
	          <div class="row">
	            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div id="cs-about" class="cs-about-courses">
						<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 		<div class="cs-section-title"><h3>{!! $course->title !!}</h3></div>

						<P>{!! formatList($course->description) !!}</p>
					</div>



				</div>
			</div>

		</div>

		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
	    	<div >
	    		<div class="well well-sm" style="margin-top: 20px">


						<p><h4>Upcoming events</h4></p>


						@if ($course->trainings->count() > 0)
							<ul style="margin-top: -15px; list-style: disc;">

								@foreach($course->trainings as $training)

									<li><div style="margin-left: 14px; margin-top: -30px;"><a href="/events/{{$training->id}}">
										<em> {{ \Carbon\Carbon::parse($training->start_date)->format('d M') }} - {{ \Carbon\Carbon::parse($training->end_date)->format('d M') }}</em></a></div>
									</li>
								@endforeach
								<br>
							</ul>

						@else
							<em>No upcoming events at this time</em>
						@endif



				</div>
	    		<div class="widget cs-text-widget center" >
                    <div class="cs-text" style="padding:0;">
                        <a href="/assets/docs/Brochure-2019.pdf" download class="cs-bgcolor" style="width: 270px;"><i class="icon-keyboard_arrow_right"></i> Download Brochure</a>
                    </div>
                </div>

                <div class="well well-sm" style="margin-top: 20px">


					<p>We offer discounts for group bookings (i.e group of delegates attending the same course at the same time) as follows:</p>



					<ul style="margin-top: -15px">

						3-5 participants - 5% discount<br>
						6-9 participants - 7.5% discount<br>
						10 or more participants - 10% discount<br>
					</ul>



				</div>







					<!-- <div class="well well-sm materials" style="margin-top: 20px; border-width: 4px; border-radius: 14px; border:5px solid #e3e3e3;">





					</div> -->

				</div>


	    	</div>


    	</div>
	</div>
</div>
</div>
</div>

	</div>
	<!-- Main End -->

</div>

@endsection
