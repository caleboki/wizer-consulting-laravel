
@extends('_main_home')

<!-- Sub Header Start -->  <br>
<div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="cs-page-title">
                    <h1>{{Auth::user()->first_name}}</h1>
                    <p style="color:#aaa;">650+ courses to help you develop creative and technical skills.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Sub Header End --> 

<!-- Main Start -->
<div class="main-section">
    <div class="page-section">
        <div class="container">
            <div class="row">
                <div class="page-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="cs-user-sidebar" style="padding-bottom:0px;">
                        <div class="cs-profile-pic" id="app">
                            
                            <div class="cs-media">
                                <figure> <img src="{{ asset('storage/'.$user->avatar) }}" alt="" style="height:200px; width:200px; "> </figure>
                            </div>
                            
                            
                            <image-component></image-component>
                        </div>
                        <div class="cs-usser-account-list">
                            <ul>
                                
                                <li><a href="{{route('home')}}"><i class="icon-graduation-cap cs-color"></i>My Events</a></li>
                                <li class="active"><a href="#"><i class="icon-graduation-cap cs-color"></i>Upcoming Events</a></li>
                            
                                <li><a href="{{route('setting')}}"><i class="icon-gear cs-color"></i>Profile Setting</a></li>
                            </ul>
                            <a href="{{ route('logout') }}" class="cs-logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="icon-log-out cs-color"></i>Logout</a> </div>
                        </div>
                      </div>
                   
                    <div class="page-content col-lg-8 col-md-8 col-sm-12 col-xs-12">
						<div class="cs-user-content cs-instructor">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="cs-section-title">
										<h2>Upcoming Events</h2>
										
									</div>
								</div>
								
								@foreach($trainings as $training)
									@if(strtotime($training->end_date) - time() > 0 )
							
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="cs-event list">
												<div class="cs-media">
													<figure><a href="events/{{$training->id}}"><img src="/{{$training->image}}" alt="" /></a><figcaption>{{ \Carbon\Carbon::parse($training->start_date)->format('d M')}} to {{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}}</figcaption></figure>
												</div>
												<div class="cs-text">
													<div class="cs-post-title">
														
														<h3><a href="events/{{$training->id}}">{!! $training->title !!}</a></h3>
													</div>
													<span class="cs-location"><i class="cs-color icon-location-pin"></i>{{ strip_tags($training->venue) }}</span>
													<p>{!!str_limit($training->outcome, 200) !!}</p>
													
													<!-- <form action="{{action('EventController@store')}}" method="post">
														@csrf
														<div class="cs-event-price">
															<span>Price<em class="cs-color"><sup> &#8358;</sup>{{$training->cost}}</em></span>
															
															
															@if(!Auth::check())
																
																<a href="{{route('register')}}">REGISTER NOW</a>

															@elseif(isset($user) AND count($user->trainings->where('id', $training->id)))

																<button type="submit" class="btn btn-success " disabled>ALREADY REGISTERED</button>

															@else

																<input name="training_id" type="hidden" value="{{$training->id}}">

																<button type="submit" class="btn btn-success">REGISTER NOW</button>

															
															@endif
															
														</div>
													</form> -->

													@if(!Auth::check())

														<form action="{{action('EventController@store')}}" method="post">
															@csrf

															<div class="cs-event-price">
																<span>Price<em class="cs-color"><sup> &#8358;</sup>{{$training->cost}}</em></span>
																	
																<a href="{{route('register')}}">REGISTER NOW</a>
															</div>

														</form>

													@elseif(isset($user) AND count($user->trainings->where('id', $training->id)))

														<form action="{{action('EventController@store')}}" method="post">
															@csrf

															<div class="cs-event-price">
																<span>Price<em class="cs-color"><sup> &#8358;</sup>{{$training->cost}}</em></span>

																<button type="submit" class="btn btn-success " disabled>ALREADY REGISTERED</button>
															</div>


														</form>

													@else

														<form action="{{action('DelegateController@create')}}" method="post">
															@csrf

															<div class="cs-event-price">
																<span>Price<em class="cs-color"><sup> &#8358;</sup>{{$training->cost}}</em></span>

																<input name="training_id" type="hidden" value="{{$training->id}}">

																<input name="training_title" type="hidden" value="{{$training->title}}">

																<input name="training_venue" type="hidden" value="{{$training->venue}}">

																<input name="training_cost" type="hidden" value="{{$training->cost}}">

																<button type="submit" class="btn btn-success">REGISTER NOW</button>
															</div>

														</form>


													@endif
												</div>
											</div>
										</div>
									@endif

							
								@endforeach
								
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer id="footer" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    text-align: center;"> 


    <div class="cs-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="copyright-text">
                        <!-- <p>© 2018 Wizer Consulting All Rights Reserved.</p> -->
                        <p>© {{ Carbon\Carbon::now()->year }} Wizer Consulting All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="cs-social-media">
                        <ul>
                            <li><a href="index.html#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-instagram2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-youtube3"></i></a></li>
                            <li><a href="index.html#"><i class="icon-linkedin22"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>
<!-- Main End --> 

