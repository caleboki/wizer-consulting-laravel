@extends('delegates._main')

<div class="container" id="app2" style="padding-top: 1.5rem;">
	<div id="signupbox" style=" margin-top:50px" class="mainbox ">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title"><!-- {{session('training_title')}} -->Summary</div>

            </div>

            
            <div class="panel-body"  v-for="(delegate, index) in delegates">
            	<form  class="form-horizontal" @submit.prevent>
	                @csrf

                    <div class="form-group"> 
                        <label for="first_name" class="control-label col-md-3"> <span style="color: red">*</span>First Name </label> 
                        <div class="controls col-md-8 ">
                        	

                            <input class="form-control" name="first_name" placeholder="First Name" v-model="delegate.first_name" style="margin-bottom: 10px" type="text" :readonly="editing == 0 ? true : false" />

                            
                        </div>
                    </div>

                    <div class="form-group"> 
                        <label class="control-label col-md-3"> <span style="color: red">*</span>Last Name </label> 
                        <div class="controls col-md-8 "> 
                            

                            <input class=" form-control" name="last_name" placeholder="Last Name" value="" style="margin-bottom: 10px" type="text" :readonly="editing == 0 ? true : false" />

                            
                        </div>
                    </div>
	                    
                    <div class="form-group"> 
                        <label class="control-label col-md-3 "> <span style="color: red">*</span>Email </label> 
                        <div class="controls col-md-8 "> 
                           

                            <input name="email" placeholder="Email" value="" style="margin-bottom: 10px" type="email" :readonly="editing == 0 ? true : false" />

                            
                        </div>
                    </div>

                    <div class="form-group pull-right"> 
                        <div class="aab controls col-md-4 "></div>
                        <div class="controls col-md-12 ">
                            
                            <button type="submit" class="btn btn-primary btn btn-info" v-if="editing == false" @click="edit(delegate)">Edit</button>

                            <button type="submit" class="btn btn-primary btn btn-info" v-if="editing == true" @click="update(delegate)">Update</button>
                            
                        </div>
                    </div>
   
	            </form>

            </div>
            
        </div>
    </div>

</div>

{{--  Footer start  --}}
<footer id="footer" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    text-align: center;"> 


    <div class="cs-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="copyright-text">
                        <p>© 2018 Wizer Consulting All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="cs-social-media">
                        <ul>
                            <li><a href="index.html#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-instagram2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-youtube3"></i></a></li>
                            <li><a href="index.html#"><i class="icon-linkedin22"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>


{{-- Footer end --}}