@extends('_admin')
@section('content')
@section('title', "|Logos" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Logos</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('logos.create')}}">New Logo</a></button>
      	<table class="table table-striped">
		    <thead>
		      <tr>
		      	<th>Image</th>
		        
		        <th></th>
		        <th></th>
		        
		        <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($logos as $logo)
		      		
			      <tr>
			      	<td><img src="{{ asset($logo->image) }}" alt="" style="400px" height="400px" style="border-radius: 50%"></td>
			      
			
			        
			        <td>
			        	<a href="{{ route('logos.show', $logo->id) }}">
			        	<i class="glyphicon glyphicon-folder-open" ></i></a>
			        </td>

			        <td>
			        	<a href="{{ route('logos.edit', $logo->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>
			        	
			          	<form class="delete" action="{{action('LogoController@destroy', $logo['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf
	        				
	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>

			        	
			      </tr>
			    @endforeach
		    </tbody>
		</table>
		
     </div>

</div>


@endsection