@extends('_admin')

@section('title', "|Add Logo")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Add Logo</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('logos.store')}}" enctype="multipart/form-data" method="post">
		      	@csrf
        		
		        

		      	<div class="form-group">
		          <label>Organization</label>
		          <input name="organization" type="text" class="form-control">
		        </div>

		        <div class="form-group">
                    <label for="name">Upload new logo</label>
                    <input type="file" name="image"  class="form-control">
                </div>

		        

	
		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

