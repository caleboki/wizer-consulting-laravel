@extends('_admin')

@section('title', "|Edit Training")
@section('content')

<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Transaction</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

				

		      <form action="{{action('TransactionController@update', $transaction['id'])}}" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">

        		<div class="form-group">
					@if ($transaction->payment_proof != null)
				  		<a href="{{asset('storage/'.$transaction->payment_proof)}}">Proof of payment</a>
					
				  		<br><br>
					@endif

					@if($transaction->status == -2 )
				  		<div style="color:red;">This Transaction has been Canceled</div><br>
				  	@endif
					  
				  <label>Narration</label>
				  
		          <input type="text" name="narration" value="{{$transaction->narration}}" class="form-control" readonly>
		        </div>

        		<div class="form-group">
					<label>Status</label>

					<div class="radio">
						<label><input type="radio" name="status" value="canceled" @if($transaction->status == -2 )
								  checked="1" @endif @if($transaction->status == -2 )
								  disabled @endif>Canceled
					  	</label>
				  	</div>
					
					<div class="radio">
						<label><input type="radio" name="status" value="expired" @if($transaction->status == -1 )
								  checked="1" @endif @if($transaction->status == -2 )
								  disabled @endif>Expired
					  </label>
				  	</div>
		        	
		        	
	        		<div class="radio">
  						<label><input type="radio" name="status" value="unverified" @if($transaction->status == 0 )
					            	checked="1" @endif  @if($transaction->status == -2 )
									disabled @endif>Unverified
						</label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="status" value="pending" @if($transaction->status == 1 )
					            	checked="1"
					        	@endif  @if($transaction->status == -2 )
								disabled @endif>Pending</label>
					</div>
					<div class="radio">
						<label><input type="radio" name="status" value="verified" @if($transaction->status == 2 )
									  checked="1"
								  @endif  @if($transaction->status == -2 )
								  disabled @endif>Verified</label>
					  </div>
				 	
				</div>

        		<div class="form-group">
		          <label>Training Title</label>
		          <input type="text" name="name" value="{{$transaction->training->title}}" class="form-control" readonly>
		        </div>
		        
		        	<table class="table">
		        		<thead>
		        			
			        		<tr>
			        			<th class="center"><label>Delegates</label></th>
			        			
			        		</tr>
		        		</thead>
		        		@foreach($transaction->users as $user)
		        			<tbody>
		        				<tr>
		        					<td>{{ $user->first_name }} {{ $user->last_name }}</td>
		        				</tr>
		        			</tbody>
		        		@endforeach
		        	</table>

		        	<table class="table">
		        		<thead>
		        			
			        		<tr>
			        			<th class="center"><label>Initiator</label></th>
			        			
			        		</tr>
		        		</thead>
		        		
		        			<tbody>
		        				<tr>
		        					<td>{{ $transaction->user['first_name']}} {{ $transaction->user['last_name']}}</td>
		        				</tr>
		        			</tbody>
		        		
		        	</table>


		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>

@endsection