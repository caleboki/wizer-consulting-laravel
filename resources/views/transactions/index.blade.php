@extends('_admin')
@section('content')
@section('title', "|Transactions" )

<div class="col-md-9">
	<table class="table" id="table">
		<thead>
			<tr>
				<th class="text-center">#</th>
				<th class="text-center">Training Title</th>
				<th class="text-center">Narration</th>
				
				<th class="text-center">Cost</th>
				<th class="text-center">Status</th>
				<th class="text-center">Date</th>
				<th class="text-center">Actions</th>
			</tr>
		</thead>
		
		<tbody>
			@foreach ($transactions as $transaction)
				<tr>
					<td>{{$transaction->id}}</td>
					<td>{{$transaction->training->title}}</td>
					<td>{{$transaction->narration}}</td>
					
					<td>{{ number_format($transaction->cost)}}</td>

					@if($transaction->status == -2)
						<td><span class="label label-default">Canceled</span></td>
					@endif
					
					@if($transaction->status == -1)
						<td><span class="label label-danger">Expired</span></td>
					@endif
					@if($transaction->status == 0)
						<td><span class="label label-warning">Unverified</span></td>
					@endif
					@if($transaction->status == 1)
						<td><span class="label label-info">Pending</span></td>
					@endif
					@if($transaction->status == 2)
						<td><span class="label label-success">Verified</span></td>
					@endif

					<td>{{ \Carbon\Carbon::parse($transaction->created_at)->format('d M Y')}}</td>
					<td><a href="{{ route('transactions.edit', $transaction->id) }}"><i class="glyphicon glyphicon-edit edit-modal" ></i></a></td>
				</tr>
			@endforeach
		</tbody>
		
	</table>

</div>





@endsection