@extends('_main')

@section('title', "|Event")
@section('content')

<div class="wrapper"> 
	<!-- Side Menu Start -->
	<div id="overlay"></div>
    <div id="mobile-menu">
        <ul>
            <li>
                <div class="mm-search">
                    <form id="search" name="search">
                        <div class="input-group">
                            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
					   <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="icon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <li><a href="index.html">Home</a></li>
			<li class="active"><a href="/#courses">Courses</a>
				
			</li>
			
			<li><a href="{{route('events.index')}}">Events</a>
				
			</li>
			
			<li><a href="{{route('contact')}}">Contact</a>
				
			</li>
            
        </ul>
    </div>
	<!-- Side Menu End -->
	
	<!-- Sub Header Start -->  
	<div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="cs-page-title">
						<h1>Event Detail</h1>
						<p style="color:#aaa;">650+ courses to help you develop creative and technical skills.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Sub Header End --> 
	<br><br>
	<!-- Main Start -->
	<div class="main-section">
	  <div class="page-section">
	    <div class="container">
	      <div class="row">
	        <aside class="page-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div >
					<img src="{{ asset($event->image) }} " style="width: 100%; margin-top: 20px; height: 200px; object-fit: cover;">
					
					@if (!is_null($event->expectations_1))

						<div class="well well-sm materials" style="margin-top: 20px; border-width: 4px; border-radius: 14px; border:5px solid #e3e3e3;">

						
							{!! formatList($event->expectations_1) !!}



						</div>

					
					@endif


    
                </div>

                @if(isset($user) AND count($user->trainings->where('id', $event->id)))
                

	                <div class="well well-sm">
				    	<h5>Courses and Materials</h5>
				    	<ul class="cs-list-style" style="margin-left: 14px;">

				    		@foreach($event->courses as $course)
				    			@if ($course->materials->count() !== 0)

					    			<li>
					    				<div style="margin-left: 14px; margin-top: -30px;">{{strip_tags($course->title) }}
					    					<div >
					    						<a href="/{{$course->materials[0]->link}}">
					    						<em>- {{$course->materials[0]->title}}</em></a>
					    					</div>
					    				</div>
					    			</li>

				    			@endif
				    		@endforeach
				    		
				    	</ul>			    	
					    	
					</div>
				@endif

				
                
            </aside>
	        <div class="page-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
	          <div class="row">
	            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div id="cs-about" class="cs-about-courses">
						<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				 		<div class="cs-section-title"><h3>{!! $event->title !!}</h3><em>{{ \Carbon\Carbon::parse($event->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($event->end_date)->format('d M Y')}}</em></div>
				 
						<P>{!! formatList($event->description) !!}</p>
					</div>   
				    <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				    	<h5>Courses</h5>
				    	<ul class="cs-list-style">

				    		@foreach($event->courses as $course)

				    			<li>{!! $course->title !!}</li>
				    		@endforeach
				    		
				    		
				    		
				    	</ul>			    	
				    	
					</div> -->
								
					
				</div>
			</div>
					
		</div>
							
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
	    	<div >
				<div class="well well-sm" style="margin-top: 20px">
						All modules are presented with
						extensive hands-on exercises/case
						studies that are designed to allow
						delegates apply concepts learned.
				</div>
					
					
				<div style="color: red">Fee per Participant: &#8358;{{number_format($event->cost)}}</div> 
					
					@if(!is_null($event->expectations_2))
						<div class="well well-sm materials" style="margin-top: 20px; border-width: 4px; border-radius: 14px; border:5px solid #e3e3e3;">
								


								{!!$event->expectations_2 !!}



						</div>
					@endif
					

					

					@if(!Auth::check())

						<form action="/register">
							

							<div class="cs-event-price">
								
									
								<button type="submit" class="btn btn-success btn-block">REGISTER NOW</button>
							</div>

						</form>

					@elseif(isset($user) AND count($user->trainings->where('id', $event->id)))

						<form action="{{action('EventController@store')}}" method="post">
							@csrf

							<div class="cs-event-price">
								
								<button type="submit" class="btn btn-success btn-block disabled">ALREADY REGISTERED</button>
							</div>


						</form>

					@else

						<form action="{{action('EventController@purchase')}}" method="post">
							@csrf

							<div class="cs-event-price">
								<!-- <span>Price<em class="cs-color">&#8358;{{$event->cost}}</em></span> -->

								<input name="training_id" type="hidden" value="{{$event->id}}">

								<input name="training_title" type="hidden" value="{{$event->title}}">

								<input name="training_venue" type="hidden" value="{{$event->venue}}">

								<button type="submit" class="btn btn-success btn-block">REGISTER NOW</button>
							</div>

						</form>


					@endif
				</div>
				
				
	    	</div>        
	            
    	</div>
	</div>
</div>
</div>
</div>
	   
</div>
	

</div>

@endsection