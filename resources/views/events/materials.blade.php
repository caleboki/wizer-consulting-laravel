@include('_header')
<div class="wrapper">
    <!-- Side Menu Start -->
    <div id="overlay"></div>
    <div id="mobile-menu">
        <ul>
            <li>
                <div class="mm-search">
                    <form id="search" name="search">
                        <div class="input-group">
                            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
                       <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="icon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <li><a href="index.html">Home</a></li>
            <li class="active"><a href="/#courses">Courses</a>

            </li>

            <li><a href="{{route('events.index')}}">Events</a>

            </li>

            <li><a href="{{route('contact')}}">Contact</a>

            </li>

        </ul>
    </div>
    <!-- Side Menu End -->

    <!-- Sub Header Start -->
    <div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="cs-page-title">
                        <h1>Materials</h1>
                        <p style="color:#aaa;">Stay ahead...Enhance your technical skills through our world class training programs</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sub Header End -->
    <br><br>

    <!-- Main Start -->
    <div class="main-section">
      <div class="page-section">
        <div class="container">
          <div class="row">
            <div class="page-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row" id="book-list">
                <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="cs-sorting-list">
                    <div class="cs-left-side">
                      <div class="cs-select-holder">
                          <input type="text" class="search" placeholder="Search" />
                      </div>
                  </div>
                </div> -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="cs-courses courses-simple" style="margin-bottom: 0px">
                      <li class="cs-header">
                        <div class="cs-courses-name"><span>Material name</span></div>
                        <div class="cs-courses-date"><span>Type</span></div>

                        <div class="cs-price"><span>Download</span></div>
                      </li>
                    </ul>
                    <ul class="cs-courses courses-simple list" id="list">

                        @foreach($event->courses as $course)
                            @foreach($course->materials as $material)
                                @if (in_array($material->id, $materialIds))
                                    @continue
                                @else
                                    <?php array_push($materialIds, $material->id); ?>
                                @endif
                                <li>
                                    <div class="cs-courses-name filename"><h6> {{ $material->title}} </h6></div>
                                    <div class="cs-courses-date"><span class="post-date" style="text-transform: uppercase;"> {{File::extension($material->link)}}</span></div>

                                    <div class="cs-courses-level"><a href="/{{$material->link}}"><span class="it" style="margin-left: 50px;cursor: pointer;"><i class="fa fa-download"></i></span></a></div>
                                </li>
                            @endforeach
                        @endforeach

                    </ul>


                    <form style="display: none" method="post" id="document-form" action="api/download-resources">
                        <input type="text" name="file_names" id="file-name-field">
                    </form>
                    <form style="display: none" method="post" id="single-document-form" action="api/download-single-file">
                        <input type="text" name="file_name" id="single-file-name-field">
                    </form>
                    <button id="download-btn" class="btn btn-primary btn-lg" style="display: none">Download Selected Items</button>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>


{{--  Footer start  --}}
<footer id="footer" style="position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    text-align: center;">


    <div class="cs-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="copyright-text">
                        <p>© {{ Carbon\Carbon::now()->year }} Wizer Consulting All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="cs-social-media">
                        <ul>
                            <li><a href="index.html#"><i class="icon-facebook2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-twitter2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-instagram2"></i></a></li>
                            <li><a href="index.html#"><i class="icon-youtube3"></i></a></li>
                            <li><a href="index.html#"><i class="icon-linkedin22"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


</footer>

{{-- Footer end --}}
