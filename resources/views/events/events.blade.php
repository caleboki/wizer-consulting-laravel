@extends('_main')

@section('title', "|Events")

@section('content')

<div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="cs-page-title">
					<h1>Wizer Events</h1>
					<p style="color:#aaa;">650+ courses to help you develop creative and technical skills.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Main Start -->
<br>
<div class="main-section">
	<div class="page-section">
		<div class="container">
			<div class="row">
				<!-- @include('flash') -->
				<aside class="page-sidebar col-lg-3 col-md-3 col-sm-12 col-xs-12">

					<div class="widget cs-recent-event-widget">
						<div class="widget-title">
							<h5>Past Events</h5>
						</div>

						@foreach($trainings as $training)

                            {{--  @if(strtotime($training->end_date) - time() < 0 && strtotime($training->end_date) - time() > -3000000)  --}}
                            @if (new Datetime($training->end_date) < new Datetime())



								<ul>
									<li>
										<div class="cs-recrnt-post">
											<div class="cs-media">
												<figure><a href="events/{{$training->id}}"><img src="{{ asset($training->image) }}" alt="" /></a></figure>
											</div>
											<div class="cs-text"> <span class="cs-location"><i class="cs-color icon-location-pin"></i>{{ strip_tags($training->venue) }}</span>
												<h6><a href="events/{{$training->id}}">{!! $training->title !!}</a></h6>
												<span>{{ \Carbon\Carbon::parse($training->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}}</span>
											</div>
										</div>
									</li>

								</ul>
							@endif
						@endforeach
					</div>

				</aside>
				<div class="page-content col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="row">
								<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
									<div class="cs-section-title" style="margin-bottom:45px;">
										<h2>UPCOMING EVENTS</h2>
										<p>Below is a selection of upcoming training events </p>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
									<div class="widget cs-event-search-widget">
										<div class="spacer" style="height:40px;"></div>
										<div class="cs-field">
											<form method="GET" action="/events/results">
												<input name="query" type="text" placeholder="SEARCH EVENT" />
												<label>
													<input name="name" type="submit" value="" />
												</label>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						@foreach($trainings as $training)

							@if(strtotime($training->end_date) - time() > 0 )

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="cs-event list">
										<div class="cs-media">
											<figure><a href="events/{{$training->id}}"><img src="{{ asset($training->image) }}" alt="" /></a><figcaption>{{ \Carbon\Carbon::parse($training->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}}</figcaption></figure>
										</div>
										<div class="cs-text">
											<div class="cs-post-title">

												<h3><a href="events/{{$training->id}}">{!! $training->title !!}</a></h3>
											</div>
											<span class="cs-location"><i class="cs-color icon-location-pin"></i>{{ strip_tags($training->venue) }}</span>
											<p style="text-align: justify;">{!!str_limit($training->outcome, 300) !!}</p>

											@if(!Auth::check())

												<form action="{{action('DelegateController@create')}}" method="post">
													@csrf

													<input type="hidden" name="training_id" value="{{$training->id}}">
													<input name="training_title" type="hidden" value="{{$training->title}}">

													<input name="training_venue" type="hidden" value="{{$training->venue}}">

													<input name="training_cost" type="hidden" value="{{$training->cost}}">
													<div class="cs-event-price">
														<span>Price<em class="cs-color"><sup> &#8358;</sup>{{number_format($training->cost)}}</em></span>

														@if(strtotime($training->end_date) - time() < 0 )


															<button type="submit" class="btn btn-success" disabled>REGISTER NOW</button>

														@else

															<button type="submit" class="btn btn-success">REGISTER NOW</button>

														@endif

													</div>

												</form>

											@elseif(isset($user) AND count($user->trainings->where('id', $training->id)))

												<form action="{{action('DelegateController@store')}}" method="post">
													@csrf

													<div class="cs-event-price">
														<span>Price<em class="cs-color"><sup> &#8358;</sup>{{number_format($training->cost)}}</em></span>

														<button type="submit" class="btn btn-success " disabled>ALREADY REGISTERED</button>
													</div>


												</form>

											@else

												<form action="{{action('DelegateController@create')}}" method="post">
													@csrf

													<div class="cs-event-price">
														<span>Price<em class="cs-color"><sup> &#8358;</sup>{{number_format($training->cost)}}</em></span>

														<input name="training_id" type="hidden" value="{{$training->id}}">

														<input name="training_title" type="hidden" value="{{$training->title}}">

														<input name="training_venue" type="hidden" value="{{$training->venue}}">

														<input name="training_cost" type="hidden" value="{{$training->cost}}">


														@if(strtotime($training->end_date) - time() < 0 )
															<button type="submit" class="btn btn-success" disabled>REGISTER NOW</button>
														@else
															<button type="submit" class="btn btn-success">REGISTER NOW</button>
														@endif
													</div>

												</form>


											@endif


										</div>
									</div>
								</div>
							@endif


						@endforeach

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Main End -->



@endsection
