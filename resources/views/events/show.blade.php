@extends('_main')

@section('title', "|Event")
@section('content')
<div class="wrapper">
    <!-- Side Menu Start -->
    <div id="overlay"></div>
    <div id="mobile-menu">
        <ul>
            <li>
                <div class="mm-search">
                    <form id="search" name="search">
                        <div class="input-group">
                            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
                       <div class="input-group-btn">
                                <button class="btn btn-default" type="submit"><i class="icon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <li><a href="index.html">Home</a></li>
            <li class="active"><a href="/#courses">Courses</a>
                
            </li>
            
            <li><a href="{{route('events.index')}}">Events</a>
                
            </li>
            
            <li><a href="{{route('contact')}}">Contact</a>
                
            </li>
            
        </ul>
    </div>
    <!-- Side Menu End -->

    <!-- Sub Header Start -->  
    <div class="page-section" style="background:#ebebeb; padding:50px 0 35px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="cs-page-title">
                        <h1>Event Detail</h1>
                        <p style="color:#aaa;">Stay ahead...Enhance your technical skills through our world class training programs</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Sub Header End --> 
    <br><br>

    <!-- Main Start -->
    <div class="main-section">
        <div class="page-section" style="margin-bottom:50px;">
            <div class="container">
                @if(strtotime($event->end_date) - time() < 0 )
                    <div class="alert alert-info center" >
                       <strong style="color: black !important; font-size: 14px !important;">This event has passed</strong>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        
                        <!-- <img src="/assets/images/BasicExcel.jpg " style="width: 100%; margin-top: 5px; height: 200px; object-fit: cover;"> -->

                        <img src="{{ asset($event->image) }}" alt=""  style="width: 100%; margin-top: 20px; height: 200px; object-fit: cover;">
                            
                        

                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">


                        <div class="cs-section-title" >

                            <h3 style="margin-left: 165px;">{{$event->title}}</h3><br>

                            <div class="center" style="margin-right: 200px;">Date: <br>{{ \Carbon\Carbon::parse($event->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($event->end_date)->format('d M Y')}} <br><br>Venue: {!! $event->venue !!} <span>Price: <br>&#8358;{{number_format($event->cost)}}</span></div><br><br>
                            
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-9">
                            <br>
                            
                            @foreach($event->courses as $course)
                                @if($course->materials->count() > 0) 
                                    <?php 
                                        $isMaterialAvailable = true;
                                    ?>
                                @endif
                                
                                <div class="element-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    
                                    <div class="widget cs-text-widget" >
                                        <div class="cs-text" style="padding:0px; background-color: white; line-height: 1.3;">
                                            <a href="/courses/{{$course->id}}" class="cs-bgcolor" style="width: 100%; text-align: center;"> {{$course->title}}</a>
                                        </div>
                                    </div>
                                    <!-- <div class="cs-courses courses-grid">
                                        <div class="cs-media">
                                            <figure>
                                                <a href="/courses/{{$course->id}}">
                                                @if(is_null($course->image))
                                                    <img src="/assets/extra-images/course-grid-img{{$course->id}}.jpg" alt=""/>
                                                @else
                                                    <img src="/{{$course->image}}" alt=""/>

                                                @endif
                                                </a>
                                            </figure>
                                        </div>
                                        <div class="cs-text">
                                            
                                            <div class="cs-post-title">
                                            <h5><a href="/courses/{{$course->id}}">{{str_limit($course->title, 32)}}</a></h5>
                                            </div>
                                            
                                        </div>
                                    </div> -->
                                </div>
                            @endforeach
                        </div>


                        <div class="col-md-3 ">

                            @if(!Auth::check())

                                <form action="{{action('DelegateController@create')}}" method="post">
                                    @csrf
                                    

                                    <input name="training_id" type="hidden" value="{{$event->id}}">

                                    <input name="training_title" type="hidden" value="{{$event->title}}">

                                    <input name="training_venue" type="hidden" value="{{$event->venue}}">

                                    <input name="training_cost" type="hidden" value="{{$event->cost}}">

                                    <div class="cs-event-price">

                                        @if(strtotime($event->end_date) - time() < 0 )
                                        
                                            <button style="height: 38.78px; margin-top: 18px; margin-bottom:18px; width: 86%;" type="submit" class="btn btn-success " disabled>REGISTER NOW</button>

                                        @else

                                            <button style="height: 38.78px; margin-top: 18px; margin-bottom:18px; width: 86%;" type="submit" class="btn btn-success ">REGISTER NOW</button>



                                        @endif
                                    </div>

                                </form>
                                <br>
                            <!-- <a href="" class="pull-right" style="">View Material <i class="fa fa-arrow-right-circle"></i></a> -->

                            @elseif(isset($user) AND count($user->trainings->where('id', $event->id)))

                                

                                    <div class="cs-event-price">

                                        @if ($isMaterialAvailable)

                                            <form action="{{action('EventController@materials')}}" method="post">
                                            @csrf
                                                
                                                <input name="training_id" type="hidden" value="{{$event->id}}">

                                                <input name="training_title" type="hidden" value="{{$event->title}}">

                                                <input name="training_venue" type="hidden" value="{{$event->venue}}">

                                                <input name="training_cost" type="hidden" value="{{$event->cost}}">
                                                
                                        
                                                <button style="height: 38.78px; margin-top: 18px; width: 86%;" type="submit" class="btn btn-success ">VIEW MATERIALS</button>
                                            </form>
                                        @else
                                            
                                                <button style="height: 38.78px; margin-top: 18px; width: 86%; margin-bottom:18px;" type="submit" class="btn btn-success disabled">ALREADY REGISTERED</button>
                                                
                                            
                                        @endif

                                    </div>


                                </form>

                            @else

                                <form action="/delegates/create" method="post">
                                    @csrf

                                    <div class="cs-event-price">
                                        <!-- <span>Price<em class="cs-color">&#8358;{{$event->cost}}</em></span> -->

                                        <input name="training_id" type="hidden" value="{{$event->id}}">

                                        <input name="training_title" type="hidden" value="{{$event->title}}">

                                        <input name="training_venue" type="hidden" value="{{$event->venue}}">

                                        <input name="training_cost" type="hidden" value="{{$event->cost}}">

                                        @if(strtotime($event->end_date) - time() < 0 )

                                            <button style="height: 38.78px; margin-top: 18px; margin-bottom:18px; width: 86%;" type="submit" class="btn btn-success " disabled>REGISTER NOW</button>
                                        @else
                                            <button style="height: 38.78px; margin-top: 18px; margin-bottom:18px; width: 86%;" type="submit" class="btn btn-success ">REGISTER NOW</button>
                                        @endif
                                    </div>

                                </form>


                            @endif
                            
                        </div>

                        </div>
                    </div>

                    

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="padding-left: 0px;">
                        <div class="cs-services top-center">
                            <div class="cs-media icon"> 
                                <!-- <figure><i class="fa fa-desktop"></i></figure> -->
                                <figure><img src="/assets/extra-images/outcome.svg"></figure>
                            </div>
                            <div style="text-align: justify";>
                                <h5 style="text-align: center;">Outcome</h5>
                                <p >{!! $event->outcome !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="cs-services top-center">
                            <div class="cs-media icon"> 
                                <!-- <figure><i class="fa fa-desktop"></i></figure> -->
                                <figure><img src="/assets/extra-images/course_delivery.svg"></figure>
                            </div>
                            <div style="text-align: justify";>
                                <h5 style="text-align: center;">Course Delivery</h5>
                                <p>{!! $event->course_delivery !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="cs-services top-center">
                            <div class="cs-media icon"> 
                                <!-- <figure><i class="fa fa-desktop"></i></figure> -->
                                <figure><img src="/assets/extra-images/who.svg"></figure>

                            </div>
                            <div style="text-align: justify";>
                                <h5 style="text-align: center;">Who Should Attend</h5>
                                <p>{!! $event->who_should_attend !!}</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="page-content course-list col-lg-12 col-md-12 col-sm-12 col-xs-12 grid" id="courses">
                        <!-- <div class="cs-element-title center" style=" padding-bottom:30px;">
                            <br>
                            <h2>Courses</h2>
                                        
                                        
                        </div> -->
                        
                        
                    </div>
                </div>

            </div>

        </div>

        

        <div class="page-section" style="margin-bottom:450px;">
            <div class="container">
                <div class="row">
                    <div class="section-fullwidth col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="cs-section-title center">
                                    <h2>INSTRUCTORS</h2>
                                    
                                </div>
                            </div>
                            @foreach($event->instructors as $instructor)

                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="cs-team grid">
                                        <div class="cs-media">
                                            <figure>
                                                <img src="{{asset($instructor->avatar)}}">
                                            </figure>
                                        </div>
                                        <div class="cs-text">
                                            <h5><a href="about-us.html#" class="cs-color">{{$instructor->first_name}} {{$instructor->last_name}}</a></h5>
                                            <span>{{ $instructor->title }}</span>
                                            <p>{!! $instructor->about !!}</p>
                                            
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            
                            <!-- <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="cs-team grid">
                                    <div class="cs-media">
                                        <figure>
                                            <a href="#"><img src="/assets/extra-images/team2.jpg" alt="#"></a>
                                        </figure>
                                    </div>
                                    <div class="cs-text">
                                        <h5><a href="about-us.html#" class="cs-color">Arthur Springs</a></h5>
                                        <span>Associate Professor of Anthropology</span>
                                        <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="cs-team grid">
                                    <div class="cs-media">
                                        <figure>
                                            <a href="about-us.html#"><img src="/assets/extra-images/team3.jpg" alt="#"></a>
                                        </figure>
                                    </div>
                                    <div class="cs-text">
                                        <h5><a href="about-us.html#" class="cs-color">Arthur Springs</a></h5>
                                        <span>Associate Professor of Anthropology</span>
                                        <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="cs-team grid">
                                    <div class="cs-media">
                                        <figure>
                                            <a href="about-us.html#"><img src="/assets/extra-images/team4.jpg" alt="#"></a>
                                        </figure>
                                    </div>
                                    <div class="cs-text">
                                        <h5><a href="about-us.html#" class="cs-color">Arthur Springs</a></h5>
                                        <span>Associate Professor of Anthropology</span>
                                        <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.</p>
                                        
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>


                </div>
            </div>
        </div>

        



        
    </div>
</div>

@endsection
