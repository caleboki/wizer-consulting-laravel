@extends('_admin')

@section('title', "|Edit Resource")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Resource</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('ResourceController@update', $resource['id'])}}" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">
		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" value="{{$resource->title}}" required>
		        </div>
		        <div class="form-group">
		          <label>Description</label>
		          <textarea rows="2" cols="106" name="description" required>{{$resource->description}}</textarea>

		        </div>
		        <!-- <div class="form-group">
		          <label>Photo</label>
		          <input name="photo" type="text" class="form-control" value="{{$resource->photo}}" required>
		        </div> -->
		        <div class="form-group">
		          <label>Link</label>

		          <input name="link" type="text" class="form-control" value="{{$resource->link}}" required>
		        </div>

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>

</div>


@endsection

