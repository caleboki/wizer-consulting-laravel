@extends('_admin')

@section('title', "|Create Resource")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create Resource</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('resources.store')}}" enctype="multipart/form-data" method="post">
		      	@csrf

		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Description</label>
		          <textarea class="form-control" rows="2" cols="111" name="description"></textarea>
		        </div>

		        <div class="form-group">
                    <label for="name">Upload resource</label>
                    <input type="file" name="link"  class="form-control">
                </div>



		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>

</div>


@endsection

