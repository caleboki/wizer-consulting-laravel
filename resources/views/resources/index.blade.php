@extends('_admin')
@section('content')
@section('title', "|Resources" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Resources</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('resources.create')}}">New Resource</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Description</th>

		        <th>Link</th>
                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($resources as $resource)

			      <tr>
			        <td>{{$resource->title}}</td>
			        <td>{{strip_tags($resource->description)}}</td>
			        <th>{{$resource->link}}</th>
			        <td>
			        	<a href="{{ route('resources.edit', $resource->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>


			          	<form class="delete" action="{{action('ResourceController@destroy', $resource['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		<div class="text-center">{!! $resources->links() !!}</div>
      </div>

	</div>


@endsection
