@extends('_admin')
@section('content')
@section('title', "|Materials" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Materials</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('materials.create')}}">New Material</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Description</th>

		        <th>Link</th>
                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($materials as $material)

			      <tr>
			        <td>{{$material->title}}</td>
			        <td>{{strip_tags($material->description)}}</td>
			        <th>{{$material->link}}</th>
			        <td>
			        	<a href="{{ route('materials.edit', $material->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>


			          	<form class="delete" action="{{action('MaterialController@destroy', $material['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		<div class="text-center">{!! $materials->links() !!}</div>
      </div>

	</div>


@endsection
