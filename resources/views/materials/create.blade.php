@extends('_admin')

@section('title', "|Create Material")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create Material</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('materials.store')}}" enctype="multipart/form-data" method="post">
		      	@csrf
        		
		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Description</label>
		          <textarea class="form-control" rows="2" cols="111" name="description"></textarea>		          
		        </div>

		       <!--  <div class="form-group">
		          <label>Photo URL</label>
		         
		          <input name="photo" type="text" class="form-control" required>
		        </div> -->
		        
		        <div class="form-group">
                    <label for="name">Upload material</label>
                    <input type="file" name="link"  class="form-control">
                </div>

		        <div class="form-group">
		        	<label>Courses</label>
		        	@foreach($courses as $course)
				        <div class="checkbox">
				        	
		        			<label><input type="checkbox" name="courses[]" value='{{ $course->id }}'>{{ $course->title }}</label><br>
		  					
						</div>
					@endforeach
				</div>

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

