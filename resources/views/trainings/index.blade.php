@extends('_admin')
@section('content')
@section('title', "|Training" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">All Training <span class="pull-right"><a href="{{route('trainings.create')}}" style="color: white;">Add Training</a></span></h3>

      </div>
      <div class="panel-body">

      	<br>
      	<table class="table table-striped" id="table">
      		<!-- <button type="button" class="btn btn-default pull-right"><a href="{{route('trainings.create')}}">New Training</a></button> -->
		    <thead>
		      <tr>
		        <th>Title</th>
		        <th>Venue</th>

		        <th>Cost (&#8358;)</th>
		        <!-- <th>Courses</th> -->
		        <th>Start Date</th>
		        <th>End Date</th>

		        <th></th>
		        <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($trainings as $training)

			      <tr>
			      	<td>{{str_limit($training->title), 10}}</td>
			      	<td>{{strip_tags(str_limit($training->venue)), 50}}</td>

			      	<td>{{number_format($training->cost)}}</td>
			      	<!-- <td>{{str_limit($training->courses()->pluck('title')->implode(', '), 50)}}</td> -->
			        <td>{{ \Carbon\Carbon::parse($training->start_date)->format('d M Y')}}</td>
			        <td>{{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}}</td>

			        <td>
			        	<a href="{{ route('trainings.edit', $training->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>


			        </td>
			        <td>

			        	<form class="delete" action="{{action('TrainingController@destroy', $training['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>

			        </td>


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		{{--  <div class="text-center">{!! $trainings->links() !!}</div>  --}}
      </div>

	</div>

</div>


@endsection
