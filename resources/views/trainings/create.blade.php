@extends('_admin')

@section('title', "|Create Training")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create Training</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('trainings.store')}}" enctype="multipart/form-data" method="post">
		      	@csrf

		      	<div class="form-group">
		          <label>Title</label>
		          <input type="text" name="title" class="form-control" value="{{$training->title or old('title') }}">
		        </div>

		        <div class="form-group">
                    <label for="name">Featured Image</label>
                    <input type="file" name="image"  class="form-control" >
                </div>

		        <div class="form-group">
		          <label>Venue</label>
		          <textarea class="form-control" rows="2" cols="111" name="venue">{{$training->venue or old('venue') }}</textarea>		          
		        </div>

		        <div class="form-group">
		          <label>Description</label>
		          <textarea class="form-control" rows="2" cols="111" name="description">{{$training->description or old('description') }}</textarea>		          
		        </div>

		        <div class="form-group">
		          <label>Outcome</label>
		          <textarea class="form-control" rows="2" cols="111" name="outcome">{{$training->outcome or old('outcome') }}</textarea>		          
		        </div>

		        <div class="form-group">
		          <label>Course Delivery</label>
		          <textarea class="form-control" rows="2" cols="111" name="course_delivery">{{$training->course_delivery or old('course_delivery')}}</textarea>		          
		        </div>

		        <div class="form-group">
		          <label>Who Should Attend</label>
		          <textarea class="form-control" rows="2" cols="111" name="who_should_attend">{{$training->who_should_attend or old('who_should_attend') }}</textarea>		          
		        </div>

		        <div class="form-group">
		          <label>Cost Per Participant(N)</label>
		          <input type="number" class="form-control" name="cost" placeholder="1.0" step="0.01" min="0" max="10000000" value="{{$training->cost or old('cost') }}">
		          
		        </div>

        		
		        <div class="form-group">
		          <label>Start Date</label>
		          <div class='input-group date' id='datetimepicker1'>
		          	<input type="text" name="start_date" class="form-control" value="{{$training->start_date or old('start_date') }}" placeholder="MM/DD/YYY" required >
		          	<span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
		      	  </div>
		        </div>

		        <div class="form-group">
		          <label>End Date</label>
		          <div class='input-group date' id='datetimepicker2'>
		          	<input type="text" name="end_date" class="form-control" value="{{$training->end_date or old('end_date') }}" placeholder="MM/DD/YYY" required>
		          	<span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
		      	  </div>
		        </div>

		        <div class="row">
		        	<div class="col-md-12">
		        		<div class="col-md-6">
		        			<div class="form-group">
		        			<label>Courses</label>
		        			@foreach($courses as $course)
					        <div class="checkbox">
					        	
			        			<label><input type="checkbox" name="courses[]" value='{{ $course->id }}'>{{ $course->title }}</label><br>
			  					
							</div>
							@endforeach
							</div>
		        		
		        		</div>

		        		<div class="col-md-6">
		        			<div class="form-group">
		        			<label>Instructors</label>
		        			@foreach($instructors as $instructor)
					        <div class="checkbox">
					        	
			        			<label><input type="checkbox" name="instructors[]" value='{{ $instructor->id }}'>{{ $instructor->first_name }} {{$instructor->last_name}}</label><br>
			  					
							</div>
							@endforeach
							</div>
		        		
		        		</div>
		        		
		        	</div>
		        </div>
		        
		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

