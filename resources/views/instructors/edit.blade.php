@extends('_admin')

@section('title', "|Edit Instructor")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Instructor</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('InstructorController@update', $instructor['id'])}}" enctype="multipart/form-data" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">

        		<div class="form-group">
		          <label>First Name</label>
		          <input type="text" name="first_name" value="{{$instructor->first_name}}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Last Name</label>
		          <input type="text" name="last_name" value="{{$instructor->last_name}}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Title</label>
		          <input type="test" name="title" value="{{$instructor->title}}" class="form-control" required>
		        </div>

		        <div class="form-group">
                    <label for="name">Upload new image</label>
                    <input type="file" name="avatar" class="form-control">
                </div>

		        <div class="form-group">
		          <label>Email</label>
		          <input type="email" name="email" value="{{$instructor->email}}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>About</label>
		          <textarea class="form-control" rows="2" cols="111" name="about">{{$instructor->about}}</textarea>
		        </div>

		       

		        <div class="row">
		        	
					<div class="col-md-12">
						<div class="col-md-6">
							<div class="form-group">
		        			<label>Trainings</label>
		        	
				        	@foreach($trainings as $training)
				        		<div class="checkbox">
							    	
							        <label>
							        	<input type="checkbox" name="trainings[]" value='{{ $training->id }}'
							        	@if(count($instructor->trainings->where('id', $training->id)) )
							            	checked="1"
							        	@endif
							        	>
							        	{{ $training->title }}
							        </label>
							        <br>
						    	</div>
						 	@endforeach
						</div>
					</div>

						

					</div>
				</div>

				


		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

