@extends('_admin')
@section('content')
@section('title', "|Instructors" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Instructors</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('instructors.create')}}">New Instructor</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		      	<th>Image</th>
		        <th>First Name</th>
		        <th>Last Name</th>
		        <th>Email</th>


		        <th>Trainings</th>


                <th></th>
                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($instructors as $instructor)

			      <tr>
			      	<td><img src="{{ asset($instructor->avatar) }}" alt="" style="60px" height="60px" style="border-radius: 50%"></td>
			        <td>{{str_limit($instructor->first_name, 10)}}</td>
			        <td>{{str_limit($instructor->last_name, 10)}}</td>
			        <td>{{$instructor->email}}</td>




			        <td>{{str_limit($instructor->trainings->pluck('title')->implode(', '), 10)}}</td>

			        <td>
			        	<a href="{{ route('instructors.show', $instructor->id) }}">
			        	<i class="glyphicon glyphicon-folder-open" ></i></a>
			        </td>

			        <td>
			        	<a href="{{ route('instructors.edit', $instructor->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        <td>

			          	<form class="delete" action="{{action('InstructorController@destroy', $instructor['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		<div class="text-center">{!! $instructors->links() !!}</div>
     </div>

</div>


@endsection
