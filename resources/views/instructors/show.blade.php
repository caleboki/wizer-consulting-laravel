@extends('_admin')
@section('content')
@section('title', "|Instructor" )

<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Instructor</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('instructors.edit', $instructor->id)}}">Edit Instructor</a></button>

      	<div class="col-md-12">
      		<div class="col-md-4">

      			<h5><strong>First Name</strong></h5>
      			{{$instructor->first_name}}
      			
      		</div>

      		<div class="col-md-4">

      			<h5><strong>Last Name</strong></h5>
      			{{$instructor->last_name}}
      			
      		</div>

      		<div class="col-md-4">

      			<h5><strong>Email</strong></h5>
      			{{$instructor->email}}
      			
      		</div>

      		

      		
      	</div>
      	
      	<!-- <div class="container">
      		<div class="row">
	      		<div class="col-md-12">
	      		
	      			<h5><strong>Trainings</strong></h5>
	      			{{$instructor->trainings->pluck('title')->implode(', ')}}
	      		
	      		</div>
      		</div>
      	</div> -->

      	
      		<div class="row">
	      		<div class="col-md-12">
			
					<table class="table">
					<thead>
					  <tr>
					    <th><h5 class="text-center"><strong>Trainings</strong></h5>
					    </th>
					    
					  </tr>
					</thead>
					<tbody>
						@foreach($instructor->trainings as $training)
							<tr>
							<td>{{$training->title}}</td>

							</tr>
						@endforeach
						
					</tbody>
					</table>
				</div>
			</div>
		
      	
      	
      	
		
    </div>

</div>
@endsection