@extends('_admin')

@section('title', "|Add Instructor")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Add Instructor</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('instructors.store')}}" enctype="multipart/form-data" method="post">
		      	@csrf
        		
		        <div class="form-group">
		          <label>First Name</label>
		          <input name="first_name" type="text" class="form-control">
		        </div>

		        <div class="form-group">
		          <label>Last Name</label>
		          <input name="last_name" type="text" class="form-control">
		        </div>

		        <div class="form-group">
		          <label>Title</label>
		          <input name="title" type="text" class="form-control">
		        </div>

		        <div class="form-group">
                    <label for="name">Upload image</label>
                    <input type="file" name="avatar"  class="form-control">
                </div>

		        <div class="form-group">
		          <label>Email</label>
		          <input name="email" type="email" class="form-control">
		        </div>

		        <div class="form-group">
		          <label>About</label>
		          <textarea class="form-control" rows="2" cols="111" name="about"></textarea>
		        </div>

		        
		        <div class="row">
		        	

					<div class="col-md-12">
						
						<div class="col-md-6">

							<div class="form-group">
			        			<label>Training</label>
				        		@foreach($trainings as $training)
							        <div class="checkbox">
							        	
					        			<label><input type="checkbox" name="trainings[]" value='{{ $training->id }}'>{{ $training->title }}</label><br>
					  					
									</div>
								@endforeach
							</div>
						</div>
						
						
					</div>				

				</div>
	
		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

