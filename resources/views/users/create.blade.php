@extends('_admin')

@section('title', "|Add User")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Add User</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('users.store')}}" enctype="multipart/form-data" method="post">
		      	@csrf

		        <div class="form-group">
		          <label>First Name</label>
		          <input name="first_name" type="text" class="form-control" value="{{$user->first_name or old('first_name') }}" required>
		        </div>

		        <div class="form-group">
		          <label>Last Name</label>
		          <input name="last_name" type="text" class="form-control" value="{{$user->last_name or old('last_name') }}" required>
		        </div>

		        <div class="form-group">
                    <label for="name">Upload new avatar</label>
                    <input type="file" name="avatar"  class="form-control">
                </div>

		        <div class="form-group">
		          <label>Email</label>
		          <input name="email" type="email" class="form-control" required>
		        </div>

		        <div class="form-group">
		        	<!-- <label>Training</label> -->

			        <div class="checkbox">

	        			<label><input type="checkbox" name="auto_generate" v-model="auto_password">Auto Generate Password</label><br>

					</div>

				</div>

		        <div class="form-group" v-if="!auto_password">
		          <label>Password</label>
		          <input name="password" id="password" type="password" class="form-control" placeholder="Manually give a password to this user">
		        </div>

		        <!-- <div class="form-group">
		          <label>Confirm Password</label>
		          <input name="password_confirmation" type="password" class="form-control" required>
		        </div> -->
		        <div class="row">
		        	<div class="col-md-6">
				        <div class="form-group">
				        	<label>Organization</label>
				        	@foreach($organizations as $organization)
						        <div class="radio">

				        			<label><input type="radio" name="organizations[]" value='{{ $organization->id }}'>{{ $organization->name }}</label><br>

								</div>
							@endforeach
						</div>
					</div>

					<div class="col-md-6">

						<div class="form-group">
		        			<label>Training</label>
                            @foreach($trainings as $training)
                                @if(strtotime($training->end_date) - time() > 0 )
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="trainings[]" value='{{ $training->id }}'>{{ $training->title }} <div>({{ \Carbon\Carbon::parse($training->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}})</div></label><br>
                                    </div>
                                @endif
							@endforeach
						</div>
					</div>

				</div>

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>

</div>


@endsection

