@extends('_admin')
@section('content')
@section('title', "|Users" )

<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Users</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('users.edit', $user->id)}}">Edit User</a></button>

      	<div class="col-md-12">
      		<div class="col-md-3">

      			<h5><strong>Name</strong></h5>
      			{{$user->first_name}}
      			
      		</div>

      		<div class="col-md-3">

      			<h5><strong>Email</strong></h5>
      			{{$user->email}}
      			
      		</div>

      		<div class="col-md-3">

      			<h5><strong>Organization</strong></h5>
      			{{$user->organization['name']}}
      			
      		</div>

      		<div class="col-md-3">

      			<h5><strong>Trainings</strong></h5>
      			{{$user->trainings->pluck('title')->implode(', ')}}
      			
      		</div>
      	</div>
      	

      	<div class="col-md-12">
      		<div class="col-md-4">
      			<h5><strong>Role</strong></h5>
      			<ul>
      				{{$user->roles->count() == 0 ? 'This user has no roles' : ''}}
	      			@foreach($user->roles as $role)

	      				<li>{{$role->display_name}} ({{$role->description}})</li>
	      			@endforeach
      			</ul>
      			
      		</div>
      		
      	</div>
      	
		
     </div>

</div>
@endsection