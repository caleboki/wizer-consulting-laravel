@extends('_admin')
@section('content')
@section('title', "|Users" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Users</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('users.create')}}">New User</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>
		      	<th>Image</th>
		        <th>First Name</th>
		        <th>Last Name</th>
		        <th>Email</th>
		        <th>Organization</th>
		        <!-- <th>Org. Trainings</th> -->
		        {{--  <th>Trainings</th>  --}}


                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($users as $user)

			      <tr>
			      	<td><img src="{{ asset('storage/'.$user->avatar) }}" alt="" style="60px" height="60px" style="border-radius: 50%"></td>
			        <td>{{str_limit($user->first_name, 10)}}</td>
			        <td>{{str_limit($user->last_name, 10)}}</td>
			        <td>{{$user->email}}</td>
			        <td>{{$user->organization['name']}}</td>

			        <!-- @if(is_null($user->organization))
			        	<td></td>
			        @else
			        	<td>{{str_limit($user->organization->trainings->pluck('title')->implode(', '), 50)}}</td>
			        @endif -->

			        {{--  <td>{{str_limit($user->trainings->pluck('title')->implode(', '), 10)}}</td>  --}}

			        <td>
			        	<a href="{{ route('users.show', $user->id) }}">
			        	<i class="glyphicon glyphicon-folder-open" ></i></a>
			        </td>

			        <td>
			        	<a href="{{ route('users.edit', $user->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>
			        {{--  <td>

			          	<form class="delete" action="{{action('UserController@destroy', $user['id'])}}" method="POST">
	        				<input type="hidden" name="_method" value="DELETE">
	        				@csrf

	        				<button type="submit" class="delete" style="background-color: transparent; border:transparent;">
        						<i class="glyphicon glyphicon-trash"></i>
        					</button>
    					</form>
			        </td>  --}}


			      </tr>
			    @endforeach
		    </tbody>
		</table>
		<div class="text-center">{!! $users->links() !!}</div>
     </div>

</div>


@endsection
