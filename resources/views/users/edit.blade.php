@extends('_admin')

@section('title', "|Edit User")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit User</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('UserController@update', $user['id'])}}" enctype="multipart/form-data" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">

        		<div class="form-group">
		          <label>First Name</label>
		          <input type="text" name="first_name" value="{{$user->first_name}}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Last Name</label>
		          <input type="text" name="last_name" value="{{$user->last_name}}" class="form-control" required>
		        </div>

		        <div class="form-group">
                    <label for="name">Upload new avatar</label>
                    <input type="file" name="avatar" class="form-control">
                </div>

		        <div class="form-group">
		          <label>Email</label>
		          <input type="email" name="email" value="{{$user->email}}" class="form-control" required>
		        </div>

		        <div class="radio">

			        <label>
			        	<input type="radio" v-model="password_options" name="password_options" value='keep'>
			        	Do Not Change Password

			        </label>
			        <br>
				</div>

				<div class="radio">

			        <label>
			        	<input type="radio" v-model="password_options" name="password_options" value='auto'>
			        	Auto Generate New Password

			        </label>
			        <br>
				</div>

				<div class="radio">

			        <label>
			        	<input type="radio" v-model="password_options" name="password_options" value='manual'>
			        	Manually Set New Password

			        </label>
			        <br>
				</div>

		        <div class="form-group" v-if="password_options == 'manual'">
		          <label>Password</label>
		          <input type="password" placeholder="Manually give a password to this user" name="password" class="form-control">
		        </div>

		        <!-- <div class="form-group" v-if="password_options == 'manual'">
		          <label>Confirm Password</label>
		          <input type="password" name="password_confirmation" class="form-control">
		        </div>  -->

		        <div class="row">
		        	<div class="col-md-6">
						<div class="form-group">
		        			<label>Organizations</label>

				        	@foreach($organizations as $organization)
				        		<div class="radio">

							        <label>
							        	<input type="radio" name="organizations[]" value='{{ $organization->id }}'
							        	@if($user->organization_id == $organization->id)
							        	checked="1"
							        	@endif
							        	>
							        	{{ $organization->name }}
							        </label>
							        <br>
						    	</div>
							 @endforeach
							 <br>

							 <h5 class="title"><strong>Roles</strong></h5>

								@foreach($roles as $role)

									<div class="checkbox">

										<label>
											<input type="checkbox" value='{{$role->id}}' name="roles[]"
											@if(count($role->users->where('id', $user->id)) )
												checked="1"
											@endif>{{$role->display_name}}
											<em>({{$role->description}})</em>
										</label>
										<br>
									</div>
								@endforeach
						</div>
					</div>
					<div class="col-md-6">

						<div class="form-group">
		        			<label>Trainings</label>

                            @foreach($trainings as $training)
                                @if(strtotime($training->end_date) - time() > 0 )
                                    <div class="checkbox">

                                        <label>
                                            <input type="checkbox" name="trainings[]" value='{{ $training->id }}'
                                            @if(count($user->trainings->where('id', $training->id)) )
                                                checked="1"
                                            @endif
                                            >
                                            {{ $training->title }} <div>({{ \Carbon\Carbon::parse($training->start_date)->format('d M')}} - {{ \Carbon\Carbon::parse($training->end_date)->format('d M Y')}})</div>
                                        </label>
                                        <br>
                                    </div>
                                @endif
						 	@endforeach
						</div>

					</div>
				</div>

				{{--  <div class="row">
					<div class="col-md-6">
						<h5 class="title"><strong>Roles</strong></h5>

						@foreach($roles as $role)

							<div class="checkbox">

						        <label>
						        	<input type="checkbox" value='{{$role->id}}' name="roles[]"
						        	@if(count($role->users->where('id', $user->id)) )
									    checked="1"
									@endif>{{$role->display_name}}
						        	<em>({{$role->description}})</em>
						        </label>
				        		<br>
							</div>
						@endforeach

					</div>
				</div>	  --}}


		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>

</div>


@endsection

