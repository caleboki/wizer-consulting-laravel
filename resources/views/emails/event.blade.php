@component('mail::message')
# Dear {{$user->first_name}}

You have been registered for the following events:

<table class="table table-striped">
    <thead>
      <tr>
        <th>Title</th>
        <th>Venue</th>
        <th>Cost (&#8358;)</th>
        <th>Start Date</th>
        <th>End Date</th>
        
        <th></th>
      </tr>
    </thead>
    <tbody>
    	@foreach($user->trainings as $training)
	    	<tr>
		      	<td>{{str_limit($training->title), 10}}</td>
		      	<td>{{strip_tags(str_limit($training->venue)), 50}}</td>
		      	<td>{{$training->cost}}</td>
		      	<td>{{ \Carbon\Carbon::parse($training->start_date)->format('jS F Y ')}}</td>
		      	<td>{{ \Carbon\Carbon::parse($training->end_date)->format('jS F Y ')}}</td>
			</tr>


		@endforeach 

    	
    </tbody>
</table>



                               
   

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
