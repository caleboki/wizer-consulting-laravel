@component('mail::message')
# Dear {{ $user->first_name }} this is to confirm that your payment has been verified and your registration for the following event is complete:

<div class="container">
    <table class="table" border="1" cellpadding="20" cellspacing="0" height="100%" width="900" id="bodyTable">
        <thead>
        <tr>
            <th>Title</th>
            <th>Venue</th>
            <th>Start Date</th>
            <th>End Date</th>
            
        </tr>
        </thead>
        <tbody>
            
            <tr>
                <td>{{str_limit($training->title), 10}}</td>
                <td>{{strip_tags($training->venue)}}</td>
                <td>{{ \Carbon\Carbon::parse($training->start_date)->format('jS F Y ')}}</td>
                <td>{{ \Carbon\Carbon::parse($training->end_date)->format('jS F Y ')}}</td>
            </tr>
            
        </tbody>
    </table>
</div><br>

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
