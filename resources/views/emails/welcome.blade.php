@component('mail::message')
# Thank you for your registration {{$user->first_name}}

To view a selection of our courses, please click the button below

@component('mail::button', ['url' => 'http://localhost:8000/events'])
Courses
@endcomponent

You can login into your account with the following credentials:<br><br>
Email: {{$user->email}}<br>
Password: {{$password}}

<!-- @component('mail::panel', ['url' => ''])
Lorem Ipsum
@endcomponent -->

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
