@component('mail::message')
# Dear {{ $user->first_name }} you have initiated the registration process for the following event:

<div class="container">
    <table class="table" border="1" cellpadding="20" cellspacing="0" height="100%" width="900" id="bodyTable">
    <thead>
      <tr>
        <th>Title</th>
        <th>Venue</th>
        <th>Start Date</th>
        <th>End Date</th>
      </tr>
    </thead>
    <tbody>
    	
        <tr>
            <td>{{str_limit($training->title), 10}}</td>
            <td>{{strip_tags($training->venue)}}</td>
            <td>{{ \Carbon\Carbon::parse($training->start_date)->format('jS F Y ')}}</td>
            <td>{{ \Carbon\Carbon::parse($training->end_date)->format('jS F Y ')}}</td>
        </tr>
    	
    </tbody>
    </table><br>
    <p>To view your events click on the button below:</p> 
    @component('mail::button', ['url' => env('APP_URL', false).":8000/home",])
    My Events
    @endcomponent
    <br>
    <p>Your narration code is <b>{{ $transaction->narration }}</b></p>
    <br>
    If you have not uploaded, or you want to update your proof of payment please click the button below <br>
    @component('mail::button', ['url' => env('APP_URL', false).":8000/delegates/transactions/update",])
    Proof of Payment
    @endcomponent
    <br>
    After your payment has been completed a confirmation email will be sent to you
</div><br>



Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
