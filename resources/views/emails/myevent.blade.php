@component('mail::message')
# Dear {{$user->first_name}}, <br>
# You have been registered for the following course:

<div class="container">
    <table class="table" border="1" cellpadding="20" cellspacing="0" height="100%" width="900" id="bodyTable">
        <thead>
        <tr>
            <th>Title</th>
            <th>Venue</th>
            <th>Start Date</th>
            <th>End Date</th>
        </tr>
        </thead>
        <tbody>
            @foreach($user->trainings as $training)
                <tr>
                    <td>{{str_limit($training->title), 10}}</td>
                    <td>{{strip_tags($training->venue)}}</td>
                    <td>{{ \Carbon\Carbon::parse($training->start_date)->format('jS F Y ')}}</td>
                    <td>{{ \Carbon\Carbon::parse($training->end_date)->format('jS F Y ')}}</td>
                </tr>


            @endforeach


        </tbody>
    </table>
    <br>
    The time for this event is 9:00 AM to 5:00 PM daily. Please note that you are required to have a laptop with Microsoft Excel (2007 or later version) installed. <br><br>
    To view details about this event, please click on the button below and login.

    @component('mail::button', ['url' => env('APP_URL').'/home'])
    My Events
    @endcomponent

    You can login into your account with the following credentials:<br><br>
    <b>Email: {{$user->email}}</b><br>
    <b>Password: wizer2019</b>
    <br><br>

    To view the materials for this course, please click on the View Materials button on the Events page.
    <br><br>


    Best Regards, <br>
    Lanre Akinbo (Course Director)<br>
    Wizer Consulting Services LLP<br>
    234 (0) 8034021026<br>
    lanre.akinbo@wizeradvisory.com

</div>



@endcomponent
