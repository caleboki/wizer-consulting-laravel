@component('mail::message')
# Dear {{$user->first_name}}

You have been registered for the following events:

<div class="container">
    <table class="table" border="1" cellpadding="20" cellspacing="0" height="100%" width="900" id="bodyTable">
    <thead >
      <tr>
        <th>Title</th>
        <th>Venue</th>
        <th>Cost</th>
        <th>Start Date</th>
        <th>End Date</th>  
        
      </tr>
    </thead>
    <tbody>
    	@foreach($user->trainings as $training)
	    	<tr>
		      	<td>{{str_limit($training->title), 10}}</td>
		      	<td>{{strip_tags($training->venue), 50}}</td>
		      	<td> &#8358; {{number_format($training->cost)}} </td>
		      	<td>{{ \Carbon\Carbon::parse($training->start_date)->format('jS F Y ')}}</td>
		      	<td>{{ \Carbon\Carbon::parse($training->end_date)->format('jS F Y ')}}</td>
			</tr>


		@endforeach 

    	
    </tbody>
    </table>
    <br>
    <p>Your narration code is <b>{{ $transaction->narration }}</b></p>
    <br>
    If you have not uploaded, or you want to update your proof of payment please click the button below 
    <br>
    @component('mail::button', ['url' => env('APP_URL', false).":8000/delegates/transactions/update",])
    Proof of Payment
    @endcomponent
    <br>
    You will recieve a confirmation email once your payment has been confirmed
    <br><br>
    An account has also been created for you, and you can log in with the following credentials:<br><br>
    Email: {{$user->email}}<br>
    Password: {{$password}}<br><br>
    To view your events click on the button below:<br>

    @component('mail::button', ['url' => env('APP_URL', false).":8000/home",])
    My Events
    @endcomponent
</div>



Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
