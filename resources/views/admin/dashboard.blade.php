@extends('_admin')
@section('content')
<div class="col-md-9">
    <!-- Website Overview -->
    <div class="panel panel-default">
        <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Website Overview</h3>
        </div>
        <div class="panel-body">
        <div class="col-md-3">
            <div class="well dash-box">
            <h2><span class="glyphicon glyphicon-user" aria-hidden="true"></span> {{$users->count()}}</h2>
            <h4>Users</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well dash-box">
            <h2><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> {{count($organizations)}}</h2>
            <h4>Organizations</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well dash-box">
            <h2><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> {{count($trainings)}}</h2>
            <h4>Trainings</h4>
            </div>
        </div>
        <div class="col-md-3">
            <div class="well dash-box">
            <h2><span class="glyphicon glyphicon-education" aria-hidden="true"></span>{{count($courses)}}</h2>
            <h4>Courses</h4>
            </div>
        </div>

        </div>
    </div>

    <!-- Latest Users -->
    <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Latest Users</h3>
    </div>
    <div class="panel-body">
        <table class="table table-striped table-hover">
            <tr>
            <th>Image</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Joined</th>
            </tr>
            <tbody>
            @foreach($users as $user)

                <tr>
                    <td><img src="{{ asset('storage/'.$user->avatar) }}" alt="" style="60px" height="60px" style="border-radius: 50%"></td>

                    <td>{{$user->first_name}}</td>
                    <td>{{$user->last_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{ \Carbon\Carbon::parse($user->created_at)->format(' jS F Y')}}</td>
                </tr>

            @endforeach

            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection
