@extends('_admin')
@section('title', "|Role")

@section('content')

<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading main-color-bg">
      <h3 class="panel-title">View Role</h3>
    </div>
    
    <div class="panel-body">
      <button type="button" class="btn btn-default pull-right"><a href="{{route('roles.edit', $role->id)}}" class="button is-primary is-pulled-right"><i class="glyphicon glyphicon-edit" ></i></a></button>
    </div>
        
    <div class="container">
      <p>
        <strong>{{$role->display_name}}</strong> <small>{{$role->name}}</small>
        <br>
        {{$role->description}}
      </p>
    </div>
  </div>
</div>
            
       
     
   
  
@endsection