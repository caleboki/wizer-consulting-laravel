@extends('_admin')
@section('content')
@section('title', "|Roles" )
<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Administer Roles</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('roles.create')}}">New Role</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>

		        <th>Role Name</th>
		        <th>Slug</th>
		        <th>Description</th>

                <th></th>
                <th></th>
		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($roles as $role)

			      <tr>

			        <td>{{$role->display_name}}</td>
			        <td>{{$role->name}}</td>
			        <td>{{$role->description}}</td>

			        <td>

			        	<a href="{{ route('roles.show', $role->id) }}">
			        	<i class="glyphicon glyphicon-folder-open" ></i></a>

			        </td>
			        <td>
			        	<a href="{{ route('roles.edit', $role->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>



			      </tr>
			    @endforeach
		    </tbody>
		</table>

     </div>

</div>


@endsection
