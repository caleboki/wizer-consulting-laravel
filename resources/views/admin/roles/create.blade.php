@extends('_admin')
@section('title', "|Roles")

@section('content')

<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create New Role</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('roles.store')}}" method="post">
		      	@csrf
        		
		        <div class="form-group">
		          <label for="display_name">Name (Human Readable)</label>
                    <input type="text" class="form-control" name="display_name" value="{{old('display_name')}}" id="display_name">
		        </div>

		        <div class="field">
                  <p class="control">
                    <label for="name">Slug (Cannot be changed)</label>
                    <input type="text" class="form-control" name="name" value="{{old('name')}}" id="name">
                  </p>
                  
                </div>

                <div class="field">
                  <p class="control">
                    <label for="description">Description</label>
                    <input type="text" class="form-control" name="description" value="{{old('description')}}" id="description">
                  </p>
                  
                </div>
                <input type="hidden" :value="permissionsSelected" name="permissions">

		        

		        <div class="content">

	                <h2 class="title">Permissions:</h2>

	                  <!-- @foreach ($permissions as $permission)
	                    <div class="field">
	                      <b-checkbox v-model="permissionsSelected" native-value="{{$permission->id}}">{{$permission->display_name}} <em>({{$permission->description}})</em></b-checkbox>
	                    </div>
	                    
	                  @endforeach -->

	                  @foreach($permissions as $permission)
				        <div class="checkbox">
				        	
		        			<label><input type="checkbox" name="permissions[]" value='{{ $permission->id }}'>{{ $permission->display_name }}</label><br>
		  					
						</div>
					  @endforeach
                  

              	</div>

		        
		        
		        

				
		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>

@endsection