@extends('_admin')

@section('title', "|Edit Role")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit {{$role->display_name}}</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('RoleController@update', $role['id'])}}" enctype="multipart/form-data" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">

        		<div class="form-group">
		          <label>Name (Display Name)</label>
		          <input type="text" name="display_name" value="{{$role->display_name}}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Slug (Cannot be changed)</label>
		          <input type="text" name="name" value="{{$role->name}}" class="form-control" disabled>
		        </div>

		        <div class="form-group">
		          <label>Description</label>
		          <input type="text" name="description" id="description" value="{{$role->description}}" class="form-control" required>
		        </div>

		        <input type="hidden" :value="permissionsSelected" name="permissions">
		        <hr>

		        <h5 class="title"><strong>Permission</strong></h5>

	            @foreach ($permissions as $permission)
	                
	                <!-- <div class="form-group">
	                  <b-checkbox v-model="permissionsSelected" native-value="{{$permission->id}}">{{$permission->display_name}} <em>({{$permission->description}})</em></b-checkbox>
	                </div> -->

	                <div class="checkbox">
							    	
				        <label>
				        	<input type="checkbox" value='{{$permission->id}}' name="permissions[]" 
				        	@if(count($permission->roles->where('id', $role->id)) )
							    checked="1"
							@endif>{{$permission->display_name}}
				        	<em>({{$permission->description}})</em>
				        </label>
				        <br>
					</div>
	                
	            @endforeach

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      	</form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

