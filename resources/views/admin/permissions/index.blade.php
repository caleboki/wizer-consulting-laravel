@extends('_admin')

@section('title', "|Permissions")

@section('content')

<div class="col-md-9">
	<div class="panel panel-default">
      <div class="panel-heading main-color-bg">
        <h3 class="panel-title">Permissions</h3>
      </div>
      <div class="panel-body">
      	<button type="button" class="btn btn-default pull-right"><a href="{{route('permissions.create')}}">New Permission</a></button>
      	<table class="table table-striped" id="table">
		    <thead>
		      <tr>

		        <th>Name</th>
		        <th>Slug</th>
		        <th>Description</th>
		        <th></th>
		        <th></th>

		      </tr>
		    </thead>
		    <tbody>
		    	@foreach ($permissions as $permission)

			      <tr>

			        <td>{{$permission->display_name}}</td>
			        <td>{{$permission->name}}</td>
			        <td>{{$permission->description}}</td>
			        <td><a href="{{route('permissions.show', $permission->id)}}"><i class="glyphicon glyphicon-folder-open" ></i></a></td>


			        <td>
			        	<a href="{{ route('permissions.edit', $permission->id) }}">
			        	<i class="glyphicon glyphicon-edit" ></i></a>
			        </td>




			      </tr>
			    @endforeach
		    </tbody>
		</table>

     </div>

</div>

@endsection
