@extends('_admin')

@section('title', "|Permissions")

@section('content')

<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Create New Permission</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{route('permissions.store')}}" method="post">
		      	@csrf

          		<div class="form-group">		        	
		        	
				    <label class="radio-inline"><input type="radio" v-model="permissionType" name="permission_type" value="basic">Basic Permission</label>
					<label class="radio-inline"><input type="radio" v-model="permissionType" name="permission_type" value="crud">CRUD Permission</label>
					
				</div>

				<div class="form-group" v-if="permissionType == 'basic'">
            		<label for="display_name">Name (Display Name)</label>
              		<input type="text" class="form-control" name="display_name" id="display_name">
            		
          		</div>

          		<div class="form-group" v-if="permissionType == 'basic'">
            		<label for="name">Slug</label>
              		<input type="text" class="form-control" name="name" id="name">
            		
          		</div>

          		<div class="form-group" v-if="permissionType == 'basic'">
            		<label for="description">Description</label>
              		<input type="text" class="form-control" name="description" id="description">
            		
          		</div>

          		<div class="form-group" v-if="permissionType == 'crud'">
            		<label for="name">Resource</label>
              		<input type="text" class="form-control" name="resource" v-model="resource" id="resource" placeholder="The name of the resource">
            		
          		</div>

          		
				<div class="col-md-3"  v-if="permissionType == 'crud'">
					<div class="checkbox">
					        	
			        	<label><input type="checkbox" name="create" value='create'>Create</label>
			        	<br>
			  					
					</div>

					<div class="checkbox">
					        	
			        	<label><input type="checkbox" v-model="crudSelected" name="read" value='read'>Read</label>
			        	<br>
			  					
					</div>

					<div class="checkbox">
					        	
			        	<label><input type="checkbox" v-model="crudSelected" name="update" value='create'>Update</label>
			        	<br>
			  					
					</div>

					<div class="checkbox">
					        	
			        	<label><input type="checkbox" v-model="crudSelected" name="delete" value='delete'>Delete</label>
			        	<br>
			  					
					</div>
				</div>

				<input type="hidden" name="crud_selected" :value="crudSelected">

				<div class="col-md-9">
					<table class="table" v-if="resource.length >= 3">
		                <thead>
		                  <th>Name</th>
		                  <th>Slug</th>
		                  <th>Description</th>
		                </thead>
		                <tbody>
		                  <tr v-for="item in crudSelected">
		                    <td v-text="crudName(item)"></td>
		                    <td v-text="crudSlug(item)"></td>
		                    <td v-text="crudDescription(item)"></td>
		                  </tr>
		                </tbody>
              		</table>
				</div>

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save</button>
		      </form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>

@endsection

