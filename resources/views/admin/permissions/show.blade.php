@extends('_admin')
@section('title', "|Permissions")

@section('content')

<div class="col-md-9">
  <div class="panel panel-default">
    <div class="panel-heading main-color-bg">
      <h3 class="panel-title">View Permission</h3>
    </div>
    
    <div class="panel-body">
      <button type="button" class="btn btn-default pull-right"><a href="{{route('permissions.edit', $permission->id)}}" class="button is-primary is-pulled-right">Edit Permission</a></button>
    </div>
        
    <div class="container">
      <p>
        <strong>{{$permission->display_name}}</strong> <small>{{$permission->name}}</small>
        <br>
        {{$permission->description}}
      </p>
    </div>
  </div>
</div>
            
       
     
   
  
@endsection