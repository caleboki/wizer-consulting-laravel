@extends('_admin')

@section('title', "|Edit Permission")
@section('content')
<div class="col-md-9">
	<div class="panel panel-default">
		<div class="panel-heading main-color-bg">
			<h3 class="panel-title">Edit Permission</h3>
		</div>

		<div class="panel-body">
		  <div class="row">
		    <div class="col-md-12">

		      <form action="{{action('PermissionController@update', $permission['id'])}}" enctype="multipart/form-data" method="post" data-smk-icon="glyphicon-remove-sign" class="well">
		      	@csrf
        		<input name="_method" type="hidden" value="PATCH">

        		<div class="form-group">
		          <label>Name (Display Name)</label>
		          <input type="text" name="display_name" value="{{$permission->display_name}}" class="form-control" required>
		        </div>

		        <div class="form-group">
		          <label>Slug (Cannot be changed)</label>
		          <input type="text" name="name" value="{{$permission->name}}" class="form-control" disabled>
		        </div>

		        <div class="form-group">
		          <label>Description</label>
		          <input type="text" name="description" id="description" value="{{$permission->description}}" class="form-control" required>
		        </div>

		        <button type="submit" class="btn btn-default btn-block" id="btnEmpty">Save Changes</button>
		      	</form>

		    </div>
		  </div>

		</div>
	</div>
	
</div>


@endsection

