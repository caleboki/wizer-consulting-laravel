@extends('_main')

@section('title', "|Contact")

@section('content')
<div class="wrapper">
    <!-- Side Menu rt -->
    <div id="overlay"></div>
    
  <div style="background:#ebebeb; padding:50px 0 35px;" class="page-section">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="cs-page-title">
            <h1>Contact Us</h1>

          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Main Start -->
  <div class="main-section"> 
    <!--Section BOX WithOut SideBar-->
    
    <!--Page Section Wide With Right SideBar-->
    <div class="page-section" style=" padding-top:10px; padding-bottom:80px;">
      <div class="container">
        <div class="row">

          <aside class="section-sidebar col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="cs-contact-info view-two">
              <div class="cs-section-title">
                <h2>Get in touch with us</h2>
                
              </div>
              <ul>
                <li>
                  <div class="cs-media"> <i class="fa fa-home cs-color"></i> </div>
                  <div class="cs-text"> <span>Address:</span>
                    <p>78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</p>
                  </div>
                </li>
                <li>
                  <div class="cs-media"> <i class="fa fa-phone cs-color"></i> </div>
                  <div class="cs-text"> <span>Phone No.</span>
                    <p>+234-809-658-9946</p>
                  </div>
                </li>
                <li>
                  <div class="cs-media"> <i class="fa fa-envelope cs-color"></i> </div>
                  <div class="cs-text"> <span>Email Address</span>
                    <p><a href="mailto:info@wizeradvisory.com">info@wizeradvisory.com</a></p>
                  </div>
                </li>
              </ul>
            </div>
          </aside>
        </div>
      </div>
    </div>
    <div class="page-section" style="height:392px;">
      <div class="cs-maps loader">
          <iframe height="392" frameborder="0" allowfullscreen="" style="border: 0px none; width: 100%; pointer-events: none;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.496308885959!2d3.4110436508829522!3d6.458625925681689!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103b8b3662c2be4d%3A0x49202516946a578b!2s78+Lafiaji+Way%2C+Dolphine+Estate+101222%2C+Lagos!5e0!3m2!1sen!2sng!4v1524496864320"></iframe>
      </div>
    </div>
  </div>
  <!-- Main End --> 
  
  <!-- Footer Start -->
    
</div>

<!-- Main End -->


@endsection