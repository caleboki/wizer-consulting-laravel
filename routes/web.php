<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Category;
use App\Course;
use App\Filter;
use App\User;
use App\Training;
use App\Comment;
use App\Organization;


Route::get('/', function () {


	$categories = Category::all();
	$courses = Course::all();
	$comments = Comment::all();
	$filters = Filter::all();
	//$trainings = Training::all();
	$trainings = Training::orderby('start_date', 'asc')->get();
	$organizations = Organization::all();
	$users = User::all();

	//Put homepage path to session, to be accessed from logincontroller
	session(['path' => '/']);

    return view('welcome')->with('categories', $categories)
    					  ->with('courses', $courses)
    					  ->with('filters', $filters)
    					  ->with('comments', $comments)
    					  ->with('trainings', $trainings)
    					  ->with('organizations', $organizations)
    					  ->with('users', $users);
})->name('/');

Route::get('/events/results', function(){

	$events = Training::where('title', 'like', '%' . request('query') . '%');
	$trainings = Training::all();

	return view('events.results')->with('events', $events)
						  		->with('title', 'Search results : ' . request('query'))
								  ->with('query', request('query'))
								  ->with('trainings', $trainings);

});

Route::post('/events/materials', 'EventController@materials');

Route::get('/test', function() {


	return view('test');


});



Auth::routes();

Route::post('/home', 'HomeController@store')->name('home.store');



Route::get('contact', 'EventController@contact')->name('contact'); //Consider a dedicated controller here

Route::resource('events', 'EventController');
Route::resource('courses', 'CurriculumController');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/upcoming', 'HomeController@upcoming')->name('upcoming');
Route::get('/setting', 'HomeController@setting')->name('setting');



Route::post('/setting', 'HomeController@updateSetting')->name('update-setting');

Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');

Route::get('/purchase', function(){



	return view('purchase');

});


Route::post('delegates/add', 'DelegateController@add');

Route::post('delegates/persist', 'DelegateController@persist');

Route::post('delegates/proof', 'DelegateController@payment_proof')->name('delegates.proof');

Route::post('delegates/narration', 'DelegateController@narration')->name('narration.proof');

Route::get('delegates/transactions/update', 'DelegateController@edit_transactions');

Route::post('delegates/transactions/update', 'DelegateController@update_transactions')->name('transactions.update');

Route::post('upload', 'HomeController@upload');

Route::post('/purchase', 'DelegateController@purchase')->name('purchase');

//note to self: consider putting path session here as well just like in home

Route::post('delegates/create', 'DelegateController@create');

Route::resource('delegates', 'DelegateController');

Route::get('/resources', 'ListResourcesController@index');


Route::prefix('admin')->middleware('role:superadministrator|administrator')->group(function () {

	Route::get('/', 'AdminController@index');

	Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');

	Route::resource('categories', 'CategoryController');

	Route::resource('courses', 'CourseController');

	Route::resource('filters', 'FilterController');

	Route::resource('trainings', 'TrainingController');

	Route::resource('instructors', 'InstructorController');

	Route::resource('organizations', 'OrganizationController');

	Route::resource('comments', 'CommentController');

	Route::resource('users', 'UserController');

    Route::resource('materials', 'MaterialController');

    Route::resource('resources', 'ResourceController');

	Route::resource('logos', 'LogoController');

	Route::resource('transactions', 'TransactionController');

	Route::resource('/permissions', 'PermissionController', ['except' => 'destroy']);
    Route::resource('/roles', 'RoleController', ['except' => 'destroy']);

});

