<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create();
        $this->call(LaratrustSeeder::class);
        //factory(App\Category::class, 12)->create();
        //factory(App\Course::class, 10)->create();

        //Comments

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'KPMG',
            'comment' => 'Extremely effective, examples are practical and all questions were clearly and articulately addressed',
        ]);

        DB::table('comments')->insert([
            'title' => 'Participant',
            'organization' => 'Stanbic IBTC',
            'comment' => 'The course was really impactful and massive knowledge acquired. Thumbs up guys.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Operations Officer',
            'organization' => 'International Finance Corporation',
            'comment' => 'The course is well structured and well adapted to the audience.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Participant',
            'organization' => 'Ministry of Finance, Ghana',
            'comment' => 'It is a very good course, the instructor and his assistant took their time to explain every stage in details.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Budget Analyst',
            'organization' => 'Ministry of Finance, Ghana',
            'comment' => 'This course should be conducted for other government officials involved in project appraisals.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Participant',
            'organization' => 'Primera Africa Finance Group',
            'comment' => 'Highly effective .... Training provoked after class topical discussion amongst colleagues',
        ]);

        DB::table('comments')->insert([
            'title' => 'Participant',
            'organization' => 'ARM Pension Managers',
            'comment' => 'Very helpful to our discipline (law) and sheds more light on how to treat financial issues.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Participant',
            'organization' => 'Custodian & Allied Insurance',
            'comment' => 'It has really broadened my knowledge on how Excel works and... I will be able to cascade the knowledge to my colleagues in the office.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Consultant',
            'organization' => 'KPMG',
            'comment' => 'Very very good, the environment, the mix of participants, the instructor, in fact this was the most effective training I ever attended!',
        ]);

        DB::table('comments')->insert([
            'title' => 'Senior Consultant',
            'organization' => 'KPMG',
            'comment' => 'The course clearly opened my mind to the many possibilities in terms of efficiency available in Excel tool. Definitely, my efficiency will increase.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Participant',
            'organization' => 'Stanbic IBTC',
            'comment' => 'Instructor is thorough and attentive to every ones need. Very professional and interested in making sure everyone understands.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Corporate Planning Department',
            'organization' => 'Wema Bank Plc',
            'comment' => 'The course is really a life saver! The modelling part will definitely impact on attendees’ effectiveness...I have learnt a lot of things that will be useful.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Lawyer',
            'organization' => 'Jackson, Etti & Edu.',
            'comment' => 'Very very versed in the subject matter... had a unique ability to bring it down to my level.  This was invaluable',
        ]);

        DB::table('comments')->insert([
            'title' => 'Financial Analyst',
            'organization' => 'Stanbic IBTC Asset Management Limited',
            'comment' => 'This is the most educative training I have attended',
        ]);

        DB::table('comments')->insert([
            'title' => 'Senior Consultant',
            'organization' => 'KPMG',
            'comment' => 'The course revealed to me a lot of ways I can refine and use clients raw data to suit my purpose.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Manager',
            'organization' => 'Monument Assets Limited',
            'comment' => 'Highly effective...excellent communication of ideas/ skills to the class. I learnt at least 50 new functions and shortcuts. Thank you!',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'ARM Pension Managers',
            'comment' => 'The course was quite effective and it’s an eye opener to the endless possibilities in Microsoft Excel.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'Ecobank Development Corporation',
            'comment' => 'He [facilitator] has excellent knowledge of the content of the course and his teaching method is highly effective',
        ]);

        DB::table('comments')->insert([
            'title' => 'Participant',
            'organization' => 'Stanbic IBTC',
            'comment' => 'The course was very intensive but great. Itshowed how simple formulas in Excel, build on each other to calculate complicated ones. Very fascinating',
        ]);

        DB::table('comments')->insert([
            'title' => 'Consultant',
            'organization' => 'KPMG',
            'comment' => 'The course was very effective... It was amazing that all the students were able to build a financial model within 5 days',
        ]);

        DB::table('comments')->insert([
            'title' => 'Financial Analyst',
            'organization' => 'Akintola Williams Deloitte',
            'comment' => 'The course is very intuitive, effective and beneficial to my work in corporate finance.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'Akintola Williams Deloitte',
            'comment' => 'The instructor demonstrated an excellent level of competence and also adapted to lower levels',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'Stanbic IBTC',
            'comment' => 'The course is very effective and can be applied to other works of life',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'Argentil Capital Partners',
            'comment' => 'The course has helped improve my understanding of financial modelling to a large extent. This would go a long way in enhancing the quality of work output and efficiency.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'KPMG',
            'comment' => 'Precise explanations of complex problems are given, with everyday language used. This really drives the point and opens up a world of understanding that was not there before',
        ]);

        DB::table('comments')->insert([
            'title' => 'Program Manager',
            'organization' => 'Private Sector Health Alliance of Nigeria',
            'comment' => 'It was very effective in making me finally comprehend many hitherto obscure concepts for me',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'Stanbic IBTC Bank',
            'comment' => 'Very good content and knowledgeable instructor. My Excelskills as well as modelling ability has certainly improved and I have recommended the course to colleagues and friends',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'KPMG',
            'comment' => 'Overall, the course greatly improved my understanding and interpretation of financial statements. Going forward, I believe I will be able to add more value to my clients in this area',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'Argentil Capital Partners',
            'comment' => 'The step-by-step approach form simple to complex exercises created a seamless transition and made it very easy to understand.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'KPMG',
            'comment' => 'The course is an integral part to build knowledge base of financial statement analysis, especially in the management consulting industry',
        ]);

        DB::table('comments')->insert([
            'title' => 'Credit Officer',
            'organization' => 'RawBank,Democratic Republic of Congo',
            'comment' => 'The course is very interesting and I learnt a lot which I will implement in my professional work.',
        ]);

        DB::table('comments')->insert([
            'title' => 'PPP Advisory',
            'organization' => 'Ministry of Finance, Ghana',
            'comment' => 'The course is a very good one and it should run in Ghana',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'The Nigerian Stock Exchange',
            'comment' => 'Great. Very useful tips and tricks for producing report in Excel, and a great primer on financial modelling. I would recommend the course to anyone who uses Excel regularly.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'Elixir Capital Partners',
            'comment' => 'The course was explosive. It is highly recommended for all finance professionals. It breathes life into you as an aspiring financial professional that is ambitious to be excellent in the profession. It cannot be better, take it from me. It opened my eyes',
        ]);

        DB::table('comments')->insert([
            'title' => 'Analyst',
            'organization' => 'Honeywell Group Limited',
            'comment' => 'The course met my expectations. I would recommend for anybody willing to improve their effectiveness and efficiency at their workplace.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Financial Controller',
            'organization' => 'Honeywell Group Limited',
            'comment' => '...I have attended a lot of Microsoft Excel trainings but this is with a difference.',
        ]);

        DB::table('comments')->insert([
            'title' => 'Financial Reporter',
            'organization' => 'Custodian Life Assurance',
            'comment' => 'Very useful for my deliverables and my reporting routine',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'KPMG',
            'comment' => 'Excellent. The facilitator comes to bare with great pedagogocal skills and practicable experience',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'OutsideIn HR Limited',
            'comment' => 'Great context, more organisations should know about [the Microsoft Excel course]',
        ]);

        DB::table('comments')->insert([
            'title' => 'Associate',
            'organization' => 'KPMG',
            'comment' => 'The course and course materials will go a long way to improving my business analysis skills',
        ]);

        DB::table('comments')->insert([
            'title' => 'HR Intern',
            'organization' => 'OutsideIn HR Limited',
            'comment' => 'I recommend [the Microsoft Word]course for every user of MS Office application. The course would give you a paradigm shift to what you think you know in using MS Word',
        ]);
        
        //Categories

        DB::table('categories')->insert([
            'title' => 'Analyst Development',
            'status' => 'APPROVED'
        ]);

        DB::table('categories')->insert([
            'title' => 'Business Valuation',
            'status' => 'APPROVED'
        ]);

        DB::table('categories')->insert([
            'title' => 'Data Visualisation',
            'status' => 'APPROVED'
        ]);

        DB::table('categories')->insert([
            'title' => 'Financial Modelling',
            'status' => 'APPROVED'
        ]);

        DB::table('categories')->insert([
            'title' => 'Financial Statement Analysis',
            'status' => 'APPROVED'
        ]);

        DB::table('categories')->insert([
            'title' => 'Microsoft Excel ',
            'status' => 'APPROVED'
        ]);

        DB::table('categories')->insert([
            'title' => 'Microsoft Office',
            'status' => 'APPROVED'
        ]);

        //Organizations

        DB::table('organizations')->insert([
            'name' => 'ARM',
            'code' => 123
        ]);

        DB::table('organizations')->insert([
            'name' => 'Access Bank',
            'code' => 245
        ]);

        DB::table('organizations')->insert([
            'name' => 'Dangote Cement',
            'code' => 456
        ]);

        DB::table('organizations')->insert([
            'name' => 'GTB',
            'code' => 007
        ]);

        //Trainings

        DB::table('trainings')->insert([
            'title' => '2-Day Intermediate/Advanced Microsoft Excel',
            'image' => 'assets/uploads/trainings/1528448409int_advExcel.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p><strong>Working with Data</strong></p>
                <ul>
                <li>Subtotals (including multi-level Subtotals)</li>
                <li>Data Validation (including advanced Validation techniques)</li>
                <li>Worksheet Protection and Security</li>
                <li>Circular References</li>
                <li>Creating ‘Tables’ in Excel</li>
                </ul>
                <p><strong>Formatting</strong></p>
                <ul>
                <li>Creating Custom Formats
                <ul>
                <li>Date and Time</li>
                <li>Numbers and Text</li>
                </ul>
                </li>
                <li>Advanced Number Formats</li>
                <li>Working with large numbers</li>
                <li>Conditional Formatting</li>
                </ul>
                <p><strong>Formulas and Functions</strong></p>
                <ul>
                <li>Relative and Absolute Referencing</li>
                <li>Statistical functions: COUNT, SUMIF, COUNTIF, COUNTA, SUMIFS, SMALL, etc.</li>
                <li>Text functions: TEXT, LEN, TRIM, LOWER, UPPER, PROPER, etc.</li>
                <li>Logical functions: IF, OR, AND, IFERROR, ISERROR, etc.</li>
                <li>Date functions: TODAY, MONTH, YEAR, NETWORKDAYS, etc.</li>
                <li>Look-up functions: VLOOKUP (including use of wildcard characters), LOOKUP, etc.</li>
                <li>Financial functions: PMT, etc.</li>
                </ul>
                <p><strong>Introduction to Spreadsheet Modelling Techniques</strong></p>
                <ul>
                <li>Custom Scenario Analysis</li>
                <li>Sensitivity Analysis with Data Tables</li>
                </ul>
                <p><strong>Working with Named Ranges</strong></p>
                <ul>
                <li>Define Named Ranges</li>
                <li>Edit Named Ranges</li>
                <li>Use Named Ranges in Formulas</li>
                </ul>
                <p><strong>Formulae Auditing</strong></p>
                <ul>
                <li>Formula Auditing (Trace, Watch, Evaluate, etc.)</li>
                </ul>
                <p><strong>Analysis Tools</strong></p>
                <ul>
                <li>Advanced Pivot Table techniques</li>
                <li>Goal Seek</li>
                <li>Data Tables</li>
                </ul>
                <p><strong>Charting Tools</strong></p>
                <ul>
                <li>Advanced Charting Techniques</li>
                <li>Creating Custom Analysis Chart Templates</li>
                <li>Creating Graphs and Charts</li>
                <li>Formatting and Editing Charts</li>
                </ul>
                <p><strong>Using Form Controls on Worksheets</strong></p>
                <ul>
                <li>Combo Box</li>
                <li>Spin Button</li>
                <li>Option Button</li>
                </ul>
                <p><strong>Introduction to Macros</strong></p>
                <ul>
                <li>Recording</li>
                <li>Editing</li>
                <li>Attach Macros to Controls</li>
                <li>Automation with Macros</li>
                </ul>',
            'outcome' => '<p>Participants will be able to apply advanced functions and tools in Microsoft Excel to solve more complex problems, analyse and present data clearly and more efficiently with Microsoft Excel tools, produce several spreadsheets/templates using a range of Excel techniques and best practices.</p>',
            'course_delivery' => '<p>In addition to our standard course materials, each participant will, upon completion of the course, receive an annotated version of the MS Excel file(s) used during the course.</p>
                <p>Participants will also receive free helpdesk support for their most difficult spreadsheets for 90 days after the course.</p>',
            'who_should_attend' => '<p>Designed for Excel users, who require higher-level spreadsheet proficiency, due to increased volume/complexity of their day-to-day tasks.</p>',
            'cost' => 130000.00,
            'start_date' => '2018-10-15 09:49:00',
            'end_date' => '2018-10-16 09:49:00'
        ]);

        DB::table('trainings')->insert([
            'title' => '3-day Financial Modelling',
            'image' => 'assets/uploads/trainings/15338168421528197392FinModelling.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p>Financial modelling, more than any other activity, provides the foundation for meaningful budgets and project evaluation. Surveys have however consistently demonstrated that more than 80% of models/spreadsheets contain significant errors – suggesting the possibility of major business decisions being made on the basis of incorrect information! Through this intensive 3-day course, learn to develop robust, consistent and error-free financial models that increase the accuracy of forecasts/ budgets and assist in investment decisions.</p>
                <p><strong>Introduction</strong></p>
                <ul>
                <li>The Modelling Process</li>
                <li>Modelling Best Practices
                <ul>
                <li>Separation, Consistency, Integrity, Protection, Colour Coding, Modular technique</li>
                </ul>
                </li>
                </ul>
                <p><strong>Defining Input/Assumptions</strong></p>
                <ul>
                <li>Macro-economics</li>
                <li>Project Cost</li>
                <li>Financing</li>
                <li>Estimating the Explicit Forecast period</li>
                <li>Revenue</li>
                <li>Expenditure</li>
                <li>Setting-up Scenarios</li>
                </ul>
                <p><strong>Setting-up the Reports</strong></p>
                <ul>
                <li>Statement of Profit or Loss</li>
                <li>Statement of Financial Position (Balance Sheet)</li>
                <li>Statement of Cash Flows</li>
                <li>Summaries/KPIs</li>
                <li>Charts</li>
                </ul>
                <p><strong>Forecasting</strong> - Detailed build-up of revenues and expenditure using the Modular technique</p>
                <ul>
                <li>Revenues</li>
                <li>CAPEX</li>
                <li>OPEX</li>
                <li>Depreciation</li>
                <li>Loan Amortisation and Interest</li>
                <li>CAPM and WACC</li>
                <li>Demonstrate inter-relationships between input, calculation modules and reports</li>
                <li>Double Entry</li>
                <li>Error checks</li>
                </ul>
                <p><strong>Financial Ratios and Key Performance Indicators</strong></p>
                <ul>
                <li>Liquidity Ratios</li>
                <li>Activity Ratios</li>
                <li>Coverage Ratios</li>
                </ul>
                <p><strong>Investment Appraisal/DCF Valuation</strong></p>
                <ul>
                <li>Free Cash Flows</li>
                <li>Cost of Capital Fundamentals</li>
                <li>DCF Valuation Model</li>
                <li>IRR, NPV and Pay-back Analysis</li>
                <li>Enterprise Valuation</li>
                <li>Equity Valuation</li>
                </ul>
                <p><strong>Sensitivity Analysis/Tables</strong></p>
                <ul>
                <li>Data Tables</li>
                </ul>
                <p><strong>Scenario Analysis</strong></p>
                <ul>
                <li>Adding and using Form controls on worksheets</li>
                </ul>
                <p><strong>Simulations</strong></p>
                <ul>
                <li>Break-Even Analysis</li>
                <li>Monte Carlo Simulation</li>
                </ul>
                <p><strong>Optimization and Targeting</strong></p>
                <ul>
                <li>Goal Seek and Solver</li>
                </ul>',

            'outcome' => '<p>All modules are presented with extensive hands-on exercises/case studies that are designed to allow delegates apply concepts learned.</p>',

            'course_delivery' => '<p>Each participant will build from scratch, a comprehensive and robust financial model using various Excel techniques and financial modelling best practices.</p>
                <p>Participants will also be able to:</p>
                <ul>
                <li>incorporate sensitivity and scenario analysis into financial models.</li>
                <li>produce management reports, summaries and meaningful charts.</li>
                </ul>
                <p>In addition to our standard course materials, each participant will, upon completion of the course, receive an annotated version of the MS Excel file(s) used during the course.</p>
                <p>Participants will also receive free helpdesk support for their most difficult spreadsheets for 90 days after the course.</p>',
                'who_should_attend' => '<p>An advanced level proficiency in MS Excel is a prerequisite for this course.</p>
                <p>Requisite skill sets include:</p>
                <ul>
                <li>Working with array functions</li>
                <li>Working with Data Tables (1-dimensional and 2-dimensional Tables)</li>
                <li>Relative&amp; Absolute Referencing</li>
                <li>Working with Named Ranges (define, edit, create from selection)</li>
                <li>Grouping and outlining</li>
                <li>Working with Form controls</li>
                <li>Conditional Formatting</li>
                </ul>',
            
            'cost' => 250000.00,
            'start_date' => '2018-12-05 10:55:00',
            'end_date' => '2018-12-07 10:55:00'
        ]);

        DB::table('trainings')->insert([
            'title' => '2-Day Intermediate/Advanced Microsoft Excel',
            'image' => 'assets/uploads/trainings/15338059921528974678int_advExcel.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p><strong>Working with Data</strong></p>
                <ul>
                <li>Subtotals (including multi-level Subtotals)</li>
                <li>Data Validation (including advanced Validation techniques)</li>
                <li>Worksheet Protection and Security</li>
                <li>Circular References</li>
                <li>Creating ‘Tables’ in Excel</li>
                </ul>
                <p><strong>Formatting</strong></p>
                <ul>
                <li>Creating Custom Formats
                <ul>
                <li>Date and Time</li>
                <li>Numbers and Text</li>
                </ul>
                </li>
                <li>Advanced Number Formats</li>
                <li>Working with large numbers</li>
                <li>Conditional Formatting</li>
                </ul>
                <p><strong>Formulas and Functions</strong></p>
                <ul>
                <li>Relative and Absolute Referencing</li>
                <li>Statistical functions: COUNT, SUMIF, COUNTIF, COUNTA, SUMIFS, SMALL, etc.</li>
                <li>Text functions: TEXT, LEN, TRIM, LOWER, UPPER, PROPER, etc.</li>
                <li>Logical functions: IF, OR, AND, IFERROR, ISERROR, etc.</li>
                <li>Date functions: TODAY, MONTH, YEAR, NETWORKDAYS, etc.</li>
                <li>Look-up functions: VLOOKUP (including use of wildcard characters), LOOKUP, etc.</li>
                <li>Financial functions: PMT, etc.</li>
                </ul>
                <p><strong>Introduction to Spreadsheet Modelling Techniques</strong></p>
                <ul>
                <li>Custom Scenario Analysis</li>
                <li>Sensitivity Analysis with Data Tables</li>
                </ul>
                <p><strong>Working with Named Ranges</strong></p>
                <ul>
                <li>Define Named Ranges</li>
                <li>Edit Named Ranges</li>
                <li>Use Named Ranges in Formulas</li>
                </ul>
                <p><strong>Formulae Auditing</strong></p>
                <ul>
                <li>Formula Auditing (Trace, Watch, Evaluate, etc.)</li>
                </ul>
                <p><strong>Analysis Tools</strong></p>
                <ul>
                <li>Advanced Pivot Table techniques</li>
                <li>Goal Seek</li>
                <li>Data Tables</li>
                </ul>
                <p><strong>Charting Tools</strong></p>
                <ul>
                <li>Advanced Charting Techniques</li>
                <li>Creating Custom Analysis Chart Templates</li>
                <li>Creating Graphs and Charts</li>
                <li>Formatting and Editing Charts</li>
                </ul>
                <p><strong>Using Form Controls on Worksheets</strong></p>
                <ul>
                <li>Combo Box</li>
                <li>Spin Button</li>
                <li>Option Button</li>
                </ul>
                <p><strong>Introduction to Macros</strong></p>
                <ul>
                <li>Recording</li>
                <li>Editing</li>
                <li>Attach Macros to Controls</li>
                <li>Automation with Macros</li>
                </ul>',
            'outcome' => '<p>Designed for Excel users, who require higher-level spreadsheet proficiency, due to increased volume/complexity of their day-to-day tasks.</p><p>All modules are presented with extensive hands-on exercises/case studies that are designed to allow delegates apply concepts learned.</p><p> </p>',
            'course_delivery' => '<p>In addition to our standard course materials, each participant will, upon completion of the course, receive an annotated version of the MS Excel file(s) used during the course.</p>
                <p>Participants will also receive free helpdesk support for their most difficult spreadsheets for 90 days after the course.</p>',
            'who_should_attend' => '<p>Participants will be able to apply advanced functions and tools in Microsoft Excel to solve more complex problems, analyse and present data clearly and more efficiently with Microsoft Excel tools, produce several spreadsheets/templates using a range of Excel techniques and best practices.</p>',
            'cost' => 130000.00,
            'start_date' => '2018-12-03 10:09:47',
            'end_date' => '2018-12-04 10:09:54'
        ]);

        DB::table('trainings')->insert([
            'title' => '2-Day Intermediate/Advanced Microsoft Excel',
            'image' => 'assets/uploads/trainings/15338059921528974678int_advExcel.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p><strong>Working with Data</strong></p>
                <ul>
                <li>Subtotals (including multi-level Subtotals)</li>
                <li>Data Validation (including advanced Validation techniques)</li>
                <li>Worksheet Protection and Security</li>
                <li>Circular References</li>
                <li>Creating ‘Tables’ in Excel</li>
                </ul>
                <p><strong>Formatting</strong></p>
                <ul>
                <li>Creating Custom Formats
                <ul>
                <li>Date and Time</li>
                <li>Numbers and Text</li>
                </ul>
                </li>
                <li>Advanced Number Formats</li>
                <li>Working with large numbers</li>
                <li>Conditional Formatting</li>
                </ul>
                <p><strong>Formulas and Functions</strong></p>
                <ul>
                <li>Relative and Absolute Referencing</li>
                <li>Statistical functions: COUNT, SUMIF, COUNTIF, COUNTA, SUMIFS, SMALL, etc.</li>
                <li>Text functions: TEXT, LEN, TRIM, LOWER, UPPER, PROPER, etc.</li>
                <li>Logical functions: IF, OR, AND, IFERROR, ISERROR, etc.</li>
                <li>Date functions: TODAY, MONTH, YEAR, NETWORKDAYS, etc.</li>
                <li>Look-up functions: VLOOKUP (including use of wildcard characters), LOOKUP, etc.</li>
                <li>Financial functions: PMT, etc.</li>
                </ul>
                <p><strong>Introduction to Spreadsheet Modelling Techniques</strong></p>
                <ul>
                <li>Custom Scenario Analysis</li>
                <li>Sensitivity Analysis with Data Tables</li>
                </ul>
                <p><strong>Working with Named Ranges</strong></p>
                <ul>
                <li>Define Named Ranges</li>
                <li>Edit Named Ranges</li>
                <li>Use Named Ranges in Formulas</li>
                </ul>
                <p><strong>Formulae Auditing</strong></p>
                <ul>
                <li>Formula Auditing (Trace, Watch, Evaluate, etc.)</li>
                </ul>
                <p><strong>Analysis Tools</strong></p>
                <ul>
                <li>Advanced Pivot Table techniques</li>
                <li>Goal Seek</li>
                <li>Data Tables</li>
                </ul>
                <p><strong>Charting Tools</strong></p>
                <ul>
                <li>Advanced Charting Techniques</li>
                <li>Creating Custom Analysis Chart Templates</li>
                <li>Creating Graphs and Charts</li>
                <li>Formatting and Editing Charts</li>
                </ul>
                <p><strong>Using Form Controls on Worksheets</strong></p>
                <ul>
                <li>Combo Box</li>
                <li>Spin Button</li>
                <li>Option Button</li>
                </ul>
                <p><strong>Introduction to Macros</strong></p>
                <ul>
                <li>Recording</li>
                <li>Editing</li>
                <li>Attach Macros to Controls</li>
                <li>Automation with Macros</li>
                </ul>',
            'outcome' => '<p>Designed for Excel users, who require higher-level spreadsheet proficiency, due to increased volume/complexity of their day-to-day tasks.</p><p>All modules are presented with extensive hands-on exercises/case studies that are designed to allow delegates apply concepts learned.</p><p> </p>',
            'course_delivery' => '<p>In addition to our standard course materials, each participant will, upon completion of the course, receive an annotated version of the MS Excel file(s) used during the course.</p>',
            'who_should_attend' => '<p>Participants will be able to apply advanced functions and tools in Microsoft Excel to solve more complex problems, analyse and present data clearly and more efficiently with Microsoft Excel tools, produce several spreadsheets/templates using a range of Excel techniques and best practices.</p>',
            'cost' => 130000.00,
            'start_date' => '2018-12-03 10:09:47',
            'end_date' => '2018-12-04 10:09:54'
        ]);

        DB::table('trainings')->insert([
            'title' => '2-Day Data Visualization With Excel',
            'image' => 'assets/uploads/trainings/15338176891528197215dataviz.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p>Through this intensive 2-day course, learn to develop visualization tools/dashboards to see where your organisation has been and where it’s going. Learn to develop powerful tools that help to track trends, gain insight, see opportunities and take action!</p>
                <p><strong>Rules of Visualization</strong></p>
                <ul>
                <li>Clarity</li>
                <li>Quality</li>
                <li>Simplicity</li>
                <li>Structure</li>
                </ul>
                <p><strong>Working with Source Data</strong></p>
                <ul>
                <li>Setting Up Source Data</li>
                <li>Use of Auxiliary Data Columns/Rows</li>
                </ul>
                <p><strong>Advanced Charting Techniques</strong></p>
                <ul>
                <li>Choosing the right Chart</li>
                <li>Font Selection</li>
                <li>Creating Chart Templates</li>
                <li>Working with 3-D Charts</li>
                <li>Snap to Grid</li>
                <li>Use of Symbols and Enhancements</li>
                <li>Alternate Plot Area with Different Colours</li>
                </ul>
                <p><strong>Special Charts</strong></p>
                <ul>
                <li>GANTT Charts</li>
                <li>Waterfall Charts</li>
                <li>Vertical’ Line Charts</li>
                <li>Tornado Charts</li>
                <li>Special Effect Pie Charts</li>
                <li>Square’ Pie Charts</li>
                <li>Dynamic Charts</li>
                </ul>
                <p><strong>Dashboard Design</strong></p>
                <ul>
                <li>Dashboard Layout/Arrangement</li>
                <li>Pre-formatting the Worksheet</li>
                <li>Creating the Dashboard</li>
                <li>Chart Formatting Techniques (Chart Area, Plot Area, Axis, Legend, etc.)</li>
                <li>Using Tables</li>
                <li>Colour Schemes</li>
                <li>Chart Layouts</li>
                <li>Working with Tables</li>
                <li>How to use Excel’s Camera tool</li>
                <li>Magazine-Quality Colours and Themes</li>
                <li>Automating Charts with Form Controls:
                <ul>
                <li>Spin button</li>
                <li>Option box</li>
                <li>Combo box</li>
                <li>Link Excel Charts to MS Word/MS PowerPoint (with automatic update)</li>
                </ul>
                </li>
                <li>Sample Visualisation with Dynamic Maps</li>
                </ul>',
            'outcome' => '<p>… visualize information in ways you never imagined were possible!</p>',
            'course_delivery' => '<p>All modules are presented with extensive hands-on exercises/case studies that are designed to allow delegates apply concepts learned.</p>',
            'who_should_attend' => '<p>An advanced level proficiency in MS Excel is a pre-requisite for this course.</p><p></p>',
            'cost' => 150000.00,
            'start_date' => '2018-09-13 10:38:20',
            'end_date' => '2018-09-14 10:38:25'
        ]);

        DB::table('trainings')->insert([
            'title' => '3-day Financial Modelling',
            'image' => 'assets/uploads/trainings/15338167941528197392FinModelling.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p>Financial modelling, more than any other activity, provides the foundation for meaningful budgets and project evaluation. Surveys have however consistently demonstrated that more than 80% of models/spreadsheets contain significant errors – suggesting the possibility of major business decisions being made on the basis of incorrect information! Through this intensive 3-day course, learn to develop robust, consistent and error-free financial models that increase the accuracy of forecasts/ budgets and assist in investment decisions.</p>
                <p><strong>Introduction</strong></p>
                <ul>
                <li>The Modelling Process</li>
                <li>Modelling Best Practices
                <ul>
                <li>Separation, Consistency, Integrity, Protection, Colour Coding, Modular technique</li>
                </ul>
                </li>
                </ul>
                <p><strong>Defining Input/Assumptions</strong></p>
                <ul>
                <li>Macro-economics</li>
                <li>Project Cost</li>
                <li>Financing</li>
                <li>Estimating the Explicit Forecast period</li>
                <li>Revenue</li>
                <li>Expenditure</li>
                <li>Setting-up Scenarios</li>
                </ul>
                <p><strong>Setting-up the Reports</strong></p>
                <ul>
                <li>Statement of Profit or Loss</li>
                <li>Statement of Financial Position (Balance Sheet)</li>
                <li>Statement of Cash Flows</li>
                <li>Summaries/KPIs</li>
                <li>Charts</li>
                </ul>
                <p><strong>Forecasting</strong> - Detailed build-up of revenues and expenditure using the Modular technique</p>
                <ul>
                <li>Revenues</li>
                <li>CAPEX</li>
                <li>OPEX</li>
                <li>Depreciation</li>
                <li>Loan Amortisation and Interest</li>
                <li>CAPM and WACC</li>
                <li>Demonstrate inter-relationships between input, calculation modules and reports</li>
                <li>Double Entry</li>
                <li>Error checks</li>
                </ul>
                <p><strong>Financial Ratios and Key Performance Indicators</strong></p>
                <ul>
                <li>Liquidity Ratios</li>
                <li>Activity Ratios</li>
                <li>Coverage Ratios</li>
                </ul>
                <p><strong>Investment Appraisal/DCF Valuation</strong></p>
                <ul>
                <li>Free Cash Flows</li>
                <li>Cost of Capital Fundamentals</li>
                <li>DCF Valuation Model</li>
                <li>IRR, NPV and Pay-back Analysis</li>
                <li>Enterprise Valuation</li>
                <li>Equity Valuation</li>
                </ul>
                <p><strong>Sensitivity Analysis/Tables</strong></p>
                <ul>
                <li>Data Tables</li>
                </ul>
                <p><strong>Scenario Analysis</strong></p>
                <ul>
                <li>Adding and using Form controls on worksheets</li>
                </ul>
                <p><strong>Simulations</strong></p>
                <ul>
                <li>Break-Even Analysis</li>
                <li>Monte Carlo Simulation</li>
                </ul>
                <p><strong>Optimization and Targeting</strong></p>
                <ul>
                <li>Goal Seek and Solver</li>
                </ul>',
            'outcome' => '<p>An advanced level proficiency in MS Excel is a prerequisite for this course.</p>
                <p>Requisite skill sets include:</p>
                <ul>
                <li>Working with array functions</li>
                <li>Working with Data Tables (1-dimensional and 2-dimensional Tables)</li>
                <li>Relative&amp; Absolute Referencing</li>
                <li>Working with Named Ranges (define, edit, create from selection)</li>
                <li>Grouping and outlining</li>
                <li>Working with Form controls</li>
                <li>Conditional Formatting</li>
                </ul>',
            'course_delivery' => '<p>Each participant will build from scratch, a comprehensive and robust financial model using various Excel techniques and financial modelling best practices.</p><p>Participants will also be able to:</p>
                <ul>
                <li>incorporate sensitivity and scenario analysis into financial models.</li>
                <li>produce management reports, summaries and meaningful charts.</li>
                </ul>
                <p>In addition to our standard course materials, each participant will, upon completion of the course, receive an annotated version of the MS Excel file(s) used during the course.</p>
                <p>Participants will also receive free helpdesk support for their most difficult spreadsheets for 90 days after the course.</p>',
            'who_should_attend' => '<p>All modules are presented with extensive hands-on exercises/case studies that are designed to allow delegates apply concepts learned.</p>',
            'cost' => 250000.00,
            'start_date' => '2018-10-17 10:46:41',
            'end_date' => '2018-10-19 10:46:47'
        ]);

        DB::table('trainings')->insert([
            'title' => '3-day Basic/Intermediate Microsoft Excel',
            'image' => 'assets/uploads/trainings/15338244271528557357.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p><strong>Worksheet Mechanics</strong></p>
                <ul>
                <li>The Ribbon (Tabs, Groups and Commands)</li>
                <li>Time-saving Worksheet Navigation Techniques and Keyboard Short-cuts</li>
                <li>The Quick Access Toolbar</li>
                <li>Insert/Delete Rows and Columns</li>
                <li>How to:
                <ul>
                <li>Select Non-Contiguous Cells/Ranges</li>
                <li>Control Your Worksheet View</li>
                <li>Group Worksheets</li>
                <li>Create a Data Series</li>
                </ul>
                </li>
                <li>Numbers</li>
                <li>Dates
                <ul>
                <li>Use Comments to Explain Cell Contents</li>
                </ul>
                </li>
                </ul>
                <p><strong>Working with Databases</strong></p>
                <ul>
                <li>Define an Excel ‘Database’/‘Table’</li>
                <li>Database/Analysis Tools
                <ul>
                <li>Sort</li>
                <li>Filter</li>
                <li>Working with Text Files (the Text-to-Column Wizard)</li>
                <li>Sub-Total</li>
                <li>Data Validation</li>
                </ul>
                </li>
                <li>Printing Techniques
                <ul>
                <li>Working with Page Set-Up: Headers and Footers, Repeat Header Rows, Set Print Area, Scaling, etc.</li>
                </ul>
                </li>
                </ul>
                <p><strong>Formulas and Functions</strong></p>
                <ul>
                <li>Building Formulas/Expressions to Perform Calculations</li>
                <li>Operator Order</li>
                <li>Relative and Absolute Referencing</li>
                <li>Statistical Functions: SUMIF, SUMPRODUCT, AVERAGE, COUNT, COUNTIF, etc.</li>
                </ul>
                <p><strong>Formulae and Functions (cont’d)</strong></p>
                <ul>
                <li>Text Functions: CONCATENATE, UPPER, TRIM, PROPER etc.</li>
                <li>The IF function</li>
                <li>The VLOOKUP function</li>
                <li>‘Nested’ Functions</li>
                <li>How Excel Works with Dates</li>
                <li>Date Functions e.g. TODAY, NOW, etc.</li>
                <li>Calculations involving Dates</li>
                </ul>
                <p><strong>Formatting</strong></p>
                <ul>
                <li>Working with the ‘Format Cells’ menu</li>
                <li>Conditional Formatting</li>
                <li>Introduction to Custom Formatting</li>
                </ul>
                <p><strong>Analysis Tools</strong></p>
                <ul>
                <li>Pivot Tables
                <ul>
                <li>Anatomy of a PivotTable Report</li>
                <li>Grouping Data and other items</li>
                <li>Using the Report Filter</li>
                <li>Pivot Charts</li>
                </ul>
                </li>
                <li>Charts
                <ul>
                <li>Anatomy of a Chart</li>
                <li>Working with Axes, Plot Area, Legend, etc.</li>
                <li>Charting Techniques
                <ul>
                <li>Stacked Bar Chart</li>
                <li>Pie ‘Explosion’</li>
                </ul>
                </li>
                </ul>
                </li>
                </ul>
                <p>Creative Use of Colours for Emphasis</p>',
            'outcome' => '<p>Participants will be able to:</p>
                <ul>
                <li>perform tasks efficiently;</li>
                <li>write formulas, and use functions in MS Excel;</li>
                <li>create professional charts;</li>
                <li>work with Pivot tables and other tools in Excel.</li>
                </ul>',
            'course_delivery' => '<p>All modules are presented with extensive hands-on exercises/case studies that are designed to allow delegates apply concepts learned.</p>',
            'who_should_attend' => '<p>Designed for users with basic-level knowledge, this 3-day course teaches participants about worksheet mechanics and time-saving navigation techniques, creating and working with databases, working with functions and formulas, formatting (including conditional formatting), creating and working with Pivot Tables, etc.</p>',
            'cost' => 135000.00,
            'start_date' => '2018-11-05 15:20:04',
            'end_date' => '2018-11-07 15:20:11'
        ]);

        DB::table('trainings')->insert([
            'title' => 'Excel For Experts',
            'image' => 'assets/uploads/trainings/15338252181528445095expert_Excel.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p>Learn to design sophisticated spreadsheets/models that you never imagined were possible! Participants are encouraged to bring their most challenging spreadsheets/models to class.</p>',
            'outcome' => '<p>Participants will be able to:</p>
                <ul>
                <li>apply advanced tools/functions in Excel;</li>
                <li>write array formulas and work with array functions</li>
                <li>create user-defined functions;</li>
                <li>learn to edit and debug Visual Basic for Applications (VBA) codes.</li>
                </ul>',
            'course_delivery' => '<p>In addition to our standard course materials, each participant will, upon completion of the course, receive an annotated version of the MS Excel file(s) used during the course.</p>
                <p>Participants will also receive free helpdesk support for their most difficult spreadsheets for 90 days after the course.</p>',
            'who_should_attend' => '<p>This course is designed for advanced users who require the highest level of proficiency in Microsoft Excel.</p>',
            'cost' => 150000.00,
            'start_date' => '2018-09-10 15:27:21',
            'end_date' => '2018-09-12 15:27:27'
        ]);


        DB::table('trainings')->insert([
            'title' => '5-day Intermediate/Advanced Excel & Financial Modelling',
            'image' => 'assets/uploads/trainings/15338303221528456883FinModelling.jpg',
            'venue' => '<p><span style="color:#999999;font-family:\'Open Sans\', sans-serif;background-color:#fbfbfb;">78b Lafiaji way, Dolphin Estate, Ikoyi, Lagos.</span></p>',
            'description' => '<p><em>Intermediate/Advanced Excel </em></p>
                <p><strong>Working with Data</strong></p>
                <ul>
                <li>Subtotals (including multi-level Subtotals)</li>
                <li>Data Validation (including advanced Validation techniques)</li>
                <li>Worksheet Protection and Security</li>
                <li>Circular References</li>
                <li>Creating ‘Tables’ in Excel</li>
                </ul>
                <p><strong>Formatting</strong></p>
                <ul>
                <li>Creating Custom Formats
                <ul>
                <li>Date and Time</li>
                <li>Numbers and Text</li>
                </ul>
                </li>
                <li>Advanced Number Formats</li>
                <li>Working with large numbers</li>
                <li>Conditional Formatting</li>
                </ul>
                <p><strong>Formulas and Functions</strong></p>
                <ul>
                <li>Relative and Absolute Referencing</li>
                <li>Statistical functions: COUNT, SUMIF, COUNTIF, COUNTA, SUMIFS, SMALL, etc.</li>
                <li>Text functions: TEXT, LEN, TRIM, LOWER, UPPER, PROPER, etc.</li>
                <li>Logical functions: IF, OR, AND, IFERROR, ISERROR, etc.</li>
                <li>Date functions: TODAY, MONTH, YEAR, NETWORKDAYS, etc.</li>
                <li>Look-up functions: VLOOKUP (including use of wildcard characters), LOOKUP, etc.</li>
                <li>Financial functions: PMT, etc.</li>
                </ul>
                <p><strong>Introduction to Spreadsheet Modelling Techniques</strong></p>
                <ul>
                <li>Custom Scenario Analysis</li>
                <li>Sensitivity Analysis with Data Tables</li>
                </ul>
                <p><strong>Working with Named Ranges</strong></p>
                <ul>
                <li>Define Named Ranges</li>
                <li>Edit Named Ranges</li>
                <li>Use Named Ranges in Formulas</li>
                </ul>
                <p><strong>Formulae Auditing</strong></p>
                <ul>
                <li>Formula Auditing (Trace, Watch, Evaluate, etc.)</li>
                </ul>
                <p><strong>Analysis Tools</strong></p>
                <ul>
                <li>Advanced Pivot Table techniques</li>
                <li>Goal Seek</li>
                <li>Data Tables</li>
                </ul>
                <p><strong>Charting Tools</strong></p>
                <ul>
                <li>Advanced Charting Techniques</li>
                <li>Creating Custom Analysis Chart Templates</li>
                <li>Creating Graphs and Charts</li>
                <li>Formatting and Editing Charts</li>
                </ul>
                <p><strong>Using Form Controls on Worksheets</strong></p>
                <ul>
                <li>Combo Box</li>
                <li>Spin Button</li>
                <li>Option Button</li>
                </ul>
                <p><strong>Introduction to Macros</strong></p>
                <ul>
                <li>Recording</li>
                <li>Editing</li>
                <li>Attach Macros to Controls</li>
                <li>Automation with Macros</li>
                </ul>
                <p><em>Financial Modelling</em></p>
                <p><strong>Introduction</strong></p>
                <ul>
                <li>Overview of Project Finance</li>
                <li>Implications of project finance terms/risks for financial modelling</li>
                </ul>
                <p><strong>Model Structure/Input Assumptions</strong></p>
                <ul>
                <li>Modelling Best Practices</li>
                <li>Project Cost</li>
                <li>Set up value drivers</li>
                <li>Set up Scenarios</li>
                </ul>
                <p><strong>Financial Analysis &amp; Modelling</strong></p>
                <ul>
                <li>Set up the financial statements (Statement of Profit or Loss, Statement of Financial Position and Statement of Cash Flows)</li>
                </ul>
                <p><strong>Modelling Mechanics</strong></p>
                <ul>
                <li>Include switch for ‘roll-up’ of time periods (monthly, quarterly, semiannually, annually)</li>
                <li>Include switch to accommodate construction delays</li>
                <li>How to deal with Circular References and alternative approaches</li>
                <li>‘Good’ Circular References</li>
                <li>Error checks</li>
                <li>Include switches for different debt repayment profiles (level, annuity, etc.)</li>
                <li>Separation of Construction and Operation time periods and time flexibility</li>
                <li>Use of ‘flags’ for model timing</li>
                <li>Modelling detailed Source &amp; Use statement to account for construction cost profile and draw-down of different financing sources</li>
                <li>Calculating the cost of different financing sources (senior/subordinated debt, preference shares, equity, etc.) and the WACC</li>
                <li>Calculating the cost of equity</li>
                <li>Modelling Cash Waterfall</li>
                <li>Determine optimal capital structure</li>
                <li>Modelling complex debt amortisation schedules (including capitalisation of Interest During Construction)</li>
                </ul>
                <p><strong>Project Evaluation</strong></p>
                <ul>
                <li>Net Cash from operations before debt Service</li>
                <li>Modelling Debt Covenants</li>
                <li>Cash flow to equity and debt providers</li>
                <li>Return to Equity, NPV, IRR and other measures</li>
                </ul>
                <p><strong>Risk and Uncertainty</strong></p>
                <ul>
                <li>Identifying and modelling project risks</li>
                </ul>
                <p><strong>Analysis Tools</strong></p>
                <ul>
                <li>Sensitivity analysis</li>
                <li>Scenario analysis</li>
                <li>Monte Carlo simulation</li>
                </ul>
                <p><strong>Optimization and Targeting</strong></p>
                <ul>
                <li>Use Goal Seek and Solver</li>
                </ul>',
            'outcome' => '<p>Participants will be able to apply advanced functions and tools in Microsoft Excel to solve more complex problems, analyse and present data clearly and more efficiently with Microsoft Excel tools, produce several spreadsheets/templates using a range of Excel techniques and best practices.</p>',
            'course_delivery' => '<p>All modules are presented with extensive hands-on exercises/case studies that are designed to allow delegates apply concepts learned.</p><p> </p>',
            'who_should_attend' => '<p>Designed for Excel users, who require higher-level spreadsheet proficiency, due to increased volume/complexity of their day-to-day tasks.</p><p> </p><p> </p>',
            'cost' => 330000.00,
            'start_date' => '2018-10-15 16:10:42',
            'end_date' => '2018-10-19 16:10:48'
        ]);



        //Instructors
        
         DB::table('instructors')->insert([
            'first_name' => 'Lanre',
            'last_name' => 'Akinbo',
            'title' => 'Managing Director',
            'email' => 'lanre.akinbo@wizeradvisory.com',
            'about' => '<p><span style="font-family: \'Trebuchet MS\', Arial; font-size: 12px; text-align: justify;">Lanre is a corporate and transactions services professional with extensive experience in financial analysis/modelling, valuation, financial due diligence, feasibility studies, business strategy development, training and research.</span></p>',
            'avatar' => 'assets/uploads/instructors/LanreAkinbo.png'
        ]);

        //Courses
        
        DB::table('courses')->insert([
            'title' => 'Data Visualisation With Excel',
            'image' => 'assets/uploads/courses/1528442957dataviz.jpg',
            'description' => '<p>In this course, you will learn to develop visualization tools/dashboards to see where your organisation has been and where it’s going. Learn to develop powerful tools that help to track trends, gain insight, see opportunities and take action!</p><p><strong>Rules of Visualisation</strong></p><ul><li>Clarity</li><li>Quality</li><li>Simplicity</li><li>Structure</li></ul><p><strong>Working with Source Data</strong></p><ul><li>Setting Up Source Data</li><li>Use of Auxiliary Data Columns/Rows</li></ul><p><strong>Advanced Charting Techniques</strong></p><ul><li>Choosing the right Chart</li><li>Font Selection</li><li>Creating Chart Templates</li><li>Working with 3-D Charts</li><li>Snap to Grid</li><li>Use of Symbols and Enhancements</li><li>Alternate Plot Area with Different Colours</li></ul><p><strong>Special Charts</strong></p><ul><li>GANTT Charts</li><li>Waterfall Charts</li><li>‘Vertical’ Line Charts</li><li>Tornado Charts</li><li>Special Effect Pie Charts</li><li>‘Square’ Pie Charts</li><li>Dynamic Charts</li></ul><p><strong>Dashboard Design</strong></p><ul><li>Dashboard Layout/Arrangement</li><li>Pre-formatting the Worksheet</li><li>Creating the Dashboard</li><li>Chart Formatting Techniques (Chart Area, Plot Area, Axis, Legend, etc.)</li><li>Using Tables</li><li>Colour Schemes</li><li>Chart Layouts</li><li>Working with Tables</li><li>How to use Excel’s Camera tool</li><li>Magazine-Quality Colours and Themes</li><li>Automating Charts with Form Controls:<ul><li>Spin button</li><li>Option box</li><li>Combo box</li><li>Link Excel Charts to MS Word/MSmPowerPoint (with automatic update)</li></ul></li><li>Sample Visualisation with Dynamic Maps</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Basic/Intermediate Microsoft Excel',
            'image' => 'assets/uploads/courses/1528557357.jpg',
            'description' => '<p>Designed for users with basic-level knowledge, this course teaches participants about worksheet mechanics and time-saving navigation techniques, creating and working with databases, working with functions and formulas, formatting (including conditional formatting), creating and working with Pivot Tables, etc.</p><p>Participants will also learn helpful hints, tips and tricks and keyboard shortcuts.</p><p><strong>Worksheet Mechanics</strong></p><ul><li>The Ribbon (Tabs, Groups and Commands)</li><li>Time-saving Worksheet Navigation Techniques and Keyboard Short-cuts</li><li>The Quick Access Toolbar</li><li>Insert/Delete Rows and Columns</li><li>How to:<ul><li>Select Non-Contiguous Cells/Ranges</li><li>Control Your Worksheet View</li><li>Group Worksheets</li><li>Create a Data Series</li></ul></li><li>Numbers</li><li>Dates<ul><li>Use Comments to Explain Cell Contents</li></ul></li></ul><p><strong>Working with Databases</strong></p><ul><li>Define an Excel ‘Database’/‘Table’</li><li>Database/Analysis Tools<ul><li>Sort</li><li>Filter</li><li>Working with Text Files (the Text-to-Column Wizard)</li><li>Sub-Total</li><li>Data Validation</li></ul></li><li>Printing Techniques<ul><li>Working with Page Set-Up: Headers and Footers, Repeat Header Rows, Set Print Area, Scaling, etc.</li></ul></li></ul><p><strong>Formulas and Functions</strong></p><ul><li>Building Formulas/Expressions to Perform Calculations</li><li>Operator Order</li><li>Relative and Absolute Referencing</li><li>Statistical Functions: SUMIF, SUMPRODUCT, AVERAGE, COUNT, COUNTIF, etc.</li></ul><p><strong>Formulae and Functions (cont’d)</strong></p><ul><li>Text Functions: CONCATENATE, UPPER, TRIM, PROPER etc.</li><li>The IF function</li><li>The VLOOKUP function</li><li>‘Nested’ Functions</li><li>How Excel Works with Dates</li><li>Date Functions e.g. TODAY, NOW, etc.</li><li>Calculations involving Dates</li></ul><p><strong>Formatting</strong></p><ul><li>Working with the ‘Format Cells’ menu</li><li>Conditional Formatting</li><li>Introduction to Custom Formatting</li></ul><p><strong>Analysis Tools</strong></p><ul><li>Pivot Tables<ul><li>Anatomy of a PivotTable Report</li><li>Grouping Data and other items</li><li>Using the Report Filter</li><li>Pivot Charts</li></ul></li><li>Charts<ul><li>Anatomy of a Chart</li><li>Working with Axes, Plot Area, Legend, etc.</li><li>Charting Techniques<ul><li>Stacked Bar Chart</li><li>Pie ‘Explosion’</li></ul></li><li>Creative Use of Colours for Emphasis</li></ul></li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Intermediate/Advanced MS Excel',
            'image' => 'assets/uploads/courses/1528444359int_advExcel.jpg',
            'description' => '<p>This course teaches participants about advanced functions, analysis tools, custom reports, professional charting techniques, automation with macros, use of form controls, etc. Participants will also learn helpful hints, tips and tricks and keyboard shortcuts.</p><p><strong>Working with Data</strong></p><ul><li>Subtotals (including multi-level Subtotals)</li><li>Data Validation (including advanced Validation techniques)</li><li>Worksheet Protection and Security</li><li>Circular References</li><li>Creating ‘Tables’ in Excel</li></ul><p><strong>Formatting</strong></p><ul><li>Creating Custom Formats<ul><li>Date and Time</li><li>Numbers and Text</li></ul></li><li>Advanced Number Formats</li><li>Working with large numbers</li><li>Conditional Formatting</li></ul><p><strong>Formulas and Functions</strong></p><ul><li>Relative and Absolute Referencing</li><li>Statistical functions: COUNT, SUMIF, COUNTIF, COUNTA, SUMIFS, SMALL, etc.</li><li>Text functions: TEXT, LEN, TRIM, LOWER, UPPER, PROPER, etc.</li><li>Logical functions: IF, OR, AND, IFERROR, ISERROR, etc.</li><li>Date functions: TODAY, MONTH, YEAR, NETWORKDAYS, etc.</li><li>Look-up functions: VLOOKUP (including use of wildcard characters), LOOKUP, etc.</li><li>Financial functions: PMT, etc.</li></ul><p><strong>Introduction to Spreadsheet Modelling Techniques</strong></p><ul><li>Custom Scenario Analysis</li><li>Sensitivity Analysis with Data Tables</li></ul><p><strong>Working with Named Ranges</strong></p><ul><li>Define Named Ranges</li><li>Edit Named Ranges</li><li>Use Named Ranges in Formulas</li></ul><p><strong>Formulae Auditing</strong></p><ul><li>Formula Auditing (Trace, Watch, Evaluate, etc.)</li></ul><p><strong>Analysis Tools</strong></p><ul><li>Advanced Pivot Table techniques</li><li>Goal Seek</li><li>Data Tables</li></ul><p><strong>Charting Tools</strong></p><ul><li>Advanced Charting Techniques</li><li>Creating Custom Analysis Chart Templates</li><li>Creating Graphs and Charts</li><li>Formatting and Editing Charts</li></ul><p><strong>Using Form Controls on Worksheets</strong></p><ul><li>Combo Box</li><li>Spin Button</li><li>Option Button</li></ul><p><strong>Introduction to Macros</strong></p><ul><li>Recording</li><li>Editing</li><li>Attach Macros to Controls</li><li>Automation with Macros</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Excel For Experts',
            'image' => 'assets/uploads/courses/1528445095expert_Excel.jpg',
            'description' => '<p>This course is targeted at advanced users who require the highest level of proficiency in Microsoft Excel. Learn about creating your own custom functions, automating your spreadsheets with macros, advanced uses of Visual Basic for Applications (VBA), creating dynamic charts, working with advanced tools, etc.</p><p><strong>Manipulating Data</strong></p><ul><li>Advanced Data Validation; including custom validation using formulas</li><li>Hide Formulas</li><li>The ‘Go to Special…’ menu</li><li>Database Function: DAVERAGE, DCOUNT, etc.</li></ul><p><strong>Formulae and Functions</strong></p><ul><li>Design Custom Reports</li><li>Writing Custom Formulas</li><li>Advanced Functions (INDEX, MATCH, SMALL, etc.)</li><li>VLOOKUP vs. INDEX-MATCH combination</li><li>Use of wildcard characters in look-up functions</li><li>User-Defined Functions (UDFs)</li><li>Work with Excel ‘Tables’</li><li>Wrap Formula/Expression Results</li></ul><p><strong>Working with Named Ranges</strong></p><ul><li>Create dynamic Named Ranges with Formulas</li><li>Work with Array Formulas and Functions</li></ul><p><strong>Advanced Charting Tools</strong></p><ul><li>Create Dynamic Charts (e.g. using one chart for varying time series, locations, etc.)</li><li>Use Form Controls for Charts</li><li>Charts with Secondary Axis</li><li>Link Charts to MS Word and PowerPoint, with automatic update</li><li>Special charts<ul><li>Waterfall Chart</li><li>GANTT Chart</li><li>Tornado Chart</li></ul></li></ul><p><strong>Dashboard Design</strong></p><ul><li>Dashboard Layout</li><li>Pre-formatting the Worksheet</li><li>Automating Charts with use of Form Controls:</li><li>Spin button</li><li>Option box</li></ul><p><strong>Formatting</strong></p><ul><li>Advanced Custom Formats</li><li>Apply Custom Conditional Formattingmwith Formulas</li><li>Create GANTT Charts with Conditional Formatting</li></ul><p><strong>Automation</strong></p><ul><li>Building Scenarios</li><li>Sensitivity Tables</li><li>Optimisation using Solver</li><li>Using Form Controls on Worksheets<ul><li>Option Button</li><li>Spin Button</li><li>Combo Box</li><li>Track Changes by Multiple Users</li></ul></li></ul><p><strong>Visual Basic for Applications</strong></p><ul><li>Introduction</li><li>Recording Macros</li><li>Assigning Macros to Form Controls</li><li>Editing Macros</li><li>Key Visual Basic commands e.g.<ul><li>- IF…..THEN</li><li>DO UNTIL…….LOOP</li><li>DO WHILE…….LOOP</li><li>WITH ….END WITH</li></ul></li><li>Working with ‘ActiveX’ Controls</li><li>Testing and Debugging Code</li><li>Designing / implementing custom Applications</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Financial Modelling With MS Excel',
            'image' => 'assets/uploads/courses/1528456883FinModelling.jpg',
            'description' => '<p>Financial modelling, more than any other activity, provides the foundation for meaningful budgets and project evaluation. Surveys have however consistently demonstrated that more than 80% of models/spreadsheets contain significant errors – suggesting the possibility of major business decisions being made on the basis of incorrect information! Through this intensive 3-day course, learn to develop robust, consistent and error-free financial models that increase the accuracy of forecasts/ budgets and assist in investment decisions.</p><p><strong>Introduction</strong></p><ul><li>The Modelling Process</li><li>Modelling Best Practices<ul><li>Separation, Consistency, Integrity, Protection, Colour Coding, Modular technique</li></ul></li></ul><p><strong>Defining Input/Assumptions</strong></p><ul><li>Macro-economics</li><li>Project Cost</li><li>Financing</li><li>Estimating the Explicit Forecast period</li><li>Revenue</li><li>Expenditure</li><li>Setting-up Scenarios</li></ul><p><strong>Setting-up the Reports</strong></p><ul><li>Statement of Profit or Loss</li><li>Statement of Financial Position (Balance Sheet)</li><li>Statement of Cash Flows</li><li>Summaries/KPIs</li><li>Charts</li></ul><p><strong>Forecasting</strong> - Detailed build-up of revenues and expenditure using the Modular technique</p><ul><li>Revenues</li><li>CAPEX</li><li>OPEX</li><li>Depreciation</li><li>Loan Amortisation and Interest</li><li>CAPM and WACC</li><li>Demonstrate inter-relationships between input, calculation modules and reports</li><li>Double Entry</li><li>Error checks</li></ul><p><strong>Financial Ratios and Key Performance Indicators</strong></p><ul><li>Liquidity Ratios</li><li>Activity Ratios</li><li>Coverage Ratios</li></ul><p><strong>Investment Appraisal/DCF Valuation</strong></p><ul><li>Free Cash Flows</li><li>Cost of Capital Fundamentals</li><li>DCF Valuation Model</li><li>IRR, NPV and Pay-back Analysis</li><li>Enterprise Valuation</li><li>Equity Valuation</li></ul><p><strong>Sensitivity Analysis/Tables</strong></p><ul><li>Data Tables</li></ul><p><strong>Scenario Analysis</strong></p><ul><li>Adding and using Form controls on worksheets</li></ul><p><strong>Simulations</strong></p><ul><li>Break-Even Analysis</li><li>Monte Carlo Simulation</li></ul><p><strong>Optimization and Targeting</strong></p><ul><li>Goal Seek and Solver</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Advanced Financial Modelling with MS Excel',
            'image' => 'assets/uploads/courses/1528454363adv_FinModelling.jpg',
            'description' => '<p><strong>Introduction</strong></p><ul><li>Overview of Project Finance</li><li>Implications of project finance terms/risks for financial modelling</li></ul><p><strong>Model Structure/Input Assumptions</strong></p><ul><li>Modelling Best Practices</li><li>Project Cost</li><li>Set up value drivers</li><li>Set up Scenarios</li></ul><p><strong>Financial Analysis &amp; Modelling</strong></p><ul><li>Set up the financial statements (Statement of Profit or Loss, Statement of Financial Position and Statement of Cash Flows)</li></ul><p><strong>Modelling Mechanics</strong></p><ul><li>Include switch for ‘roll-up’ of time periods (monthly, quarterly, semiannually, annually)</li><li>Include switch to accommodate construction delays</li><li>How to deal with Circular References and alternative approaches</li><li>‘Good’ Circular References</li><li>Error checks</li><li>Include switches for different debt repayment profiles (level, annuity, etc.)</li><li>Separation of Construction and Operation time periods and time flexibility</li><li>Use of ‘flags’ for model timing</li><li>Modelling detailed Source &amp; Use statement to account for construction cost profile and draw-down of different financing sources</li><li>Calculating the cost of different financing sources (senior/subordinated debt, preference shares, equity, etc.) and the WACC</li><li>Calculating the cost of equity</li><li>Modelling Cash Waterfall</li><li>Determine optimal capital structure</li><li>Modelling complex debt amortisation schedules (including capitalisation of Interest During Construction)</li></ul><p><strong>Project Evaluation</strong></p><ul><li>Net Cash from operations before debt Service</li><li>Modelling Debt Covenants</li><li>Cash flow to equity and debt providers</li><li>Return to Equity, NPV, IRR and other measures</li></ul><p><strong>Risk and Uncertainty</strong></p><ul><li>Identifying and modelling project risks</li></ul><p><strong>Analysis Tools</strong></p><ul><li>Sensitivity analysis</li><li>Scenario analysis</li><li>Monte Carlo simulation</li></ul><p><strong>Optimization and Targeting</strong></p><ul><li>Use Goal Seek and Solver</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Microsoft Word',
            'image' => 'assets/uploads/courses/1528455050MSWord.jpg',
            'description' => '<p>Learn useful, time-saving features including; Styles, Sections, Columns, Citations, Mail Merge, Table of Contents, Footnotes, Endnotes, working with tables; link reports/charts to MS Excel, etc.</p><p><strong>MS Word Mechanics</strong></p><ul><li>Bullets, Numbers &amp; Lists</li><li>Styles</li><li>Save Time with Templates</li><li>Text and Paragraph Editing</li><li>Working with Breaks<ul><li>Page Break</li><li>Section Break</li><li>Column Break</li></ul></li><li>‘Ruler’ Basics<ul><li>First line Indent</li><li>Hanging Indent</li><li>Left Indent</li><li>Right Indent</li><li>Tab Stops</li></ul></li><li>Working with Document Outline</li><li>Working with Page Layout<ul><li>Column</li><li>Margins</li><li>Paragraph</li><li>Orientation</li></ul></li></ul><p><strong>Headings and Styles</strong></p><ul><li>Using Headings and Styles</li><li>Built-In Heading Styles</li><li>Custom Styles</li></ul><p><strong>References &amp; Document Review</strong></p><ul><li>Footnotes and Endnotes</li><li>Table of Contents</li><li>Table of Figures</li><li>Cross references</li><li>Bookmarks</li></ul><p><strong>Track and Merge Documents</strong></p><ul><li>Compare and Merge Documents</li><li>Revise Documents with Track Changes and Comments</li><li>Compare Documents Side by Side</li></ul><p><strong>More Features</strong></p><ul><li>Cover Page</li><li>Customise the Header and Footer</li><li>Hyperlinks</li><li>Watermarks</li><li>Drop Cap</li><li>Quick Parts</li><li>Working with Tables (including arithmetic calculations)</li><li>Great MS Word Features Mail Merge</li><li>Use Mail Merge for Mass Mailings (letters, email, etc.)</li><li>Setup and maintain a data source</li><li>Formatting numbers and dates with Field Codes</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Microsoft Powerpoint',
            'image' => 'assets/uploads/courses/1528455656MSPowerPoint.jpg',
            'description' => '<p>This course teaches participants about great MS PowerPoint features including animations, working with the Slide Master, pictures, videos and sounds, professional presentation techniques, professional use of animations, etc.</p><p><strong>Microsoft PowerPoint Mechanics</strong></p><ul><li>Slides, text, and notes</li><li>Working with the slide area</li><li>Working with texts (increase/decrease list level, etc.)</li><li>Slide layout</li><li>Slide View<ul><li>Normal</li><li>Slide sorter</li><li>Notes page</li><li>Slide show</li></ul></li><li>Ruler</li><li>Insert and delete slides</li><li>Duplicate slides</li><li>Navigation techniques</li></ul><p><strong>Customise Presentations</strong></p><ul><li>Apply a Theme<ul><li>Preview and Select a Theme</li><li>Save a Custom Theme</li></ul></li><li>Apply a Background Style</li><li>Create slide notes</li><li>Insert date/time, slide number</li><li>Add Footers</li><li>Arrange and Print Sections</li></ul><p><strong>Working with the Slide Master</strong></p><ul><li>Templates and their masters</li><li>Create custom slides - colour schemes, backgrounds, logos, etc.</li><li>Insert new slide master</li><li>Add Watermarks</li><li>Edit built-in templates</li></ul><p><strong>Working with Animations</strong></p><ul><li>Slide Transition (speed, sound, etc.)</li><li>Preset animation</li><li>Custom animation</li><li>Custom effects for text and pictures</li><li>Free Form Animation</li><li>Timing</li></ul><p><strong>Objects/Drawing</strong></p><ul><li>Work with Objects</li><li>Insert and edit text in a shape</li><li>Work with SmartArt</li></ul><p><strong>Proof, Print &amp; Prepare for the Presentation</strong></p><ul><li>Preview on your computer</li><li>Print slides, notes and handouts</li><li>Rehearse presentation timing</li><li>Use Presenter View</li><li>Record Narration</li><li>Integrate graphics and tables into presentations</li><li>Add Hyperlinks to presentations</li><li>Hide Slides</li></ul><p><strong>Sharing a Presentation</strong></p><ul><li>Sharing with a remote audience</li><li>Package a presentation for CD</li><li>Save a presentation to the web</li><li>MS Office (Excel and Word) integration</li></ul><p><strong>Class Exercises</strong> –<em> Impactful Presentations</em></p><ul><li>Professional presentation techniques</li><li>Impactful presentations:<ul><li><em>Participants develop and make presentations. Designed to teach presentation skills and provide tips on public speaking.</em></li></ul></li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Basic Accounting & Financial Statement Analysis',
            'image' => 'assets/uploads/courses/1528456756adv_FinAnalysis.jpg',
            'description' => '<p>Understand the Statement of Financial Position (Balance Sheet), Statement of Profit or Loss and the Statement of Cash Flows; and the relationship between these key reports. Use financial ratios and working capital analysis to assess the financial health of organizations.</p><p><strong>Introduction to Accounting</strong></p><ul><li>Define accounting and the accounting function</li><li>Explain the reasons for preparing accounts</li><li>Explain the basic accounting concepts</li></ul><p><strong>The Accounting Equation and the Statement of Financial Position</strong></p><ul><li>The accounting equation</li><li>Differentiate between assets, liabilities and owners’ equity</li><li>Purpose of the Statement of Financial Position</li><li>Format of the Statement of Financial Position</li></ul><p><strong>The Statement of Profit or Loss</strong></p><ul><li>Understand the Statement of Profit or Loss</li><li>Understand the link between the Statement of Profit or Loss and the Statement of Financial Position</li></ul><p><strong>Double Entry Accounting</strong></p><ul><li>Understand cash and credit transactions in terms of debits and credits</li><li>Record debit and credit entries in ‘T’ accounts</li></ul><p>• Close off revenue and expense accounts</p><p>to the Statement of Profit or Loss</p><ul><li>Close off asset and liability accounts</li></ul><p><strong>The Trial Balance</strong></p><ul><li>Introduction to the General Ledger</li><li>Post transactions for a period, close off the accounts and extract a trial balance</li></ul><p><strong>Accruals and Prepayments</strong></p><ul><li>Matching income with related expenses</li><li>Accrued expenses and income</li><li>Prepaid expenses</li><li>Pre-billed income</li></ul><p><strong>Property, Plant and Equipment</strong></p><ul><li>Calculating depreciation charge</li><li>Statement of Financial Position and Statement of Profit or Loss effect of depreciation</li></ul><p><strong>Interpretation of Financial Statements</strong></p><ul><li>Describe the form and content of published financial statements</li><li>Statement of Cash Flows (and its relationship with Statement of Financial Position and Statement of Profit or Loss)</li><li>Financial statement analysis</li><li>Financial ratios (profitability, efficiency, etc.)</li></ul><p><strong>Cash Flow Analysis</strong></p><ul><li>Understand the components of Statement of Cash Flows</li><li>Apply cash flow analysis to evaluate the financial health of an entity</li></ul><p> </p>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Analyst Boot Camp (ABC) I',
            'image' => 'assets/uploads/courses/1528457922ABC_1.jpg',
            'description' => '<p>This course is designed for Analysts working in corporate/project finance, investment banking, consulting, advisory, asset management, etc. It covers core concepts in Financial Mathematics, MS Excel, Business Valuation and Financial Statement Analysis.</p><p><strong>Financial Mathematics</strong></p><ul><li>Compounding, Discounting and the CAGR</li><li>Annuities (Ordinary and Annuity Due); Perpetuities</li><li>Calculating Cash Flows for Investment Appraisal</li><li>Discounted Cash Flow Analysis</li><li>NPV/IRR &amp; investment decision rules</li></ul><p><strong>Selected Topics in Intermediate /Advanced MS Excel, including:</strong></p><ul><li>Formulas and Functions<ul><li>Relative and Absolute Referencing</li><li>Statistical functions: COUNT, SUMIF, COUNTIF, COUNTA, SUMIFS, SMALL, etc.</li><li>Text functions: TEXT, LEN, TRIM, LOWER, UPPER, PROPER, etc.</li><li>Logical functions: IF, OR, AND, IFERROR, ISERROR, etc.</li><li>Date functions: TODAY, MONTH, YEAR, NETWORKDAYS, etc.</li><li>Look-up functions: VLOOKUP (including use of wildcard characters), LOOKUP</li></ul></li><li>Analysis Tools<ul><li>Working with Pivot Tables</li><li>Goal Seek</li><li>Data Tables</li><li>Working with Named Ranges</li><li>Define Named Ranges</li><li>Edit Named Ranges</li><li>Use Named Ranges in Formulas</li><li>Spreadsheet Modelling Techniques</li><li>Custom Scenario Analysis</li><li>Sensitivity Analysis with Data Tables</li></ul></li></ul><p><strong>Selected Topics in Business Valuation</strong></p><ul><li>The Concept of Value<ul><li>Value of a business</li><li>Equity valuation vs. Firm valuation</li><li>Value vs. Price</li></ul></li><li>Valuation Models<ul><li>Asset-Based Valuation</li><li>Relative/Market-Based (Co-Co, Co-Tran)</li><li>Discounted Cash Flow (DCF)</li><li>Dividend Discount Model (DDM)</li></ul></li></ul><p><strong>Accounting and Financial Analysis</strong></p><ul><li>The Accounting Equation and the Statement of Financial Position<ul><li>The accounting equation</li><li>Differentiate between assets, liabilities and owners’ equity</li><li>Purpose of the Statement of Financial Position</li></ul></li><li>The Statement of Profit or Loss<ul><li>Understand the Statement of Profit or Loss</li><li>Understand the link between the</li></ul></li><li>Statement of Profit or Loss and the Financial Position</li><li>Double Entry Accounting<ul><li>Understand cash and credit transactions in terms of debits and credits</li><li>Record debit and credit entries in ‘T’ accounts</li><li>Close off revenue and expense accounts to the Statement of Profit or Loss</li><li>Close off asset and liability accounts</li></ul></li><li>The Trial Balance<ul><li>Introduction to the General Ledger</li><li>Post transactions for a period, close off the accounts and extract a trial balance</li></ul></li><li>Property, Plant and Equipment<ul><li>Calculating depreciation charge</li><li>Statement of Financial Position and Statement of Profit or Loss effect of depreciation</li></ul></li></ul><p><strong>Assessing the Financial Health of a Company</strong></p><ul><li>Interpretation of Financial Statements, including ratio analysis (Profitability, Liquidity, Solvency, Efficiency, etc.)</li><li>Cash Flow Analysis – examine the source and use of cash in a company</li><li>Free Cash Flows to the Firm (FCFF) &amp; Free Cash Flows to Equity (FCFE)</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Analyst Boot Camp (ABC) II',
            'image' => 'assets/uploads/courses/1528553390ABC_2.jpg',
            'description' => '<p>In this course, participants will learn to use contemporary tools for macro-economic analysis, industry /market analysis and corporate strategy. The course also covers statistical/data analysis with MS Excel.</p><p><strong>Tools for Macro-economic Analysis</strong></p><ul><li>PEST/PESTEL</li></ul><p><strong>Industry Analysis</strong></p><ul><li>Demand Analysis</li><li>Supply/Competitor Analysis</li><li>Porter’s Five Forces Model</li><li>Critical Success Factors</li><li>Case Study of Selected Industries</li></ul><p><strong>Corporate Strategy</strong></p><ul><li>Value Chain Analysis</li><li>SWOT Analysis</li><li>Balanced Scorecard</li></ul><p><strong>Selected Topics in Intermediate/Advanced MS Excel and MS Excel for Experts</strong></p><ul><li>Formulas and Functions<ul><li>Relative and Absolute Referencing</li><li>Statistical functions: COUNT, SUMIF, COUNTIF, COUNTA, SUMIFS, SMALL, etc.</li><li>Text functions: TEXT, LEN, TRIM, LOWER, UPPER, PROPER, etc.</li><li>Logical functions: IF, OR, AND, IFERROR, ISERROR, etc.</li><li>Date functions: TODAY, MONTH, YEAR, NETWORKDAYS, etc.</li><li>Look-up functions: VLOOKUP (including use of wildcard characters), LOOKUP</li></ul></li><li>Analysis Tools<ul><li>Working with Pivot Tables</li><li>Goal Seek</li><li>Data Tables</li></ul></li><li>Working with Named Ranges<ul><li>Define Named Ranges</li><li>Edit Named Ranges</li><li>Use Named Ranges in Formulas</li></ul></li><li>Spreadsheet Modelling Techniques<ul><li>Custom Scenario Analysis</li><li>Sensitivity Analysis with Data Tables</li></ul></li></ul><p><strong>Statistical Analysis with Excel</strong></p><ul><li>Standard Deviation</li><li>Variance Analysis</li><li>Regression Analysis</li></ul><p><strong>Data Presentation</strong></p><ul><li>Creating Dynamic Charts (e.g. using one chart for varying time series, locations, etc.)</li><li>Using Form Controls for Charts</li><li>Charts with Secondary Axis</li><li>Link Charts to MS Word and PowerPoint, with automatic update</li><li>Special charts<ul><li>Waterfall Chart</li><li>GANTT Chart</li><li>Tornado Chart</li></ul></li></ul><p><strong>Selected Topics in Financial Modelling</strong></p><ul><li>Define Inputs/Assumptions<ul><li>Macro-economics</li><li>Project Cost</li><li>Financing</li><li>Revenue</li><li>Expenditure</li><li>Setting-up Scenarios</li></ul></li><li>Setting-up the Reports<ul><li>Statement of Profit or Loss</li><li>Statement of Financial Position</li><li>Statement of Cash Flows</li><li>Summaries/KPIs</li></ul></li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Advanced Financial Statement Analysis & Forecasting',
            'image' => 'assets/uploads/courses/1528553643iStock_000037272592Small.jpg',
            'description' => '<p>Analysts must be able to understand, rigorously analyse and interpret financial and other qualitative information in financial statements. This skill provides the foundation for meaningful performance evaluation, comparative analysis, valuation, due diligence, financial forecast, etc.</p><p>This intensive financial statement analysis course provides participants with a thorough grounding in theoretical frameworks, spreadsheet mechanics and real-world perspectives through the use of extensive exercises and case studies.</p><p><strong>Review of Published Financial Statements</strong></p><ul><li>Trend Analysis<ul><li>Statement of Profit or Loss</li><li>Statement of Financial Position</li><li>Statement of Cash Flows</li></ul></li></ul><p><strong>Common-Sized Financial Statements</strong></p><ul><li>Vertical and Horizontal Common sized financial statements</li></ul><p><strong>Ratio Analysis</strong></p><ul><li>Assess the financial health of an organization</li><li>Evaluate financial performance<ul><li>Profitability;</li><li>Liquidity;</li><li>Efficiency;</li><li>Stability etc.</li></ul></li><li>DuPont Analysis – Decomposition of Return on Equity (ROE)</li></ul><p><strong>Cash Flow Analysis</strong></p><ul><li>Review of the Statement of Cash Flows (Operating, Investing and Financing Activities)</li><li>Estimate Free Cash Flows (Free Cash Flow to the Firm, Free Cash Flow to Equity)</li><li>Working Capital Analysis</li><li>Cash Conversion Cycle</li></ul><p><strong>Forecasting Financial Statements</strong></p><ul><li>Detailed build-up of the Statement if Profit or Loss, Statement of Financial Position &amp; Statement of Cash Flows</li><li>Assess historical trends, strategic plans and determine sustainable growth rates</li><li>Excel best-practices</li></ul><p><strong>Data Presentation</strong></p><ul><li>Professional charting techniques</li><li>Dashboard Design</li></ul><p><strong>The International Financial Reporting Standards (IFRS)</strong></p><ul><li>Overview</li><li>Key drivers</li><li>Comparison with Nigerian GAAP</li><li>Implications for Nigerian organizations (listed entities, significant public entities, SMEs, etc.)</li><li>Challenges</li><li>Implementation timelines</li></ul>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        DB::table('courses')->insert([
            'title' => 'Business Valuation',
            'image' => 'assets/uploads/courses/1528443519BizValuation.jpg',
            'description' => '<p>In this course, participants will learn about the concept of Value, differentiating between value and price; equity valuation methods/models, forecasting techniques and addressing specific issues in Valuation (loss making firm, banks, etc.).</p><p><strong>Time Value of Money and Investment Appraisal</strong></p><ul><li>Compounding/Discounting</li><li>CAGR</li><li>NPV, IRR, Payback Period</li></ul><p><strong>The Concept of Value</strong></p><ul><li>Value of a business</li><li>Equity valuation vs. Firm valuation</li><li>Value vs. Price</li></ul><p><strong>Valuation Methods &amp; Models – Theoretical Framework</strong></p><ul><li>Asset-based method</li><li>Market-based method</li><li>Discounted Cash Flow method</li><li>Choosing the right method</li></ul><p><strong>Discounted Cash Flow Valuation</strong></p><ul><li>Discounted Cash Flow (DCF) Model</li><li>The Explicit Forecast Period</li><li>Concept of Terminal Value</li><li>Cost of Capital</li><li>The Long-Term Growth Rate</li></ul><p><strong>Forecasting for Discounted Flow Valuation</strong></p><ul><li>Forecasting Statement of Profit or Loss, Statement of Financial Position, and Statement of Cash Flows for existing businesses</li><li>Industry Analysis</li><li>Naïve Extrapolation and other issues to consider when forecasting</li></ul><p><strong>Relative/Market Based Valuation</strong></p><ul><li>Comparable Companies (Co-Co)</li><li>Comparable Transactions (Co-Tran)</li><li>Multiples (Earnings, EBITDA, Sales, etc.)</li><li>Choosing the right multiples</li><li>“Normalization” of earnings</li><li>Maintainable Earnings</li></ul><p><strong>Asset-based Valuation</strong></p><ul><li>Replacement Value</li><li>Adjusted Net Book Value</li></ul><p><strong>Specific Issues in Valuation</strong></p><ul><li>Valuing loss making firms</li><li>Valuing financial services firms</li><li>Valuation in the absence of data (emerging markets, etc.)</li><li>Application of Discounts and Premiums</li></ul><p> </p>',
            // 'expectations_1' => '',
            // 'expectations_2' => ''
        ]);

        



        





    }
}
