<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('title');
            $table->string('image')->nullable();
            $table->string('venue');
            $table->text('description')->nullable();
            $table->text('outcome');
            $table->text('course_delivery');
            $table->text('who_should_attend');
            $table->double('cost', 11, 2);

            $table->dateTime('start_date');
            $table->dateTime('end_date');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
