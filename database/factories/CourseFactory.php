<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(10),
        'status' => 'APPROVED',
    ];
});

$factory->define(App\Course::class, function (Faker $faker) {
    

    return [
        'title' => $faker->realText(50),
        'description' => $faker->realText(50),
        'photo' => $faker->realText(10),
       
        'status' => 'APPROVED'
    ];
});


