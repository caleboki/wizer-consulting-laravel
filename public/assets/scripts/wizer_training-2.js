var $grid = $('.grid').isotope({
	itemSelector: '.element-item',
	// layoutMode: 'fitRows',
	// getSortData: {
	// 	category: [data-category]
	// }
});

$('#filters').on('click', 'a', function(){
	var filterValue = $(this).attr('data-filter');
	console.log("My filter: " + filterValue);
	$grid.isotope({filter: filterValue});
});